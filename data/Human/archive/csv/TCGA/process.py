import os
from os import listdir
from os.path import isfile, join
import pandas as pd

main = pd.DataFrame()
cwd = os.getcwd()
for f in listdir(cwd):
    if (isfile(join(cwd, f)) and f != ".DS_Store"):
        print(f)
        df = pd.read_json(f)
        df['normal'] = 0
        df['abnormal'] = 0
        df['pvalue'] = 1
        df = df[['sampleID','gene','normal','abnormal','log2','pvalue']]
        if main.empty:
            main = df
        else:
            main = main.append(df)

main.to_csv("a-thca.csv",index=False)
