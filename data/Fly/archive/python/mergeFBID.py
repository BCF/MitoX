import os
from os import listdir
from os.path import isfile, join
import pandas as pd

real = pd.read_csv("../real.txt",sep="\t")
real = real[['FBgen','FBgenNew','gene']]
unseen = pd.read_csv("../unseen.txt",sep="\t")
unseen = unseen[['FBgen','FBgenNew','gene']]
seen = pd.read_csv("../seen.txt",sep="\t")
seen = seen[['FBgen','FBgenNew','gene']]

cwd = os.getcwd()
for f in listdir(cwd):
    if (isfile(join(cwd, f)) and f != ".DS_Store"):
        main = pd.DataFrame()
        df = pd.read_csv(f,sep=",")
        df2 = pd.merge(df,real,how='inner',on='FBgen')
        main = df2
        df2 = pd.merge(df,unseen,how='inner',on='FBgen')
        main = main.append(df2)
        df2 = pd.merge(df,seen,how='inner',on='FBgen')
        main = main.append(df2)
        main.drop_duplicates(subset='gene', keep="first")
        main['flybaseID'] = main['FBgen']
        main['userID'] = 'mitox'
        main['normal'] = 0
        main['abnormal'] = 0
        main = main[['userID','sampleID','flybaseID','gene','normal','abnormal','log2','pvalue']]

        sampleName = f.split('.')[0]
        main.to_csv(sampleName+'_newnew.csv',index=False,header=False)
