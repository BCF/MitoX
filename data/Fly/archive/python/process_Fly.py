import os
from os import listdir
from os.path import isfile, join
import pandas as pd

main = pd.DataFrame()
cwd = os.getcwd()
for f in listdir(cwd):
    if (isfile(join(cwd, f)) and f != ".DS_Store"):
        df = pd.read_csv(f,sep="\t")
        sampleName = f.split('.')[0]
        df['log2'] = df['log2FoldChange']
        #df['FBgen'] = df['FbgnID']
        df['pvalue'] = df['padj']
        df['sampleID'] = sampleName
        #df['userID'] = 'mitox'
        #df['normal'] = 0
        #df['abnormal'] = 0
        #df['flybaseID'] = df['FBgen']
        #df['gene'] = df['gene_symbol']
        #df = df[['userID','sampleID','flybaseID','gene','normal','abnormal','log2','pvalue']]
        df = df [['sampleID','FBgen','log2','pvalue']]
#        if main.empty:
#            main = df
#        else:
#            main = main.append(df)

        df.to_csv(sampleName+'_new.csv',index=False,header=True)
