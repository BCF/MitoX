import os
from os import listdir
from os.path import isfile, join
import pandas as pd
import math

df2 = pd.DataFrame()
f = "Expression_DGRP_male_Huang.txt"
df = pd.read_csv(f,sep=" ")
df['mean'] = df.mean(axis=1)
collist = list(df)
collist.pop(0)
for x in collist:
    df[x] /= df['mean']
    df[x] = df[x].apply(lambda x: math.log(x,2))
    sampleName = x + '_Male'
    df2['FBgen'] = df['gene']
    df2['pvalue'] = 1
    df2['log2'] = df[x]
    df2['sampleID'] = sampleName
    df2 = df2 [['sampleID','FBgen','log2','pvalue']]
    df2.to_csv(sampleName+'.csv',index=False,header=True)


