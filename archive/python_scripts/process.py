import os
from os import listdir
from os.path import isfile, join
import pandas as pd

main = pd.DataFrame()
cwd = os.getcwd()
for f in listdir(cwd):
    if (isfile(join(cwd, f)) and f != ".DS_Store"):
        print(f)
        df = pd.read_json(f)
        df['userID'] = 'mitox'
        df['normal'] = 0
        df['abnormal'] = 0
        df['flybaseID'] = df['FlybaseID']
        df = df[['userID','sampleID','flybaseID','gene','normal','abnormal','log2','pvalue']]
        if main.empty:
            main = df
        else:
            main = main.append(df)

main.to_csv("male.csv",index=False,header=False)
