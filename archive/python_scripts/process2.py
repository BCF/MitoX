#!/usr/bin/python

import cgi, os, re, sys
import cgitb;cgitb.enable()
import json
import pandas as pd
import sys
#Read in files
sampleID = sys.argv[3]

exp = pd.read_csv(sys.argv[1],sep="\t")
exp.columns = ['gene','normal','abnormal','log2','pvalue']
exp['sampleID'] = sampleID
exp = exp[['sampleID','gene','normal','abnormal','log2','pvalue']]

mut = pd.read_csv(sys.argv[2],sep="\t",usecols=[0,1,2,3,4,5,6])
mut.columns = ['gene','chr','position','ref','alt','effect','consequence']
mut['mutation'] = mut['chr'].astype(str)+ ' ' + mut['position'].astype(str) + ' ' + mut['ref'] + '|' + mut['alt'] + ': ' + mut['effect'] + ': ' + mut['consequence']
mut = mut[['gene','mutation']]
mut = mut.groupby('gene').apply(lambda x: '; '.join(x.mutation)).reset_index()
#mut.columns=['gene','mutation']
#mut['sampleID'] = sampleID
#mut = mut[['sampleID','gene','mutation']]


outputname1 = sampleID + '_newexp.csv'
#outputname2 = sampleID + '_newmut.csv'

exp.to_csv(outputname1,index=False,header=False)
#mut.to_csv(outputname2,index=False,header=False)
