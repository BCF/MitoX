import os
from os import listdir
from os.path import isfile, join
import pandas as pd

main = pd.DataFrame(columns=('userID','sampleID','folder','subfolder','organism','datetime'))
cwd = os.getcwd()
for f in listdir(cwd):
    if (isfile(join(cwd, f)) and f != ".DS_Store"):
        print(f)
        dfraw = pd.read_json(f)
        sampleID = dfraw['sampleID'][0]
        main.loc[-1] = ['mitox', sampleID, 'DGRP','Male','Fly','20870519']
        main.index = main.index + 1

main.to_csv("dir-male.csv",index=False)
