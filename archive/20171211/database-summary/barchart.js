function Barchart(){

  var BC={};

  var data = [],
      svg,
      defs,
      gBrush,
      brush,
      main_xScale,
      mini_xScale,
      main_yScale,
      mini_yScale,
      main_yZoom,
      main_xAxis,
      main_yAxis,
      mini_width,
      textScale;

  var bccolor = d3.scale.ordinal()
    .range(["#8dd3c7", "#ffffb3", "#bebada", "#fb8072", "#80b1d3", "#fdb462", "#b3de69", "#fccde5", "#d9d9d9", "#bc80bd", "#ccebc5", "#f781bf", "#fbb4ae", "#b3cde3", "#ffed6f", "#decbe4", "#fed9a6"]);

  BC.init = function(data) {

    //Create the random data
    /*for (var i = 0; i < 40; i++) {
      var my_object = {};
      my_object.key = i;
      my_object.country = makeWord();
      my_object.value = Math.floor(Math.random() * 600);
      data.push(my_object);
    }//for i 
    data.sort(function(a,b) { return b.value - a.value; });*/

    var counter = 0;
	data.forEach(function(d){
		d.key = counter;
		d.country = d.name + " (" + d.total + ")";
		d.value = d.total;
		counter +=1;
		delete d.name;
		delete d.total;
		delete d.type;
	})
	data.sort(function(a,b) { return b.value - a.value; });

    /////////////////////////////////////////////////////////////
    ///////////////// Set-up SVG and wrappers ///////////////////
    /////////////////////////////////////////////////////////////

    //Added only for the mouse wheel
    var zoomer = d3.behavior.zoom()
        .on("zoom", null);

    var main_margin = {top: 10, right: 10, bottom: 30, left: 10},
        main_width = 400 - main_margin.left - main_margin.right,
        main_height = 500 - main_margin.top - main_margin.bottom;

    var mini_margin = {top: 10, right: 10, bottom: 30, left: 10},
        mini_height = 500 - mini_margin.top - mini_margin.bottom;
    mini_width = 100 - mini_margin.left - mini_margin.right;


    svg = d3.select("#chart").append("svg")
        .attr('class', 'canvas svg-content-responsive')
        .attr('preserveAspectRatio', 'xMinYMin meet')
        .attr('viewBox', [0, 0, main_width + main_margin.left + main_margin.right + mini_width + mini_margin.left + mini_margin.right, main_height + main_margin.top + main_margin.bottom].join(' '))
        //.attr("class", "svgWrapper")
        //.attr("width", main_width + main_margin.left + main_margin.right + mini_width + mini_margin.left + mini_margin.right)
        //.attr("height", main_height + main_margin.top + main_margin.bottom)
        .call(zoomer)
        .on("wheel.zoom", scroll)
        //.on("mousewheel.zoom", scroll)
        //.on("DOMMouseScroll.zoom", scroll)
        //.on("MozMousePixelScroll.zoom", scroll)
        //Is this needed?
        .on("mousedown.zoom", null)
        .on("touchstart.zoom", null)
        .on("touchmove.zoom", null)
        .on("touchend.zoom", null);

    var mainGroup = svg.append("g")
            .attr("class","mainGroupWrapper")
            .attr("transform","translate(" + main_margin.left + "," + main_margin.top + ")")
            .append("g") //another one for the clip path - due to not wanting to clip the labels
            .attr("clip-path", "url(#clip)")
            .style("clip-path", "url(#clip)")
            .attr("class","mainGroup");

    var miniGroup = svg.append("g")
            .attr("class","miniGroup")
            .attr("transform","translate(" + (main_margin.left + main_width + main_margin.right + mini_margin.left) + "," + mini_margin.top + ")");

    var brushGroup = svg.append("g")
            .attr("class","brushGroup")
            .attr("transform","translate(" + (main_margin.left + main_width + main_margin.right + mini_margin.left) + "," + mini_margin.top + ")");

    /////////////////////////////////////////////////////////////
    ////////////////////// Initiate scales //////////////////////
    /////////////////////////////////////////////////////////////

    main_xScale = d3.scale.linear().range([0, main_width]);
    mini_xScale = d3.scale.linear().range([0, mini_width]);

    main_yScale = d3.scale.ordinal().rangeBands([0, main_height], 0.4, 0);
    mini_yScale = d3.scale.ordinal().rangeBands([0, mini_height], 0.4, 0);

    //Based on the idea from: http://stackoverflow.com/questions/21485339/d3-brushing-on-grouped-bar-chart
    main_yZoom = d3.scale.linear()
        .range([0, main_height])
        .domain([0, main_height]);

    //Create x axis object
    main_xAxis = d3.svg.axis()
      .scale(main_xScale)
      .orient("bottom")
      .ticks(4)
      //.tickSize(0)
      .outerTickSize(0);

    //Add group for the x axis
    d3.select(".mainGroupWrapper")
        .append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(" + 0 + "," + (main_height + 5) + ")");

    //Create y axis object
    main_yAxis = d3.svg.axis()
      .scale(main_yScale)
      .orient("right")
      .tickSize(0)
      .outerTickSize(0);

    //Add group for the y axis
    d3.select(".mainGroupWrapper")
        .append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(10,0)")
        .attr("clip-path", "url(#clip)")
        .style("clip-path", "url(#clip)");
 
    /////////////////////////////////////////////////////////////
    /////////////////////// Update scales ///////////////////////
    /////////////////////////////////////////////////////////////

    //Update the scales
    main_xScale.domain([0, d3.max(data, function(d) { return d.value; })]);
    mini_xScale.domain([0, d3.max(data, function(d) { return d.value; })]);
    main_yScale.domain(data.map(function(d) { return d.country; }));
    mini_yScale.domain(data.map(function(d) { return d.country; }));
    
    //Create the visual part of the y axis
    d3.select(".mainGroupWrapper").select(".y.axis").call(main_yAxis);
    d3.select(".mainGroupWrapper").select(".x.axis").call(main_xAxis);

    /////////////////////////////////////////////////////////////
    ///////////////////// Label axis scales /////////////////////
    /////////////////////////////////////////////////////////////

    textScale = d3.scale.linear()
      .domain([15,50])
      .range([20,12])
      .clamp(true);
    
    /////////////////////////////////////////////////////////////
    ///////////////////////// Create brush //////////////////////
    /////////////////////////////////////////////////////////////

    //What should the first extent of the brush become - a bit arbitrary this
    var brushExtent = Math.max( 1, Math.min( 20, Math.round(data.length*0.3) ) );

    brush = d3.svg.brush()
        .y(mini_yScale)
        .extent([mini_yScale(data[0].country), mini_yScale(data[brushExtent].country)])
        .on("brush", function(){
        	brushmove(data);
        })
        //.on("brushend", brushend);

    //Set up the visual part of the brush
    gBrush = d3.select(".brushGroup").append("g")
      .attr("class", "brush")
      .call(brush);
    
    gBrush.selectAll(".resize")
      .append("line")
      .attr("x2", mini_width);

    gBrush.selectAll(".resize")
      .append("path")
      .attr("d", d3.svg.symbol().type("triangle-up").size(20))
      .attr("transform", function(d,i) { 
        return i ? "translate(" + (mini_width/2) + "," + 4 + ") rotate(180)" : "translate(" + (mini_width/2) + "," + -4 + ") rotate(0)"; 
      });

    gBrush.selectAll("rect")
      .attr("width", mini_width);

    //On a click recenter the brush window
    gBrush.select(".background")
      .on("mousedown.brush", brushcenter)
      .on("touchstart.brush", brushcenter);

    ///////////////////////////////////////////////////////////////////////////
    /////////////////// Create a rainbow gradient - for fun ///////////////////
    ///////////////////////////////////////////////////////////////////////////

    defs = svg.append("defs")

    //Add the clip path for the main bar chart
    defs.append("clipPath")
      .attr("id", "clip")
      .append("rect")
    	.attr("x", -main_margin.left)
      .attr("width", main_width + main_margin.left)
      .attr("height", main_height);

    /////////////////////////////////////////////////////////////
    /////////////// Set-up the mini bar chart ///////////////////
    /////////////////////////////////////////////////////////////

    //The mini brushable bar
    //DATA JOIN
    var mini_bar = d3.select(".miniGroup").selectAll(".bar")
      .data(data, function(d) { return d.key; });

    //UDPATE
    mini_bar
      .attr("width", function(d) { return mini_xScale(d.value); })
      .attr("y", function(d,i) { return mini_yScale(d.country); })
      .attr("height", mini_yScale.rangeBand());

    //ENTER
    mini_bar.enter().append("rect")
      .attr("class", "bar")
      .attr("x", 0)
      .attr("width", 0)
      .attr("y", function(d,i) { return mini_yScale(d.country); })
      .attr("height", mini_yScale.rangeBand())
      .transition()
      .duration(1500)
      .attr("width", function(d) { return mini_xScale(d.value); })
      .style("fill", function (d) {
                return bccolor(d.country);
        });


    //EXIT
    mini_bar.exit()
      .remove();

    //Start the brush
    gBrush.call(brush.event);


  }//init

  //Function runs on a brush move - to update the big bar chart
  function update(data) {

    /////////////////////////////////////////////////////////////
    ////////// Update the bars of the main bar chart ////////////
    /////////////////////////////////////////////////////////////
    //DATA JOIN
    var bar = d3.select(".mainGroup").selectAll(".bar")
        .data(data, function(d) { return d.key; });

    //UPDATE
    bar
      .attr("y", function(d,i) { return main_yScale(d.country); })
      .attr("height", main_yScale.rangeBand())
      .attr("x", 0)
      .transition().duration(50)
      .attr("width", function(d) { return main_xScale(d.value); });

    //ENTER
    bar.enter().append("rect")
      .attr("class", "bar")
      .style("fill", function (d) {
                return bccolor(d.country);
      })
      .attr("y", function(d,i) { return main_yScale(d.country); })
      .attr("height", main_yScale.rangeBand())
      .attr("x", 0)
      .attr("width", 0)
      .transition().duration(1500)
      .attr("width", function(d) { return main_xScale(d.value); });

    //EXIT
    bar.exit()
      .remove();

  }//update

  /////////////////////////////////////////////////////////////
  ////////////////////// Brush functions //////////////////////
  /////////////////////////////////////////////////////////////

  //First function that runs on a brush move
  function brushmove(data) {

    var extent = brush.extent();

    //Which bars are still "selected"
    var selected = mini_yScale.domain()
      .filter(function(d) { return (extent[0] - mini_yScale.rangeBand() + 1e-2 <= mini_yScale(d)) && (mini_yScale(d) <= extent[1] - 1e-2); }); 
    //Update the colors of the mini chart - Make everything outside the brush grey
    d3.select(".miniGroup").selectAll(".bar")
      .style("fill", function(d, i) { return selected.indexOf(d.country) > -1 ? bccolor(d.country) : "#e0e0e0"; });

    //Update the label size
    d3.selectAll(".y.axis text")
      .style("font-size", "18px");
    
    /////////////////////////////////////////////////////////////
    ///////////////////// Update the axes ///////////////////////
    /////////////////////////////////////////////////////////////

    //Reset the part that is visible on the big chart
    var originalRange = main_yZoom.range();
    main_yZoom.domain( extent );

    //Update the domain of the x & y scale of the big bar chart
    main_yScale.domain(data.map(function(d) { return d.country; }));
    main_yScale.rangeBands( [ main_yZoom(originalRange[0]), main_yZoom(originalRange[1]) ], 0.4, 0);

    //Update the y axis of the big chart
    d3.select(".mainGroupWrapper")
      .select(".y.axis")
      .call(main_yAxis);

    //Update the big bar chart
    update(data);
    
  }//brushmove

  /////////////////////////////////////////////////////////////
  ////////////////////// Click functions //////////////////////
  /////////////////////////////////////////////////////////////

  //Based on http://bl.ocks.org/mbostock/6498000
  //What to do when the user clicks on another location along the brushable bar chart
  function brushcenter() {
    var target = d3.event.target,
        extent = brush.extent(),
        size = extent[1] - extent[0],
        range = mini_yScale.range(),
        y0 = d3.min(range) + size / 2,
        y1 = d3.max(range) + mini_yScale.rangeBand() - size / 2,
        center = Math.max( y0, Math.min( y1, d3.mouse(target)[1] ) );

    d3.event.stopPropagation();

    gBrush
        .call(brush.extent([center - size / 2, center + size / 2]))
        .call(brush.event);

  }//brushcenter

  /////////////////////////////////////////////////////////////
  ///////////////////// Scroll functions //////////////////////
  /////////////////////////////////////////////////////////////

  function scroll() {

    //Mouse scroll on the mini chart
    var extent = brush.extent(),
      size = extent[1] - extent[0],
      range = mini_yScale.range(),
      y0 = d3.min(range),
      y1 = d3.max(range) + mini_yScale.rangeBand(),
      dy = d3.event.deltaY,
      topSection;

    if ( extent[0] - dy < y0 ) { topSection = y0; } 
    else if ( extent[1] - dy > y1 ) { topSection = y1 - size; } 
    else { topSection = extent[0] - dy; }

    //Make sure the page doesn't scroll as well
    d3.event.stopPropagation();
    d3.event.preventDefault();

    gBrush
        .call(brush.extent([ topSection, topSection + size ]))
        .call(brush.event);

  }//scroll

  return BC
}

function Piechart(){
  	var PC = {};

	var main_margin = {top: 10, right: 10, bottom: 30, left: 10},
	    main_width = 500 - main_margin.left - main_margin.right,
	    main_height = 500 - main_margin.top - main_margin.bottom,
	    radius = Math.min(main_width, main_height) / 2;

	var color = d3.scale.ordinal()
	    .range(["#8dd3c7", "#ffffb3", "#bebada", "#fb8072", "#80b1d3", "#fdb462", "#b3de69", "#fccde5", "#d9d9d9", "#bc80bd", "#ccebc5", "#f781bf", "#fbb4ae", "#b3cde3", "#ffed6f", "#decbe4", "#fed9a6"]);

	var arc = d3.svg.arc()
	    .outerRadius(radius - 10)
	    .innerRadius(0);

	var label = d3.svg.arc()
	    .outerRadius(radius - 60)
	    .innerRadius(radius - 100);

	var pie = d3.layout.pie()
	    .sort(null)
	   .startAngle(1.1*Math.PI)
	    .endAngle(3.1*Math.PI)
	    .value(function(d) { return d.total; });

	PC.init = function(divname,dataset){

	 	var div = d3.select("body").append("div").attr("class", "toolTip");


	    var svg = d3.select(divname).append("svg")
	        .attr('class', 'canvas svg-content-responsive')
	        .attr('preserveAspectRatio', 'xMinYMin meet')
	        .attr('viewBox', [0, 0, main_width + main_margin.left + main_margin.right, main_height + main_margin.top + main_margin.bottom].join(' '))
	      .append("g")
	        .attr("transform", "translate(" + (main_margin.left + (main_width / 2))  + "," + (main_margin.top + (main_height / 2))  + ")");


	     var g = svg.selectAll(".arc")
	          .data(pie(dataset))
	        .enter().append("g")
	          .attr("class", "arc");

	      g.append("path")
	      .style("fill", function(d) { return color(d.data.name); })
	        .transition().duration(1500)
	      .attrTween('d', function(d) {
	        var i = d3.interpolate(d.startAngle+0.1, d.endAngle);
	        return function(t) {
	          d.endAngle = i(t); 
	          return arc(d)
	          }
	        }); 
	      g.append("text")
	          .attr("transform", function(d) { return "translate(" + label.centroid(d) + ")"; })
	          .attr("dy", ".35em")
	        .transition()
	        .delay(1500)
	          .text(function(d) { return d.data.name; })
	          .style('display', function (d) { return (d.endAngle-d.startAngle)/Math.PI*180 > 60 ? null : "none"; });

	      d3.selectAll("path").on("mousemove", function(d) {
	          div.style("left", d3.event.pageX+10+"px");
	          div.style("top", d3.event.pageY-25+"px");
	          div.style("display", "inline-block");
	        div.html((d.data.name)+"<br>"+(d.data.total));
	    });
	        
	    d3.selectAll("path").on("mouseout", function(d){
	        div.style("display", "none");
	    });
	    }
	    
	//d3.select("body").transition().style("background-color", "#d3d3d3");
	function type(d) {
	  d.total = +d.total;
	  return d;
	}
  	return PC
}

var BC=Barchart();
    PC1=Piechart();
    PC2=Piechart();

var this_js_script = $('script[src*=barchart]');
    var host = this_js_script.attr('host'),
        port = this_js_script.attr('port'),
        user = this_js_script.attr('user'),
        passwd = this_js_script.attr('passwd'),
        unix_socket = this_js_script.attr('unix_socket');

var parameter = 'host='+host + '&port='+port + '&user='+user + '&passwd='+passwd + '&unix_socket='+unix_socket;

    jQuery.ajax({
        url: "./python/databaseStat.py", 
        data: parameter,
        type: "POST",
        //dataType: "json",    
        success: function (jsondata) {
            
            d3.select('#loadingdb').remove();
            document.getElementById("barchart").style.display = ""
            document.getElementById("pies").style.display = ""
            var orgs = JSON.parse(jsondata[0]["orgs"])
            PC1.init("#pie1",orgs)

            var total = d3.sum(orgs, function(d){return d.total;})
            var orgsNum = Object.keys(orgs).length

            document.getElementById("pcinfo").innerHTML = "Total number of organisms: " + orgsNum + "<br>" + "Total number of samples: " + total;

            var proj = JSON.parse(jsondata[0]["proj"])
            PC2.init("#pie2",proj)

            var process = JSON.parse(jsondata[0]["process"])
            BC.init(process)
        },
        error: function(e){
            console.log(e);
        }
    });
