<?php 
// restore existing user upload session when browser was closed
if (isset($_COOKIE['mitox_session_id'])) { 
        session_id($_COOKIE['mitox_session_id']);
}
if ($_GET['sessionid']) { 
        session_id($_GET['sessionid']);
}
session_start();
$id = session_id();
setcookie('mitox_session_id',$id,time() + (86400 * 7));


$PCA_path = "data/user_uploads/".$id."/PCA/";
if (!is_dir($PCA_path)){
    mkdir($PCA_path, 0777, true);
}

$heatmap_path = "data/user_uploads/".$id."/heatmap/";
if (!is_dir($heatmap_path)){
    mkdir($heatmap_path, 0777, true);
}

//Get info for mysql server
$str = file_get_contents('mysql/mysql_info.json');
$json = json_decode($str, true);
$host = $json['host'];
$port = $json['port'];
$user = $json['user'];
$passwd = $json['passwd'];
$unix_socket = $json['unix_socket'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MitoXplorer</title>

    <!-- CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font.css" rel="stylesheet" type="text/css">
    <link href="css/index/agency.css" rel="stylesheet">
    <!-- CSS for database -->
    <link href="css/index/database.css" rel="stylesheet">
    <link href="css/index/bootstrap-table.min.css" rel="stylesheet">
    
    <!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <link rel="icon" type="image/png" href="img/logos/favicon.png">

</head>

<body id="page-top" class="index">
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">MitoXplorer</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#database">Database</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#analysis">Analysis</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#tutorial">Tutorial</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#download">Download</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Welcome To MitoXplorer</div>
                <div class="intro-heading">Let's get started!</div>
                <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>

            </div>
        </div>
    </header>
    
    <!-- About Section -->
    <section id="about" class="bg-lightest">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">About</h2>
                    <h3 class="section-subheading text-muted">MitoXplorer - A Visualization Tool for Mitochondrial Gene Expression and Mutation</h3>
                    <p class="large">Mitochondria are important organelles of eukaryotic cells. They serve primarily as a powerhouse to generate energy, and are also responsible for a lot of crucial biological functions.</p>
                    <p class="large">MitoXplorer maps mutation and expression data to our mitochondrial model that consists of known functions of mitochondria, and allows user to explore the data in an interactive way. You could start with browsing our <a class="page-scroll" href="#database">databases</a>, doing <a class="page-scroll" href="#analysis">comparative analysis</a> on them or even <a class="page-scroll" href="#analysis">upload your own data</a>.</p>
                    <p class="large">We also provide pipelines and other plugin for <a class="page-scroll" href="#download">downloading</a> to aid your analysis in mitochondrial genes and mitochondria-related research.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Database Human Grid Section -->
    <section id="database" class="bg-darkest">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Database</h2>
                    <h3 class="section-subheading text-muted">Browse our public databases</h3>
                </div>
            
            </div>
            
            <div class="row">
                <div class="col-md-12 col-sm-6 database-item">
                    <h4 class="titleDatabase"> HUMAN DATASET</h4>
                </div>
                <div class="col-md-4 col-sm-6 database-item">
                    <a href="#aneuploidy_subwindow" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/database/aneuploidy.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>Aneuploidy</h4>
                        <p class="text-muted">Cell lines and public data with aneuploidy</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 database-item">
                    <a href="#database-forall" onclick="return createTable('TCGA','The Cancer Genome Atlas','Human');" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/database/tcga.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>TCGA project</h4>
                        <p class="text-muted">The Cancer Genome Atlas</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 database-item">
                    <a href="#database-forall" onclick="return createTable('User_upload','My Data (Human)','Human');" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/database/mydata.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>My Data</h4>
                        <p class="text-muted" >Browse your own data</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Database Mouse Grid Section -->
    <section id="databaseMouse" class="bg-lightest">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 database-item">
                <!-- Fly Database -->
                <div class="col-md-12 col-sm-6 database-item">
                     <h4 class="titleDatabase">MOUSE DATASET</h4>
                </div>
                <div class="col-md-4 col-sm-6 database-item">
                    <a href="#database-forall" onclick="return createTable('Aneuploidy_public_m','Public Mouse Data','Mouse');" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/database/trisomy21.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>Public Mouse Data</h4>
                        <p class="text-muted">From online databases</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 database-item">
                    <a href="#database-forall" onclick="return createTable('User_upload','My Data (Mouse)','Mouse');" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/database/mydata.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>My Data</h4>
                        <p class="text-muted" >Browse your own data</p>
                    </div>
                </div>
                </div>
          </div>
		</div>
    </section>    
    
    <!-- Database Fly Grid Section -->
    <section id="databaseFly" class="bg-darkest">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 database-item">
                <!-- Fly Database -->
                <div class="col-md-12 col-sm-6 database-item">
                     <h4 class="titleDatabase"> FLY DATASET</h4>
                </div>
                <div class="col-md-4 col-sm-6 database-item">
                    <a href="#database-forall" onclick="return createTable('DGRP','DGRP','Fly');" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/database/dgrp2.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>DGRP</h4>
                        <p class="text-muted">DGRP - Summarized Expression</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 database-item">
                    <a href="#database-forall" onclick="return createTable('DEGENES','Time Course','Fly');" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/database/flightmuscle.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>Time Course Comparison</h4>
                        <p class="text-muted">DEGENES</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 database-item">
                    <a href="#database-forall" onclick="return createTable('FALLEN','TF gene knockout','Fly');" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/database/flightmuscle.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>FALLEN</h4>
                        <p class="text-muted" >TF gene Fallen knockout studies</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 database-item">
                    <a href="#database-forall" onclick="return createTable('User_upload','My Data (Fly)','Fly');" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/database/mydata.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>My Data</h4>
                        <p class="text-muted" >Browse your own data</p>
                    </div>
                </div>
                </div>
          </div>
		</div>
    </section>
    
    
    
    
    <!-- Analysis Section -->
    <section id="analysis" class="bg-lightest">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Analysis</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row text-center analysis">
                <div class="col-md-6">
                    <ul class="list-inline analysis">
                        <li>
                            <a href="#analysis-compare" class="database-link" data-toggle="modal"><i class="fa fa-laptop"></i></a>
                        </li>
                    </ul>
                    <h4 class="service-heading">Comparative Analysis</h4>
                    <p class="text-muted">Launch an analysis on the dataset</p>
                </div>
                
               <div class="col-md-6">
                    <ul class="list-inline analysis">
                        <li>
                            <a href="#analysis-upload" class="database-link" data-toggle="modal"><i class="fa fa-upload"></i></a>
                        </li>
                    </ul>
                    <h4 class="service-heading">Upload and Visualize Your Data</h4>
                    <p class="text-muted">Loading your data on the website and launch analysis</p>
                </div>
                <br>
            </div>
        </div>
    </section>
    
    
    
    <!-- Tutorial Section -->
    <section id="tutorial" class="bg-darkest">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                            <h2 class="section-heading">Tutorial</h2>
                            <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row text-center analysis">
                <div class="col-md-12">
					<ul class="list-inline analysis">
                        <li>
                            <a href="tutorial.php#page-top" class="tutorial-launcher" onclick="window.open(this.href,'','scrollbars=no,resizable=yes, location=no,menubar=no,status=no,toolbar=no,left='+(screen.availWidth/2-350)+ ', top='+(screen.availHeight/2-350)+',width=1200,height=850');return false;"><i class="fa fa-question-circle-o" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                    <h4 class="service-heading">Tutorial</h4>
                    <p class="text-muted">Launch the tutorial session for better understanding of the different tools</p>
                </div>
            </div>
        </div>    
   </section>
    
    <!-- Download Grid Section -->
    <section id="download" class="bg-lightest">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Download</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 database-item">
                    <a href="#databaseModal1" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/download/pipeline.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>RNA-seq pipeline</h4>
                        <p class="text-muted">Analysis Pipeline</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 database-item">
                    <a href="#databaseModal2" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/download/fiji.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>Mitomorph</h4>
                        <p class="text-muted">FIJI Plugin</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Contact Us</h2>
                    <h3 class="section-subheading text-muted">Interested in our project? Contact us through email!</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <h4>Dr. Bianca Habermann</h4>
                    <p>Group leader Computational biology</p><br>
                    <h4>Max Planck Institute of Biochemistry</h4>
                    <p>
                        <span class="glyphicon glyphicon-envelope" aria-hidden="true" style="font-size: 1em;width: 25px;"></span><a href="mailto:your-email@your-domain.com">habermann@biochem.mpg.de</a><br>
                    <span class="glyphicon glyphicon-earphone" aria-hidden="true" style="font-size: 1em;width: 25px;"></span>+49 1234567<br>

                        <span><i class="fa fa-map-marker fa-1x" style="font-size: 1em;width: 25px;"></i></span>Am Klopferspitz 18 <br>
                        <span><i class="fa fa-map-marker fa-1x" style="opacity:0;font-size: 1em;width: 25px;"></i></span>82152 Martinsried Germany<br>
                    </p>
                </div>
                <div class="col-lg-6">
                <a href = "https://www.mpg.de/de" target="_blank"><img src="img/logos/mpglogo.png" height="200"></a>
                <a href = "http://www.biochem.mpg.de/en" target="_blank"><img src="img/logos/logo-mpi-biochem.png" width="300"></a>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="copyright">Copyright &copy; Max-Planck-Gesellschaft, München 2016</span>
                </div>
            </div>
        </div>
    </footer>

    <!-- database modal -->
    
    <div class="database-modal modal fade" id="aneuploidy_subwindow" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="text-align:right;padding:0px 50px;"><button type="button" class="btn btn-primary btn-lg dismiss" data-dismiss="modal"><i class="fa fa-times"></i> Close </button></div>
                        <div class="col-lg-12">
                            <div class="modal-body" style="text-align:left">
                                <!-- Project Details Go Here -->
                                <h2>Aneuplody</h2>
                                <div class="col-md-12" style='font-size:18px'>
                                <b>Aneuploidy Cell Lines</b><br>Includes the transcriptome and proteome of a couple of cell lines with various aneuploidy status<br><br>
                                <b>Public Human Data</b><br>Contains the expression data published in public online literature
                                <br><br>
                                <div class="col-md-4 col-sm-6 database-item">
                                    <a href="#database-forall" onclick="return createTable('Aneuploidy','Aneuploidy Cell Lines','Human');" class="database-link" data-toggle="modal">
                                        <div class="database-hover">
                                            <div class="database-hover-content">
                                                <i class="fa fa-plus fa-3x"></i>
                                            </div>
                                        </div>
                                        <img src="img/database/mmct.jpg" class="img-responsive" alt="">
                                    </a>
                                    <h4 style="text-align:center">Aneuploidy Cell Lines</h4>
                                </div>
                                <div class="col-md-4 col-sm-6 database-item">
                                    <a href="#database-forall" onclick="return createTable('Aneuploidy_public','Public Human Data','Human');" class="database-link" data-toggle="modal">
                                        <div class="database-hover">
                                            <div class="database-hover-content">
                                                <i class="fa fa-plus fa-3x"></i>
                                            </div>
                                        </div>
                                        <img src="img/database/trisomy21.jpg" class="img-responsive" alt="">
                                    </a>
                                    <h4 style="text-align:center">Public Human Data</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    
    <!--User subwindow 
    <div class="database-modal modal fade" id="user_subwindow" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="text-align:right;padding:0px 50px;"><button type="button" class="btn btn-primary btn-lg dismiss" data-dismiss="modal"><i class="fa fa-times"></i> Close </button></div>
                        <div class="col-lg-12">
                            <div class="modal-body" style="text-align:left">
                                <h2>My Data</h2>
                                <div class="col-md-12" style='font-size:18px'>
                                    <b>Select the organism</b>
                                <br><br>
                                <div class="col-md-4 col-sm-6 database-item">
                                    <a href="#database-forall" onclick="return createTable('User_upload','My Data (Human)','Human');" class="database-link" data-toggle="modal">
                                        <div class="database-hover">
                                            <div class="database-hover-content">
                                                <i class="fa fa-plus fa-3x"></i>
                                            </div>
                                        </div>
                                        <img src="img/database/mydata.jpg" class="img-responsive" alt="">
                                    </a>
                                    <h4 style="text-align:center">My Human Data</h4>
                                </div>
                                <div class="col-md-4 col-sm-6 database-item">
                                    <a href="#database-forall" onclick="return createTable('User_upload','My Data (Mouse)','Mouse');" class="database-link" data-toggle="modal">
                                        <div class="database-hover">
                                            <div class="database-hover-content">
                                                <i class="fa fa-plus fa-3x"></i>
                                            </div>
                                        </div>
                                        <img src="img/database/mydata.jpg" class="img-responsive" alt="">
                                    </a>
                                    <h4 style="text-align:center">My Mouse data</h4>
                                </div>
                                <div class="col-md-4 col-sm-6 database-item">
                                    <a href="#database-forall" onclick="return createTable('User_upload','My Data (Fly)','Fly');" class="database-link" data-toggle="modal">
                                        <div class="database-hover">
                                            <div class="database-hover-content">
                                                <i class="fa fa-plus fa-3x"></i>
                                            </div>
                                        </div>
                                        <img src="img/database/mydata.jpg" class="img-responsive" alt="">
                                    </a>
                                    <h4 style="text-align:center">My Fly Data</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div> 
    -->

    <div class="database-modal modal fade" id="database-forall" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="text-align:right;padding:0px 50px;"><button type="button" class="btn btn-primary btn-lg dismiss" data-dismiss="modal"><i class="fa fa-times"></i> Close </button></div>
                        <div class="col-lg-12">
                            <div class="modal-body" style="text-align:left">
                                <!-- Project Details Go Here -->
                                <h2 id="database-head"></h2>
                                <div class="col-md-12" style='font-size:18px'>
                                <b>Quick Browse</b> <span class="glyphicon glyphicon-eye-open" aria-hidden="true" style="font-size: 1.2em"></span><br>Check out the expression and mutation profile of individual samples in a new window<br><br>
                                <b>Comparative Analysis</b> <span class="glyphicon glyphicon-ok" aria-hidden="true" style="font-size: 1.2em"></span><br>Select up to 6 samples from the table and click compare to start the Data Analysis Portal
                                <br><br>
                                <button id="readygo" class="btn btn-success"> Compare </button>
                                <div style = "display:none;margin-top:10px" id = "warning"></div>
                                </div>
                                <div id="loading" style="text-align:center">
                                <img src="./img/loading.gif">
                                </div>
                                <div class="row" id="userfiles">
                                </div><br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Comparative analysis tutorial -->
    <div class="database-modal modal fade" id="analysis-compare" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
<!--
                                 Project Details Go Here 
-->
                                <div class="col-lg-12">
                                <h2>Comparative Analysis</h2><hr>
                                
                                </div>
                                <br>
                                <div>
									 
                                    <h3>Choose your database</h3>
                                
                                <hr>
                                <br>
                                </div>
								
								<div class="col-lg-6">
									<h4> Human </h4><br>
									<img width="500" height="250" border="10" align="center"  src="img/database/trisomy21.jpg"><br>
                                <a href="compare.php"><button type="button" class="btn btn-primary">GO !</button></a>
                                <hr>
                                </div>
								
								<div class="col-lg-6">
									<h4> Fly (Drosophila melanogaster)</h4><br>
									<img width="500" height="250" border="10" align="center"  src="img/database/drosophila.png"><br>
                                <a href="compareFly.php"><button type="button" class="btn btn-primary">GO !</button></a>
                                <hr>
                                </div>
									
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Upload tutorial -->
    <div class="database-modal modal fade" id="analysis-upload" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <div class="col-lg-12">
                                <h2>Analyse Your Data</h2><hr>
                                </div>
                                <div class="col-lg-9">
                                    <img src="img/analysis/upload1.png" style="max-width:100%;max-height: 100%;">
                                </div>
                                <div class="col-lg-3">
                                    <p>Here are some descriptions on how to upload the data...</p>
<p>Science cuts two ways, of course; its products can be used for both good and evil. But there's no turning back from science. The early warnings about technological dangers also come from science.</p>
<p>What was most significant about the lunar voyage was not that man set foot on the Moon but that they set eye on the earth.</p>
                                </div>                      
                                <div class="col-lg-12">
                                <a href="upload.php"><button type="button" class="btn btn-primary">Upload Data</button></a>&nbsp;&nbsp;<b> OR </b>&nbsp;&nbsp;<a href="#database-user" class="database-link" data-toggle="modal" data-dismiss="modal"><button type="button" class="btn btn-primary" data-dismiss="modal">Check Your Database</button></a> 
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Plugin JavaScript -->
    <script src="js/index/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/index/agency.min.js"></script>
        
    <!--Database Javascript -->
    <script src="js/index/bootstrap-table.min.js"></script>   
    
    <script session-id="<?php echo $id; ?>" host="<?php echo $host; ?>" port="<?php echo $port; ?>" user="<?php echo $user; ?>" passwd="<?php echo $passwd; ?>" unix_socket="<?php echo $unix_socket; ?>" src="js/index/userFiles.js"></script>

</body>

</html>
