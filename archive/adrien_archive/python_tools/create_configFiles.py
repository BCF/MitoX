#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Update Mon 14 June, 15-39

This python scripts build the config files :
- DEGENES-dataHEATMAP.txt
- FALLEN-dataHEATMAP.txt
- HEART-dataHEATMAP.txt

This files are used to run the Rscript for the HEATMAP tools in MitoXplorer.

This scripts parse the different json in the 3 directories about the dataset DEGENES, FALLEN, HEART(Heart-aging).
And write this config files in the R directory then the HeatmapFly.R script can accesss it.


@author: adrien
"""
import os
import json

if __name__ == '__main__':
	

#########
######
######			A GENERIC create_configFILES will be or is already build IF YOU WANT TO PRODUCT A CONFIG FILE ABOUT 
######			A NEW DATASET IN FLY
######
#########
	
	## FOR DEGENES 
	
	# Settings some Dictionary.
	the_title = 'gene\tprocess'
	the_dict = {}
	the_processDict = {}
	
	# THE OUTPUT path
	pathOutputDEGENES = '../../R/DEGENES-dataHEATMAP.txt'
	
	
	# LOOP THROUGH THE FILES IN THE DIRECTORY DEGENES
	stepFile = 0  ### COUNT THE NUMBER OF FILE PARSE THIS VALUE IS USED TO KNOW THE NUMBER OF COLUMN IN THE OUTPUT ARRAY AND OTHER TRICKS
	# Parse the jsonFile in the directory DEGENES ONE BY ONE
	for file in os.listdir('../../data/DEGENES'):
		
		
		# CHECK ABOUT JSON FILES
		if file.endswith(".json") :
			
			
			# SET THE FILE AS AN INPUT FILE PATH FOR READING
			fileDEGENES = (os.path.join('../../data/DEGENES', file))
			pathInputFile = fileDEGENES	
			
			
			# START TO GET THE INFORMATION IN THE JSON FILE
			with open(pathInputFile) as json_file:
				stepFile = stepFile+1 # NEW FILE SO +1 IN THE COUNTING
					
				## Get the data on the JSON
				json_data = json.load(json_file)
				
				## Split by gene information
				json_dataSplitted = str(json_data).split('}, {')
					
					
				## Get the number of genes
				dataLength = len(json_dataSplitted)
				
				# implement the writing title with the sample ID name
				info_gene = json_dataSplitted[0].split(',')
				
				# Get the sample ID name
				sampleID = ((info_gene[2].split(': '))[1]).strip("'")
				the_title = the_title +'\t'+sampleID
					
				## Split each information by gene
				for gene in range(dataLength) :
					info_gene = json_dataSplitted[gene].split(',')
					
					
						
				# Get the geneName
					geneName = ((info_gene[0].split(': '))[1]).strip("'")
					
					# Get the processName
					processName = ((info_gene[3].split(': '))[1]).strip("'")
					if geneName not in the_processDict :
							the_processDict[geneName] = geneName+'\t'+processName
					
					
					log2=''
					for position in range(5) :  ### WE WILL CHECK IN DIFFERENT POSITION POSSIBLE FOR THE LOG2FOLDCHANGE
					# Get the Log2FoldChange value
						if ((((info_gene[position+6].split(': '))[0])) == " 'log2'" ):
							log2 = ((info_gene[position+6].split(': '))[1]).strip("'")
							
					# implement the information for the each gene as a dictionary with a key : gene and a value : list of Log2foldChange
					# THE IMPLEMENTATION DEPENDS IN SOME CONDITION
					# HAS THE GENE ALREADY BEEN WRITE IN THE DICTIONARY.
					#		if not are we in the first file parse or further
					#			if we are further we need to add the fact the gene is not annotated in the previous file(s)
					# THE GENE HAS ALREADY BEEN WRITE IN THE DICTIONARY.
					#		so how many times.
					#			if the gene got 4 informations when we are in the 6th files the 5th files got no information for the gene so we have to implement a NA(not annotated) for the 5 file and the value for the 6th
					#			if the gene got 4 informations when we are in the 7th files the 5th and the 6th files got no information for the gene so we have to implement a NA(not annotated) for the 5th and 6th file and the value for the 6th
					# IF A GENE IS PRESENT IN A FILE BUT the log2folchange got not annotated:
					# we have to check the same conditions.
					# etc....
							
							
							if geneName not in the_dict and stepFile == 1 :
								the_dict[geneName] = log2
							
							elif geneName not in the_dict and stepFile > 1 :
								the_dict[geneName] = 'NA\t'+('\tNA\t')*(int(stepFile)-2)+log2
								
							elif geneName in the_dict :
								
								if len(the_dict[geneName].split('\t')) == 1 :
									if stepFile-1 > len(the_dict[geneName].split('\t')):
										the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
									else  :
										the_dict[geneName] = the_dict[geneName] +'\t'+log2
								
								elif len(the_dict[geneName].split('\t')) > 1 :
								
									if stepFile-1 > len(the_dict[geneName].split('\t')):
										the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
									else  :
										the_dict[geneName] = the_dict[geneName] +'\t'+log2
									
					
					if log2 == ''	:
						log2 = 'NA'
						if geneName not in the_dict and stepFile == 1 :
							the_dict[geneName] = log2
						
						elif geneName not in the_dict and stepFile > 1 :
							the_dict[geneName] = 'NA\t'+('\tNA\t')*(int(stepFile)-2)+log2
							
						elif geneName in the_dict :
							
							if len(the_dict[geneName].split('\t')) == 1 :
								if stepFile-1 > len(the_dict[geneName].split('\t')):
									the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
								else  :
									the_dict[geneName] = the_dict[geneName] +'\t'+log2
							
							elif len(the_dict[geneName].split('\t')) > 1 :
							
								if stepFile-1 > len(the_dict[geneName].split('\t')):
									the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
								else  :
									the_dict[geneName] = the_dict[geneName] +'\t'+log2
									
									
					
					
						
					
					


		#### Write the output for this FILE LIST DEGENES
					
			## write the title then each key : gene and value : list of log2Foldchange 
				
		with open(pathOutputDEGENES,'w') as outputDEGENES :
			outputDEGENES.write(the_title+'\n')
			
			i=0
			
			for elem in the_processDict :
				
				i=i+1
				## CHECK IF THE GENE US PRESENT IN the_dict too and not only in the_processDict and adapt the writing
				if elem not in the_dict :	
					outputDEGENES.write(the_processDict[elem]+'\n') ## IN FACT IT WRITE THE TITLE 
				elif elem in the_dict and len(str(the_dict[elem]).split('\t')) < stepFile:
					outputDEGENES.write(the_processDict[elem]+'\t'+the_dict[elem]+('\tNA')*(stepFile-int(len(str(the_dict[elem]).split('\t'))))+'\n')
				elif elem in the_dict and len(str(the_dict[elem]).split('\t')) == stepFile:
					outputDEGENES.write(the_processDict[elem]+'\t'+the_dict[elem]+'\n')
					
					
					
	print("////////////// \n // The configuration file about DEGENES use as a table in R by HeatmapFly.R is done\n //////////////")

										#################################################
										#################################################
										
#########
######
######			THE FOLLOWING PART ARE COPY OF THE PREVIOUS (JUST PATH CHANGE) AND NOT AS COMMENTED AS THE PREVIOUS
######			A GENERIC create_configFILES will be or is already build IF YOU WANT TO PRODUCT A CONFIG FILE ABOUT 
######			A NEW DATASET IN FLY
#########										
										
										
## FOR FALLEN 
	
	# GET THE LIST OF FILE NAME ON DEGENES, FALLEN, HEART Directory.
	
	# Settings some Library
	the_title = 'gene\tprocess'
	the_dict = {}
	the_processDict = {}
	
	# THE OUTPUT path
	pathOutputFALLEN = '../../R/FALLEN-dataHEATMAP.txt'
	stepFile = 0
	# Parse the jsonFile in the directory DEGENES
	for file in os.listdir('../../data/FALLEN'):
		
		if file.endswith(".json") :
			
			filesFALLEN = (os.path.join('../../data/FALLEN', file))
			
			pathInputFile = filesFALLEN	
			
			
			### WORK ON EACH JSON FROM EACH DIRECTORY, as FILES LIST  ///// DO IT FOR EACH
			
			
				## WORKIN ON A FILE LIST 
				
			# Opening each json File
			
			with open(pathInputFile) as json_file:
				stepFile = stepFile+1
					
				## Get the data on the JSON
				json_data = json.load(json_file)
				
				## Split by gene information
				json_dataSplitted = str(json_data).split('}, {')
					
					
				## Get the number of genes
				dataLength = len(json_dataSplitted)
				
				# implement the writing title with the sample ID name
				info_gene = json_dataSplitted[0].split(',')
				# Get the sample ID name
				sampleID = ((info_gene[2].split(': '))[1]).strip("'")
				the_title = the_title +'\t'+sampleID
					
				## Split each information by gene
				
				for gene in range(dataLength) :
					info_gene = json_dataSplitted[gene].split(',')
					
					
						
				# Get the geneName
					geneName = ((info_gene[0].split(': '))[1]).strip("'")
					
					# Get the processName
					processName = ((info_gene[3].split(': '))[1]).strip("'")
					if geneName not in the_processDict :
							the_processDict[geneName] = geneName+'\t'+processName
					
					
					log2=''
					for position in range(5) :
					# Get the Log2FoldChange value
						if ((((info_gene[position+6].split(': '))[0])) == " 'log2'" ):
							log2 = ((info_gene[position+6].split(': '))[1]).strip("'")
							
							# implement the information for the each gene as a dictionary with a key : gene and a value : list of Log2foldChange
							if geneName not in the_dict and stepFile == 1 :
								the_dict[geneName] = log2
							
							elif geneName not in the_dict and stepFile > 1 :
								the_dict[geneName] = 'NA\t'+('\tNA\t')*(int(stepFile)-2)+log2
								
							elif geneName in the_dict :
								
								if len(the_dict[geneName].split('\t')) == 1 :
									if stepFile-1 > len(the_dict[geneName].split('\t')):
										the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
									else  :
										the_dict[geneName] = the_dict[geneName] +'\t'+log2
								
								elif len(the_dict[geneName].split('\t')) > 1 :
								
									if stepFile-1 > len(the_dict[geneName].split('\t')):
										the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
									else  :
										the_dict[geneName] = the_dict[geneName] +'\t'+log2
									
					
					if log2 == ''	:
						log2 = 'NA'
						if geneName not in the_dict and stepFile == 1 :
							the_dict[geneName] = log2
						
						elif geneName not in the_dict and stepFile > 1 :
							the_dict[geneName] = 'NA\t'+('\tNA\t')*(int(stepFile)-2)+log2
							
						elif geneName in the_dict :
							
							if len(the_dict[geneName].split('\t')) == 1 :
								if stepFile-1 > len(the_dict[geneName].split('\t')):
									the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
								else  :
									the_dict[geneName] = the_dict[geneName] +'\t'+log2
							
							elif len(the_dict[geneName].split('\t')) > 1 :
							
								if stepFile-1 > len(the_dict[geneName].split('\t')):
									the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
								else  :
									the_dict[geneName] = the_dict[geneName] +'\t'+log2
									
									
					
					
						
					
					


		#### Write the output for this FILE LIST DEGENES
					
			## write the title then each key : gene and value : list of log2Foldchange 
				
		with open(pathOutputFALLEN,'w') as outputFALLEN :
			outputFALLEN.write(the_title+'\n')
			
			i=0
			
			for elem in the_processDict :
				
				i=i+1
				if elem not in the_dict :
					outputFALLEN.write(the_processDict[elem]+'\n')
				elif elem in the_dict and len(str(the_dict[elem]).split('\t')) < stepFile:
					outputFALLEN.write(the_processDict[elem]+'\t'+the_dict[elem]+('\tNA')*(stepFile-int(len(str(the_dict[elem]).split('\t'))))+'\n')
				elif elem in the_dict and len(str(the_dict[elem]).split('\t')) == stepFile:
					outputFALLEN.write(the_processDict[elem]+'\t'+the_dict[elem]+'\n')

	print("////////////// \n // The configuration file about FALLEN use as a table in R by HeatmapFly.R is done\n //////////////")



											#########################################
											#########################################

#########
######
######			THE FOLLOWING PART ARE COPY OF THE DEGENES (JUST PATH CHANGE) PART AND NOT AS COMMENTED AS THE DEGENES PART
######			A GENERIC create_configFILES will be or is already build IF YOU WANT TO PRODUCT A CONFIG FILE ABOUT 
######			A NEW DATASET IN FLY
#########


## FOR HEART  (Heart_aging
	
	
	
	# Settings some Library
	the_title = 'gene\tprocess'
	the_dict = {}
	the_processDict = {}
	
	# THE OUTPUT path
	pathOutputDEGENES = '../../R/HEART-dataHEATMAP.txt'
	stepFile = 0
	# Parse the jsonFile in the directory DEGENES
	for file in os.listdir('../../data/HEART'):
		
		if file.endswith(".json") :
			
			fileHEART = (os.path.join('../../data/HEART', file))

	
	
	
			pathInputFile = fileHEART	
			
			
			### WORK ON EACH JSON FROM EACH DIRECTORY, as FILES LIST  ///// DO IT FOR EACH
			
			
				## WORKIN ON A FILE LIST 
				
			# Opening each json File
			
			with open(pathInputFile) as json_file:
				stepFile = stepFile+1
					
				## Get the data on the JSON
				json_data = json.load(json_file)
				
				## Split by gene information
				json_dataSplitted = str(json_data).split('}, {')
					
					
				## Get the number of genes
				dataLength = len(json_dataSplitted)
				
				# implement the writing title with the sample ID name
				info_gene = json_dataSplitted[0].split(',')
				# Get the sample ID name
				sampleID = ((info_gene[2].split(': '))[1]).strip("'")
				the_title = the_title +'\t'+sampleID
					
				## Split each information by gene
				
				for gene in range(dataLength) :
					info_gene = json_dataSplitted[gene].split(',')
					
					
						
				# Get the geneName
					geneName = ((info_gene[0].split(': '))[1]).strip("'")
					
					# Get the processName
					processName = ((info_gene[3].split(': '))[1]).strip("'")
					if geneName not in the_processDict :
							the_processDict[geneName] = geneName+'\t'+processName
					
					
					log2=''
					for position in range(3) :
					# Get the Log2FoldChange value
						if ((((info_gene[position+6].split(': '))[0])) == " 'log2'" ):
							log2 = ((info_gene[position+6].split(': '))[1]).strip("'")
							
							# implement the information for the each gene as a dictionary with a key : gene and a value : list of Log2foldChange
							if geneName not in the_dict and stepFile == 1 :
								the_dict[geneName] = log2
							
							elif geneName not in the_dict and stepFile > 1 :
								the_dict[geneName] = 'NA\t'+('\tNA\t')*(int(stepFile)-2)+log2
								
							elif geneName in the_dict :
								
								if len(the_dict[geneName].split('\t')) == 1 :
									if stepFile-1 > len(the_dict[geneName].split('\t')):
										the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
									else  :
										the_dict[geneName] = the_dict[geneName] +'\t'+log2
								
								elif len(the_dict[geneName].split('\t')) > 1 :
								
									if stepFile-1 > len(the_dict[geneName].split('\t')):
										the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
									else  :
										the_dict[geneName] = the_dict[geneName] +'\t'+log2
										
					
							
					if log2 == ''	:
						log2 = 'NA'
						if geneName not in the_dict and stepFile == 1 :
							the_dict[geneName] = log2
						
						elif geneName not in the_dict and stepFile > 1 :
							the_dict[geneName] = 'NA\t'+('\tNA\t')*(int(stepFile)-2)+log2
							
						elif geneName in the_dict :
							
							if len(the_dict[geneName].split('\t')) == 1 :
								if stepFile-1 > len(the_dict[geneName].split('\t')):
									the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
								else  :
									the_dict[geneName] = the_dict[geneName] +'\t'+log2
							
							elif len(the_dict[geneName].split('\t')) > 1 :
							
								if stepFile-1 > len(the_dict[geneName].split('\t')):
									the_dict[geneName] = the_dict[geneName] +('\tNA')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
								else  :
									the_dict[geneName] = the_dict[geneName] +'\t'+log2
									
									
					
					
						
					
					


		#### Write the output for this FILE LIST HEART
					
			## write the title then each key : gene and value : list of log2Foldchange 
				
		with open(pathOutputDEGENES,'w') as outputDEGENES :
			outputDEGENES.write(the_title+'\n')
			
			i=0
			
			for elem in the_processDict :
				
				i=i+1
				if elem not in the_dict :
					outputDEGENES.write(the_processDict[elem]+'\n')
				elif elem in the_dict and len(str(the_dict[elem]).split('\t')) <= stepFile:
					outputDEGENES.write(the_processDict[elem]+'\t'+the_dict[elem]+('\tNA')*(stepFile-int(len(str(the_dict[elem]).split('\t'))))+'\n')
				elif elem in the_dict and len(str(the_dict[elem]).split('\t')) == stepFile:
					outputDEGENES.write(the_processDict[elem]+'\t'+the_dict[elem]+'\n')
		
	print("////////////// \n // The configuration file about HEART use as a table in R by HeatmapFly.R is done\n //////////////")
