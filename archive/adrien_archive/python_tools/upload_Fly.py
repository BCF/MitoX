#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Last Update Fride Mar 17 2017

NB : THIS FILE IS ONLY USED IN A SCRIPT IN THE Upload DIRECTORY OF MITOXPLORER
	USE THIS FILE IF YOU UNDERSTAND CLEARLY HOW IT WORKS at least
	contact : adrien.bonnard@etu.univ-amu.fr
			  adrienbonnard83@yahoo.fr 

@author: adrien
"""
from __future__ import division
import os.path
import sys
import math
import random


## The rename synonymous function to replace the FB_gene ID by the ID use in our Interactome
def Rename_Synonymous(GeneList, Dictionary) :
	
	
	## Path for the expression file and the synonymous dicitonary file
	pathInputGene= GeneList
	pathInputDictionary='/Users/adrien/Sites/mitox/Download_Tools/synonymous_dictionary/synonymous_dictionary.txt'
	
	fileInputGene = open( pathInputGene, 'r')
	fileInputDictionary = open( pathInputDictionary, 'r')
	
	## Set variable and the dictionary text
	dictionary = fileInputDictionary.readline()
	mydict = {}
	Key_ValueList = dictionary.split(' , ')
	size = len( Key_ValueList)
	
	## Build a dict that will be used in this program for comparison with synonynous.
	for item in range( size) :
		key_value = ( Key_ValueList[ item]).split(' : ')
		
		mydict[key_value[ 0]] = key_value[ 1]
	
	renamedGeneList = ""
	
	#~ print(mydict)
	## Build the dictionary with the key-value  gene : synonymous
	i=0
	while i<= size :
		line = ((fileInputGene.readline()).strip())
		dataGene=line.split('\t')[0]
		
		if i != size :
			if dataGene in mydict :
				renamedGeneList = renamedGeneList + ( mydict[ dataGene] + ':' + dataGene + ',')
		else : 
			if dataGene in mydict :
				renamedGeneList = renamedGeneList + ( mydict[ dataGene] + ':' + dataGene)
		i+=1
	
	
	fileInputGene.close()
	fileInputDictionary.close()
	
	#~ print(renamedGeneList)
	## Return the dictionary with key-value gene : synonymous
	return renamedGeneList


if __name__ == '__main__':
	
	
	### Create the synonymous List from my dictionary for the known gene in our interactome
	SynonymousText = ( Rename_Synonymous( '/Users/adrien/Sites/mitox/Download_Tools/Fly_archive/expression_dataset/DEgenes_pairwiseComp.txt', 'synonymous_dictionary.txt '))
	
	pathExpression = '/Users/adrien/Sites/mitox/Download_Tools/Fly_archive/expression_dataset/DEgenes_pairwiseComp.txt'
	pathGeneProcess='/Users/adrien/Sites/mitox/Download_Tools/Fly_archive/gene_process.txt' ## Will use the gene_process file dependent of the gene in the interactome
	pathGeneFunction='/Users/adrien/Sites/mitox/Download_Tools/Fly_archive/gene_function.txt'
	
	SynonymousList = SynonymousText.split( ',')
	
	
	
	
	### Build a dictionary with the SynonymousText : each synonymous will give the value of the corect gene we want
	synonymousDictionary = {}
	
	for gene in range( len( SynonymousList)) :
		SynonymousListSplitted = ( SynonymousList[ gene]).split(':')
		synonymous = SynonymousListSplitted[ 1]
		geneStandard = SynonymousListSplitted[ 0]
		synonymousDictionary[ synonymous] = geneStandard
	
		
	
	### Build a dictionary with the list of Gene_Process 
	processDictionary ={}
	
	sizeProcess = 0
	with open( pathGeneProcess) as fileGeneProcess :
		sizeProcess = len( fileGeneProcess.readlines())
		fileGeneProcess.close()
	
	with open( pathGeneProcess) as fileGeneProcess :
		for item in range( sizeProcess) :
			line = ((fileGeneProcess.readline()).strip()).split(' : ')
			gene = line[0]
			process = line[1]
			processDictionary[ gene] = process
	fileGeneProcess.close()
	
	gene_functionDictionary = {}
	sizeGene_function = 0
	with open( pathGeneFunction) as fileGeneFunction :
		sizeGene_function = len( fileGeneFunction.readlines())
		fileGeneFunction.close()
	
	with open( pathGeneFunction) as fileGeneFunction :
		for item in range( sizeGene_function) :
			line = ((fileGeneFunction.readline()).strip()).split('\t',1)
			gene = (line[0].replace('\t',' , '))
			function = (line[1].replace('\t','')).replace('-','')
			gene_functionDictionary[ gene] = function
	fileGeneFunction.close()
	
	print(gene_functionDictionary)
	### The Loop to build the json file for each line
	
	
	
	with open( pathExpression) as fileExpression:
		sizefileExpression = len( fileExpression.readlines())
		fileExpression.close()
	
	with open( pathExpression) as fileExpression:
		fileExpression.readline()
		
		pathOutput = '/Users/adrien/Sites/mitox/data/user_uploads/user_dataFly/'+ str(sys.argv[3]) +'.json'
		
		fileOutput = open( pathOutput, 'w')
		added = 0
		the_writing = '[\n'
		
		
		for gene in range( sizefileExpression-1) :
			data = fileExpression.readline().split('\t')
			
			
			geneName = data[0]
			log2FoldChange = (data[2]).replace(',','.')
			
			if "6" != 'None' :
				pvalue = (data[5]).replace(',','.')
			else : pvalue = ""
			
			if '2' != 'None' :
				baseMean = (data[1]).replace(',','.')
			else : baseMean = ""
			
			if 'None' != 'None' and 'None'!='None' :
				wt = (data[5]).replace(',','.')
				Abnormal = (data[6]).replace(',','.')
			else : 
				wt = ""
				Abnormal =""
			#~ print(geneName)
			#~ print(log2FoldChange)
			#~ print(pvalue)
			#~ print(baseMean +'\n')
			
			
			
			#~ if geneName in synonymousDictionary :
				#~ print( synonymousDictionary[ geneName])
			
			
			if geneName in synonymousDictionary and synonymousDictionary[ geneName] in processDictionary :
				keyDictionary = synonymousDictionary[ geneName]
				process = processDictionary[ synonymousDictionary[ geneName]]
				gene_function = gene_functionDictionary[ synonymousDictionary[ geneName]]
				print(gene_function)
				
				if gene_function != "" :
					if log2FoldChange != 'NA' and pvalue != 'NA' and baseMean != 'NA' :
						the_writing = the_writing+('{ \n"gene"'+ ': "'+synonymousDictionary[ geneName]+'",'+'\n"FlybaseID"'+': "'+geneName+'",' +'\n"sampleID"'+ ': "'+str(sys.argv[3])+'",' + '\n"process": "' + process+'",'+'\n"gene_function":  "' + gene_function +'",'+'\n"base_Mean": "' + str(baseMean) +'",' + '\n"log2": ' + log2FoldChange +','+ '\n"pvalue": ' + pvalue + ','+'\n"WT expression": "' + str(wt) + '",' + '\n"Abnormal": "' + str(Abnormal) +'",' +'\n"mutation": ""'+ '\n}\n')
						added +=1
					
				else :
					gene_function = 'Unknow gene_function'
					if log2FoldChange != 'NA' and pvalue != 'NA' and baseMean != 'NA' :
						the_writing = the_writing+('{ \n"gene"'+ ': "'+synonymousDictionary[ geneName]+'",'+'\n"FlybaseID"'+': "'+geneName+'",' +'\n"sampleID"'+ ': "'+str(sys.argv[3])+'",' + '\n"process": "' + process+'",'+'\n"gene_function":  "' + gene_function +'",'+'\n"base_Mean": "' + str(baseMean) +'",' + '\n"log2": ' + log2FoldChange +','+ '\n"pvalue": ' + pvalue + ','+'\n"WT expression": "' + str(wt) + '",' + '\n"Abnormal": "' + str(Abnormal) +'",' +'\n"mutation": ""'+ '\n}\n')
						added +=1
							
				
					
		if the_writing != '[\n' :
			the_writing = the_writing+("]")
			the_writing = the_writing.replace( "}", "},", added-1)
			fileOutput.write( str( the_writing))
			fileOutput.close()
				
			print('>>>>> Data about DEgen have been write in the json format file //////\n')
	
		fileExpression.close()	
