#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Last Update 
06-06-2017

This is a python script used to build the json file about the dataset FALLEN,
Comparison between knockout Fly for the transcriptional factor gene FALLEN against WT, in flight muscle development
at two different timing.

5 Arguments : 
- the geneExpression file about DEGENEin the fly_archive directory
- the synonymousDictionary file in the synonymous directory
- the name of the output json wanted 
- If there is a column in the expression file for wt : give the column number
- If there is a column in the expression file for the abnormal : give the column number



NB : THIS FILE IS ONLY USED IN A SCRIPT IN THE BIN DIRECTORY OF MITOXPLORER
	USE THIS FILE IF YOU UNDERSTAND CLEARLY HOW IT WORKS 
	contact : adrien.bonnard@etu.univ-amu.fr
			  adrienbonnard83@yahoo.fr 

@author: adrien
"""
from __future__ import division
import os.path
import sys
import math
import random


## The rename synonymous function to replace the FB_gene ID by the ID use in our Interactome
def Rename_Synonymous(GeneList, Dictionary) :
	
	
	## Path for the expression file and the synonymous dicitonary file
	pathInputGene= '../Fly_archive/expression_dataset/DGRP_Bianca_dataset/'+GeneList
	pathInputDictionary='../synonymous_dictionary/'+Dictionary
	
	## Open the different files : the one with the geneName we have to rename, 
	# and a second with synonymous dictionary .
	fileInputGene = open( pathInputGene, 'r')
	fileInputDictionary = open( pathInputDictionary, 'r')
	
	## Set variable and the dictionary text
	dictionary = fileInputDictionary.readline()
	mydict = {}
	Key_ValueList = dictionary.split(' , ') # we split the line in the ' , ' because the file is write as a dictionary 
	size = len( Key_ValueList)
	
	## Build a dict that will be used in this program to rename the synonynous.
	for item in range( size) :
		key_value = ( Key_ValueList[ item]).split(' : ')
		
		mydict[key_value[ 0]] = key_value[ 1]
	
	
	## Build the dictionary with the key-value  gene : synonymous
	renamedGeneList = ""
	i=0
	while i<= size :
		line = ((fileInputGene.readline()).strip())
		dataGene=line.split(';')[0] # The geneList is supposed to be a csv file and the geneID in the first column
			
		# Still we are in th last element in the list of gene implement the renamed List as follow
		if i != size :
			if dataGene in mydict :
				renamedGeneList = renamedGeneList + ( mydict[ dataGene] + ':' + dataGene + ',')
		
		# If it's the last element implement the renamed list as follow
		else : 
			if dataGene in mydict :
				renamedGeneList = renamedGeneList + ( mydict[ dataGene] + ':' + dataGene)
		i+=1
	

	fileInputGene.close()
	fileInputDictionary.close()
	
	## Return the dictionary with key-value gene : synonymous
	return renamedGeneList # This list will be used to rename the geneID correctly


if __name__ == '__main__':
	
	
	### Create the synonymous List from my dictionary for the known gene in our interactome
	SynonymousText = ( Rename_Synonymous( sys.argv[ 1], sys.argv[ 2]))
	
	## Create the Path to different files use
	pathExpression = '../Fly_archive/expression_dataset/DGRP_Bianca_dataset/'+str(sys.argv[ 1])
	pathGeneProcess='../Fly_archive/gene_process.txt' ## Will use the gene_process file dependent of the gene in the interactome
	pathGeneFunction='../Fly_archive/gene_function.txt'
	
	
	## With the renamedGeneList build by th Rename_Synonymous() function we build the synonymousList 
	SynonymousList = SynonymousText.split( ',')
	
	
	### Build a dictionary with the SynonymousText : each synonymous will give the value of the corect gene we want
	synonymousDictionary = {}
	
	for gene in range( len( SynonymousList) - 1) :
		SynonymousListSplitted = ( SynonymousList[ gene]).split(':')
		synonymous = SynonymousListSplitted[ 1]
		geneStandard = SynonymousListSplitted[ 0]
		synonymousDictionary[ synonymous] = geneStandard
	
		
	
	### Build a dictionary with the list of Gene_Process 
	processDictionary ={}
	
	sizeProcess = 0
	with open( pathGeneProcess) as fileGeneProcess :
		sizeProcess = len( fileGeneProcess.readlines())
		fileGeneProcess.close()
	
	with open( pathGeneProcess) as fileGeneProcess :
		for item in range( sizeProcess) :
			line = ((fileGeneProcess.readline()).strip()).split(' : ')
			gene = line[0]
			process = line[1]
			processDictionary[ gene] = process
	fileGeneProcess.close()
	
	
	### Build a dictionary with the list of function for each gene
	gene_functionDictionary = {}
	sizeGene_function = 0
	with open( pathGeneFunction) as fileGeneFunction :
		sizeGene_function = len( fileGeneFunction.readlines())
		fileGeneFunction.close()
	
	with open( pathGeneFunction) as fileGeneFunction :
		for item in range( sizeGene_function) :
			line = ((fileGeneFunction.readline()).strip()).split('\t',1)
			gene = (line[0].replace('\t',' , '))
			function = (((line[1].replace('\t','')).replace('-','')).split('|', 1))[0]
			gene_functionDictionary[ gene] = function
	fileGeneFunction.close()
	

#######	
	### THEN WRITE THE JSON FILE
	
	with open( pathExpression) as fileExpression:
		sizefileExpression = len( fileExpression.readlines())
		fileExpression.close()
	
	with open( pathExpression) as fileExpression:
		fileExpression.readline()
		
		## The path used as output
		pathOutput = '../../data/FALLEN/'+ sys.argv[3] +'.json'
		
		fileOutput = open( pathOutput, 'w')
		added = 0
		the_writing = '[\n' # The initiation of what will be write in the output
	
		
		## Begin to Parse the geneExpression file
		for gene in range( sizefileExpression-1) :
			# Get the data for each line
			# Read and split each data in the line
			data = fileExpression.readline().split(';')
			
			# Set the different information we want in the data
			geneName = data[0]
			log2FoldChange = (data[3]).replace(',','.')
			
			if data[6]!= 'None' :
				pvalue = (data[6]).replace(',','.')
			else : pvalue = ""
			
			if data[2] != 'None' :
				baseMean = (data[2]).replace(',','.')
			else : baseMean = ""
			
			wt = ""
			Abnormal =""
			
			# Check if it's a mito-associated gene to complete the writing string
			if geneName in synonymousDictionary and synonymousDictionary[ geneName] in processDictionary :
				# Set the correct GeneID with the dictionary build previously, the process and the gene_function
				keyDictionary = synonymousDictionary[ geneName]
				process = processDictionary[ synonymousDictionary[ geneName]]
				gene_function = gene_functionDictionary[ synonymousDictionary[ geneName]]
				
				#Check if we know the function of the gene before continue :
				if gene_function != "" :
					# Implement the future writing with the data of this gene as a Json format
					if log2FoldChange != 'NA' and pvalue != 'NA' and baseMean != 'NA' :
						the_writing = the_writing+('{ \n"gene"'+ ': "'+synonymousDictionary[ geneName]+'",'+'\n"FlybaseID"'+': "'+geneName+'",' +'\n"sampleID"'+ ': "'+sys.argv[3]+'",' + '\n"process": "' + process+'",'+'\n"gene_function":  "' + gene_function +'",'+'\n"base_Mean": "' + str(baseMean) +'",' + '\n"log2": ' + log2FoldChange +','+ '\n"pvalue": ' + pvalue + ','+'\n"WT expression": "' + str(wt) + '",' + '\n"Abnormal": "' + str(Abnormal) +'",' +'\n"mutation": ""'+ '\n}\n')
						added +=1
				
				# We didn't know the gene_function so :	
				else :
					gene_function = 'Unknow gene_function'
					# Implement the future writing with the data of this gene as a Json format
					if log2FoldChange != 'NA' and pvalue != 'NA' and baseMean != 'NA' :
						the_writing = the_writing+('{ \n"gene"'+ ': "'+synonymousDictionary[ geneName]+'",'+'\n"FlybaseID"'+': "'+geneName+'",' +'\n"sampleID"'+ ': "'+sys.argv[3]+'",' + '\n"process": "' + process+'",'+'\n"gene_function":  "' + gene_function +'",'+'\n"base_Mean": "' + str(baseMean) +'",' + '\n"log2": ' + log2FoldChange +','+ '\n"pvalue": ' + pvalue + ','+'\n"WT expression": "' + str(wt) + '",' + '\n"Abnormal": "' + str(Abnormal) +'",' +'\n"mutation": ""'+ '\n}\n')
						added +=1
							
				
		# Write the output with the data compute as a String in the JSON format				
		if the_writing != '[\n' :
			the_writing = the_writing+("]")
			the_writing = the_writing.replace( "}", "},", added-1)
			fileOutput.write( str( the_writing))
			fileOutput.close()
				
			print('\n >>>>> Data about '+ str(sys.argv[3]) +' have been write in the json format file //////\n')
	
		
