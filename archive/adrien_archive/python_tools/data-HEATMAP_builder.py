#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Update Mon 14 June, 15-39

This python scripts build the config file used by HeatmapFly.R

This script can be execute on any directory dataset with JSON files holding the data to analyse.

The output file will used to run the Rscript for the HEATMAP tools in MitoXplorer.

This scripts parse the different json in the directory 
And write the config file in the R directory then the HeatmapFly.R script can accesss it.

		---------------------------------------------------------------------------------------------------------------------
			THIS SCRIPT IS LAUNCH WITH ONE ARGUMENT ABOUT THE DIRECTORY YOU TARGET IN THE DATA DIRECTORY IN MITOX :
				exemples : 
					- DEGENES
					- FALLEN
					- HEART
			THIS ARE SOME POSSIBLE ARGUMENTS YOU CAN USE WITH THIS SCRIPT BECAUSE THIS DIRECTORIES HOLD SOME JSON DATA ABOUT 
			FLY GENE IN JSON FILE FORMAT	
		---------------------------------------------------------------------------------------------------------------------

@author: Adrien BONNARD 
mail : adrien.bonnard@etu.univ-amu.fr
"""

## IMPORT
import os
import json
import sys

if __name__ == '__main__':
	
	
	# Settings some Dictionary.
	the_title = 'gene\tprocess' # The first line in the output
	the_dict = {}				# will hold the data about each gene
	the_processDict = {}		# will hold the key-value GeneName-Process
	
	# THE OUTPUT path
	pathOutput = '../../R/'+str(sys.argv[1])+'-dataHEATMAP.txt'
	
	
	# LOOP THROUGH THE FILES IN THE DIRECTORY DEGENES
	stepFile = 0  ### COUNT THE NUMBER OF FILE PARSE THIS VALUE IS USED TO KNOW THE NUMBER OF COLUMN IN THE OUTPUT ARRAY AND OTHER TRICKS
	# Parse the jsonFile in the directory ONE BY ONE
	for file in os.listdir('../../data/'+sys.argv[1]):
		
		
		# ACCEPT ONLY JSON FILES 
		if file.endswith(".json") :
			
			
			# SET THE FILE AS AN INPUT FILE PATH FOR READING
			fileDEGENES = (os.path.join('../../data/'+sys.argv[1], file))
			pathInputFile = fileDEGENES	
			
			
			# START TO GET THE INFORMATION IN THE JSON FILE
			with open(pathInputFile) as json_file:
				stepFile = stepFile+1 # NEW FILE SO +1 IN THE COUNTING
					
				## Get the data on the JSON
				json_data = json.load(json_file)
				
				## Split by gene information
				json_dataSplitted = str(json_data).split('}, {')
					
					
				## Get the number of genes
				dataLength = len(json_dataSplitted)
				
				# implement the writing title with the sample ID name
				info_gene = json_dataSplitted[0].split(',')
				
				# Get the sample ID name
				sampleID = ((info_gene[2].split(': '))[1]).strip("'")
				the_title = the_title +'\t'+sampleID
					
				## Split each information by gene
				for gene in range(dataLength) :
					info_gene = json_dataSplitted[gene].split(',')
					
					
						
				# Get the geneName
					geneName = ((info_gene[0].split(': '))[1]).strip("'")
					
					# Get the processName
					processName = ((info_gene[3].split(': '))[1]).strip("'")
					if geneName not in the_processDict :
							the_processDict[geneName] = geneName+'\t'+processName
					
					
					log2=''
					for position in range(5) :  ### WE WILL CHECK IN DIFFERENT POSITION POSSIBLE FOR THE LOG2FOLDCHANGE
					# Get the Log2FoldChange value
						if ((((info_gene[position+6].split(': '))[0])) == " 'log2'" ):
							log2 = ((info_gene[position+6].split(': '))[1]).strip("'")
					
					# THE FOLLOWING PART DESCRIPTION :
					#
					#		
					# 	implement the information for the each gene as a dictionary with a key : gene and a value : list of Log2foldChange
					# 	THE IMPLEMENTATION DEPENDS IN SOME CONDITION
					# 	HAS THE GENE ALREADY BEEN WRITE IN THE DICTIONARY.
					#			if not are we in the first file parse or further
					#				if we are further we need to add the fact the gene is not annotated in the previous file(s)
					# 	THE GENE HAS ALREADY BEEN WRITE IN THE DICTIONARY.
					#			so how many times.
					#				if the gene got 4 informations when we are in the 6th files the 5th files got no information for the gene so we have to implement a 0(not annotated) for the 5 file and the value for the 6th
					#				if the gene got 4 informations when we are in the 7th files the 5th and the 6th files got no information for the gene so we have to implement a 0(not annotated) for the 5th and 6th file and the value for the 6th
					# 	IF A GENE IS PRESENT IN A FILE BUT the log2folchange got not annotated:
					# 	we have to check the same conditions.
					# 	etc....
							
							
											#######################################
							# For the first File
							if geneName not in the_dict and stepFile == 1 :
								the_dict[geneName] = log2	# input the data of this file about this gene GeneName
							
							# If it's not the first file the conditions changed
							
							# If the geneName wasn't in the previous file
							elif geneName not in the_dict and stepFile > 1 :
								the_dict[geneName] = '0'+('\t0')*(int(stepFile)-2)+'\t'+log2
							
							# If the geneName was in the previous file	
							elif geneName in the_dict :
								
								
								if len(the_dict[geneName].split('\t')) == 1 :
									if stepFile-1 > len(the_dict[geneName].split('\t')):
										the_dict[geneName] = the_dict[geneName] +('\t0')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
									else  :
										the_dict[geneName] = the_dict[geneName] +'\t'+log2
								
								elif len(the_dict[geneName].split('\t')) > 1 :
								
									if stepFile-1 > len(the_dict[geneName].split('\t')):
										the_dict[geneName] = the_dict[geneName] +('\t0')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
									else  :
										the_dict[geneName] = the_dict[geneName] +'\t'+log2
									
					# When a file don't have log2 value but the gene is present in the file
					if log2 == ''	:
						log2 = '0'
						if geneName not in the_dict and stepFile == 1 :
							the_dict[geneName] = log2
						
						elif geneName not in the_dict and stepFile > 1 :
							the_dict[geneName] = '0'+('\t0')*(int(stepFile)-2)+'\t'+log2
							
						elif geneName in the_dict :
							
							if len(the_dict[geneName].split('\t')) == 1 :
								if stepFile-1 > len(the_dict[geneName].split('\t')):
									the_dict[geneName] = the_dict[geneName] +('\t0')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
								else  :
									the_dict[geneName] = the_dict[geneName] +'\t'+log2
							
							elif len(the_dict[geneName].split('\t')) > 1 :
							
								if stepFile-1 > len(the_dict[geneName].split('\t')):
									the_dict[geneName] = the_dict[geneName] +('\t0')*(stepFile-(1+len(the_dict[geneName].split('\t'))))+'\t'+log2
								else  :
									the_dict[geneName] = the_dict[geneName] +'\t'+log2
									
									
													#### DESSCIPTION OF THIS PART IN TOP ####
					
						
					
					


		#### Write the output for this FILE LIST 
					
			## write the title then each key : gene and value : list of log2Foldchange 
				
		with open(pathOutput,'w') as output :
			output.write(the_title+'\n')
			
			for elem in the_processDict :
				
				## CHECK IF THE GENE USE PRESENT IN the_dict too and not only in the_processDict and adapt the writing
				if elem not in the_dict :	
					output.write(the_processDict[elem]+'\n') ## IN FACT IT WRITE THE TITLE 
				elif elem in the_dict and len(str(the_dict[elem]).split('\t')) < stepFile:
					output.write(the_processDict[elem]+'\t'+the_dict[elem]+('\t0')*(stepFile-int(len(str(the_dict[elem]).split('\t'))))+'\n')
				elif elem in the_dict and len(str(the_dict[elem]).split('\t')) == stepFile:
					output.write(the_processDict[elem]+'\t'+the_dict[elem]+'\n')
					
					
					
	print("////////////// \n // The configuration file about "+sys.argv[1]+" use as a table in R by HeatmapFly.R is done\n //////////////")

										#################################################
										#################################################
