#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Last Update 
27-07-2017

This is a python script used to build the "gene process chr function ENSG" file to run
the tools on Fly data.


	contact : adrien.bonnard@etu.univ-amu.fr
			  adrienbonnard83@yahoo.fr 

@author: adrien
"""


if __name__ == "__main__":
	
    
    gene_pathway_PATH = "../Fly_archive/gene_process.txt"
    gene_function_PATH = "../Fly_archive/gene_function.txt"
    
    gene_output_PATH = "../../Fly_gene-function.txt"
    
    output = "gene\tprocess\tchr\tfunction\tENSG\n"
    output =""
    data_pathway = {}
    data_function = {}
    with open(gene_pathway_PATH, 'r') as file_gene_pathway :
        lines_pathway = []
        lines_pathway = file_gene_pathway.readlines()
        for line in lines_pathway :
            gene = (line.split(' : ')[0]).strip()
            pathway = (line.split(' : ')[1]).strip()
            if gene not in data_pathway :
                data_pathway[gene]= pathway
   
    
    with open(gene_function_PATH, 'r') as file_gene_function :
        lines_function = []
        lines_function = file_gene_function.readlines()
        for line in lines_function :
            gene = (line.split('\t')[0]).strip()
            function = ((line.split('\t')[1]).split('|')[0]).strip()
            if gene not in data_function :
                data_function[gene]= function
    
    
    
    for gene in data_pathway :
        if gene in data_function :
            output = output + gene +'\t'+ data_pathway[gene]+ '\tNA\t' + data_function[gene]+'\tNA\n'
   
    with open(gene_output_PATH, 'w') as file_gene_output :
        file_gene_output.write(output)