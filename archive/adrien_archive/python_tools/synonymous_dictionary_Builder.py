#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 14:38:03 2017

This file product a dictionary with the synonysm of a list of gene.
Firstly this file was used for a DGRP Gene submit on Flybase producting a list of synonymous for each gene.

But we made it most flexible.

You need to set the file in the correct directories :
We give a file with the Gene List with a gene per line ### Put in the Fly_archive directory
And a file the synonymous list of a gene per line.     ### Put in the Flybase_synonymous directory

You give the name of the output file ### The file will be put in the synonymous_dictionary directory

Then this program will build in an other file all the combination of the synonymous and the GENE NAME we want.
The Purpose is to be able to rename each gene in an expression file by the correct GENE NAME to merge the 
synonymous in one same name.

@author: adrien
"""
import sys

if __name__ == '__main__':
	
	### THE PATHs: the geneList, the SynonymousLists, the output File.
	pathInputValue = '../Fly_archive/'+ str( sys.argv[1]) ## Give the gene list file
	
	pathInputKey ='../Flybase_synonymous/'+ str( sys.argv[2]) ## Give the synonynous list from flybase
	## We product the synonymous list with the Tools 'Batch Download' in Flybase.org
	# For it, we used a Gene List modified in the directory Fly_archive 
	# it was modify to don't use unknow ID for FB and find all the synonymous in all others database. 
	# the file name is 'Gene_ID_for_FB.txt' just some gene in 'Gene_r554.txt' 
	# have been renamed by their NCBI ID.
	
	pathOutput ='../synonymous_dictionary/' + str( sys.argv[3]) ## Give the output file name
	
	### Opening of the differents input and output file
	fileInputKey = open( pathInputKey, 'r')
	fileOutput = open( pathOutput, 'w')
	
	
	## We build a file wre each synonymous will be a key for a value
	# The value will be the Gene Name we want for our database
	# Then All the synonymous will be able to merge to the same value => The Gene Name we want
	with open( pathInputValue) as fileInputValue :
		the_output = ""		## The string that will be write in the output
		for line in fileInputValue :
			if the_output != "" :
				the_output = the_output + ' , '	## will be add to each line but not the last one
			dataValue = ((str( line.strip())).replace( 'mt:' , 'mt_')).replace( 'mt ', 'mt_')
				
			dataKey = ((( ( fileInputKey.readline()).strip()).replace ( '<newline>', '\t'))) ## we replace the newline tag build by the Batch Download tools on Flybase
			dataKeySplitted = ( dataKey.split( '\t'))
			size = len( dataKeySplitted)
			
			for value in range( size) :
				if the_output != "" and the_output[-2] != ',' : ## Conditions to not add two ',' in a row
					the_output = the_output + ' , '	## will be add to each line but not the last one
				if dataKeySplitted[ value] != "-" : 
					the_output = the_output + ( str( dataKeySplitted[ value]) + ' : ' + dataValue)
		fileOutput.write( the_output)			
	
	### Close all the files
	fileInputValue.close()
	fileInputKey.close()
	fileOutput.close()

