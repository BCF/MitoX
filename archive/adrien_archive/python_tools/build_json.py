#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 6 11:28 2017
Last Update 
06-06-2017

This is a python script used to build the json file about DGRP lines from a PNAS article : Huang & al 2015 PNAS
We used the summarized expression of each line and made a comparison against the average of all the lines.

3 Arguments :
- The gene Expression file from huang about the CORRECT SEX you want in txt format
- the synonymous dictionary in the synonymous directory
- the name of the mean expression file this script will build to compare at each different lines of DGRP
- the DGRP sex of the Fly in the gene expression file 

COMMAND LINE :
-------- DGRP Male
python3 build_json.py Expression_DGRP_male_Huang.txt synonymous_dictionary.txt MeanExpression Male


-------- DGRP Female 
python3 build_json.py Expression_DGRP_female_Huang.txt synonymous_dictionary.txt MeanExpression Female


NB : THIS FILE IS ONLY USED IN A SCRIPT IN THE BIN DIRECTORY OF MITOXPLORER
	USE THIS FILE IF YOU UNDERSTAND CLEARLY HOW IT WORKS at least
	contact : adrien.bonnard@etu.univ-amu.fr
			  adrienbonnard83@yahoo.fr 

@author: adrien
"""

from __future__ import division
import os.path
import sys
import math
import random


## The rename synonymous function to replace the FB_gene ID by the ID use in our Interactome
def Rename_Synonymous(GeneList, Dictionary) :
	
	
	## Path for the expression file and the synonymous dicitonary file
	pathInputGene= '../Fly_archive/expression_dataset/'+GeneList
	pathInputDictionary='../synonymous_dictionary/'+Dictionary
	
	## Open the different files : the one with the geneName we have to rename, 
	# and a second with synonymous dictionary .
	fileInputGene = open( pathInputGene, 'r')
	fileInputDictionary = open( pathInputDictionary, 'r')
	
	## Set variable and the dictionary text
	dictionary = fileInputDictionary.readline()
	mydict = {}
	Key_ValueList = dictionary.split(' , ')  # we split the line in the ' , ' because the file is write as a dictionary 
	size = len( Key_ValueList)
	
	## Build a dict that will be used in this program for comparison with synonynous.
	for item in range( size) :
		key_value = ( Key_ValueList[ item]).split(' : ')
		
		mydict[key_value[ 0]] = key_value[ 1]
	

	## Build the dictionary with the key-value  gene : synonymous
	renamedGeneList = ""
	i=0
	while i<= size :
		line = ((fileInputGene.readline()).strip()).replace(" ",",")
		dataGene=line.split(',')[0]
		
		if i != size :
			if dataGene in mydict :
				renamedGeneList = renamedGeneList + ( mydict[ dataGene] + ':' + dataGene + ',')
		else : 
			if dataGene in mydict :
				renamedGeneList = renamedGeneList + ( mydict[ dataGene] + ':' + dataGene)
		i+=1
	
	
	fileInputGene.close()
	fileInputDictionary.close()
	
	
	## Return the dictionary with key-value gene : synonymous
	return renamedGeneList # This list will be used to rename the geneID correctly
	
	 
if __name__ == '__main__':
	
	### Create the synonymous List from my dictionary for the known gene in our interactome
	SynonymousText = ( Rename_Synonymous( sys.argv[ 1], sys.argv[ 2]))
	
	## Create the Path to different files use
	pathExpression = '../Fly_archive/expression_dataset/'+ str( sys.argv[ 1])
	pathMeanExpression= '../Fly_archive/expression_dataset/'+str(sys.argv[ 4])+'_'+str(sys.argv[ 3])
	pathGeneProcess='../Fly_archive/gene_process.txt' ## Will use the gene_process file dependent of the gene in the interactome
	pathGeneFunction='../Fly_archive/gene_function.txt'
	
	# Set the DGRP sex
	DGRP_sex = (sys.argv[ 4])
	
	## With the renamedGeneList build by th Rename_Synonymous() function we build the synonymousList 
	SynonymousList = SynonymousText.split( ',')
	
	
	### Build a dictionary with the SynonymousText : each synonymous will give the value of the corect gene we want
	synonymousDictionary = {}
	for gene in range( len( SynonymousList) - 1) :
		SynonymousListSplitted = ( SynonymousList[ gene]).split(':')
		synonymous = SynonymousListSplitted[ 1]
		geneStandard = SynonymousListSplitted[ 0]
		synonymousDictionary[ synonymous] = geneStandard
		
	
	### Build a dictionary with the list of Gene_Process
	processDictionary ={}
	sizeProcess = 0
	with open( pathGeneProcess) as fileGeneProcess :
		sizeProcess = len( fileGeneProcess.readlines())
		fileGeneProcess.close()
	
	with open( pathGeneProcess) as fileGeneProcess :
		for item in range( sizeProcess) :
			line = ((fileGeneProcess.readline()).strip()).split(' : ')
			gene = line[0]
			process = line[1]
			processDictionary[ gene] = process
	fileGeneProcess.close()
	
	### Build a dictionary with the list of function for each gene
	gene_functionDictionary = {}
	sizeGene_function = 0
	with open( pathGeneFunction) as fileGeneFunction :
		sizeGene_function = len( fileGeneFunction.readlines())
		fileGeneFunction.close()
	
	with open( pathGeneFunction) as fileGeneFunction :
		for item in range( sizeGene_function) :
			line = ((fileGeneFunction.readline()).strip()).split('\t',1)
			gene = (line[0].replace('\t',' , '))
			function = (((line[1].replace('\t','')).replace('-','')).split('|', 1))[0]
			gene_functionDictionary[ gene] = function
	fileGeneFunction.close()
	
	
	## Build the list of gene in the Expression File
	geneList = []  ### Will hold the list of gene in the Expression File
	geneExpList = [] ### will hold the list of expression per line for each gene
	linesList = [] ### will hold the list of the lines
	
	
	# Build the gene expression list
	sizeGeneExpression = 0
	with open( pathExpression) as fileExpression :
		sizeGeneExpression = len(fileExpression.readlines())
		fileExpression.close()
	
	with open( pathExpression) as fileExpression :
		linesList = ((fileExpression.readline()).strip()).split( ' ')
		for item in range( sizeGeneExpression-1) :
			line = ((fileExpression.readline()).strip()).split( ' ')
			
			geneList.append(line[0]) # get just the GeneID in the expression file to build the gene list
			geneExpList.append(line) # append in geneExplist all the information about this gene
			
		
	
	
	# set some variable
	sizeLinesList = len(linesList)
	sizeGeneExpList = len(geneExpList)
	
	## Build the File with the Mean Expression of each gene per line if doesn't exist
	if os.path.isfile( pathMeanExpression) == False :  ### Will only build the mean file if it doesn't already exist
		fileMeanExpression = open( pathMeanExpression, 'w')
		
		### Create the file with the mean expression for each gene then he will never be ran again if the file already exist.
		the_output = ""
		for line in range(sizeGeneExpList) :
			sumExp = 0
			counts = 0
			lineGeneExpression = geneExpList[line]
			gene_Name = lineGeneExpression[ 0]
			
			for sample in range(sizeLinesList-1) :
				sumExp = sumExp + float(lineGeneExpression[ sample+1])
				counts = counts + 1
			mean = sumExp/counts
			
			the_output = the_output + gene_Name + ' : ' + str (mean) + '\n'
		
		fileMeanExpression.write( the_output)
		fileMeanExpression.close()
		
	
	## Build the Dictionary of the mean	with the content in the file with the mean expression per genes for the correct sex		
	geneMeanDictionary = {}
	with open ( pathMeanExpression) as fileMeanExpression :
		
		for line in range(sizeGeneExpList) :
			gene_Mean = ((fileMeanExpression.readline()).strip()).split( ' : ')
			
			if gene_Mean != [''] and gene_Mean[ 0] in synonymousDictionary :
				geneMeanDictionary[ synonymousDictionary[ gene_Mean[0]]] = gene_Mean[1]
	

		
#######	
	### THEN WRITE THE JSON FILE
	for line in range( len( linesList) - 1) :
			
			# Path to the output
			pathOutput = '../../data/DGRP/' + DGRP_sex.lower() + '_repository/' + (( linesList[ line+1]).replace( ':' , '_')) + '_' +DGRP_sex.lower() + '.json'
			fileOutput = open( pathOutput, 'w')
			
			# Some Setting
			added = 0
			the_writing = '[\n' # Initiation of the json format futur writing
			
			# Parse each gene extract previously from the expression file
			for gene in range( len( geneExpList)) :
				# Set the different data about this gene
				data =  geneExpList[ gene]
				geneName = data[ 0]
				expValue = data[ line+1]
				DGRPline = linesList[ line+1]
				DGRPline = ( DGRPline).replace(':','_')
				
				# Check if this gene is in our interactome
				if geneName in synonymousDictionary and synonymousDictionary[ geneName] in processDictionary and synonymousDictionary[ geneName] in geneMeanDictionary and synonymousDictionary[ geneName] in gene_functionDictionary :
					# set the different data we will implement in the json
					keyDictionary = synonymousDictionary[ geneName]
					process = processDictionary[ synonymousDictionary[ geneName]]
					expValue = float( expValue)
					gene_Mean = float( geneMeanDictionary[ keyDictionary])
					foldChange = expValue/gene_Mean
					Log2FoldChange = str( math.log( float( foldChange), 2))
					
					
					gene_function = gene_functionDictionary[ synonymousDictionary[ geneName]]
					
					# Check is the gene function is known
					if gene_function != "" :
						# if we know the gene functiion implements the data as follow
						the_writing = the_writing+('{ \n"gene"'+ ': "'+synonymousDictionary[ geneName]+'",'+'\n"FlybaseID"'+': "'+geneName+'",' +'\n"sampleID"'+ ': "' + DGRPline + '_' +( str( DGRP_sex)).lower() + '",' + '\n"sex": "' + DGRP_sex +'",'+ '\n"process": "' + process+'",' + '\n"gene_function": "' + gene_function + '",'+'\n"summarized_gene_expression": "' + str( expValue) +'",' + '\n"log2": ' + Log2FoldChange +','+'\n"pvalue": 1.000'+','+ '\n"mutation": ""'+ "\n}\n")
						added +=1
					
					else :
						gene_function = "Unknown gene_function"
						# if we don't know the gene function implement the data as follow
						the_writing = the_writing+('{ \n"gene"'+ ': "'+synonymousDictionary[ geneName]+'",'+'\n"FlybaseID"'+': "'+geneName+'",' +'\n"sampleID"'+ ': "' + DGRPline + '_' +( str( DGRP_sex)).lower() + '",' + '\n"sex": "' + DGRP_sex +'",'+ '\n"process": "' + process+'",' + '\n"gene_function": "' + gene_function + '",'+'\n"summarized_gene_expression": "' + str( expValue) +'",' + '\n"log2": ' + Log2FoldChange +','+'\n"pvalue": 1.000'+','+ '\n"mutation": ""'+ "\n}\n")
						added +=1
				
			# Write the json file about the mito-associated gene			
			if the_writing != '[\n' :
				the_writing = the_writing+("]")
				the_writing = the_writing.replace( "}", "},", added-1)
				fileOutput.write( str( the_writing))
				fileOutput.close()
				
				print(">>>>> (" + str(line+1) +str ("/") + str( len( linesList)-1) + ") with Data about " + ( str( DGRP_sex)).lower()+" " + str( DGRPline) + " have been write in the json format file //////\n")
		
			
	
