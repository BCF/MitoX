#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Update Fri 17 Mar 13:24 2017

This python file when executed build the json interactome file from 
The Cytoscape 2.8 product in association with the DroID database and 
The gene list

This program get 3 arguments :
the sif file with the interaction give as output in the CytoScape 2.8 ## Need to be in the archive_Interactome_DroID directory
the synonymous dictionary to rename the FB_ID by the gene Name used in MitoXplorer ## Need to be in the synonymous_dictionary directory
and the output ## will put a file with the name of the output argument in the archive_Interactome_DroID directory

@author: adrien
"""
import sys
import csv
import json

if __name__ == '__main__':
	dataset = sys.argv[1]
	the_dictionary = sys.argv[2]
	theOutput = sys.argv[3]
	
	# CORRECTION ON THE SIF FILE
	##############
	pathInput='../archive_Interactome_DroID/'+str(dataset)
	pathOutput='../archive_Interactome_DroID/Interactome_DroID_corrected.sif'
	fileInput = open(pathInput, 'r')
	fileOutput = open(pathOutput, 'w')

	i = 1
	with open(pathInput) as f:
		interactome = f.readlines()
		size = len(interactome)
		for line in range( size):
			interaction = ((interactome[line]).strip()).replace('\t\t',',')
			if line != size-1 :
				fileOutput.write(interaction + ';')
			if line == size-1 :
				fileOutput.write(interaction)
	
	fileInput.close()
	fileOutput.close()


	# RENAME THE FB ID WITH THE CORRECT GENE NAME USED ON MITOXPLORER
	##############
	pathInputGene='../archive_Interactome_DroID/Interactome_DroID_corrected.sif'
	pathInputDictionary='../synonymous_dictionary/'+str(the_dictionary)
	pathOutput='../archive_Interactome_DroID/Interactome_DroID_renamed.txt'
	
	fileInputGene = open(pathInputGene, 'r')
	fileInputDictionary = open(pathInputDictionary, 'r')
	fileOutput = open(pathOutput, 'w')
	
	dictionary = fileInputDictionary.readline()
	mydict = {}
	Key_ValueList = dictionary.split(' , ')
	size = len(Key_ValueList)
	
	for item in range(size) :
		key_value = (Key_ValueList[item]).split(' : ')
		mydict[key_value[0]] = key_value[1]
	
	
	dataGene=(fileInputGene.readline().strip().split(';'))
	sizeDataGene = len(dataGene)
		
	for interaction in range( sizeDataGene) :
		gene1=(dataGene[interaction]).split(',')[0]
		gene2=(dataGene[interaction]).split(',')[1]
		if gene1 in mydict and gene2 in mydict:
			fileOutput.write(mydict[gene1]+ ',' +mydict[gene2] + '\n')
	
	fileInputGene.close()
	fileInputDictionary.close()
	fileOutput.close()
	
	
	# BUILD THE JSON FILE FROM THE RENAMED TXT 
	#################
	csvfile = open('../archive_Interactome_DroID/Interactome_DroID_renamed.txt', 'r')
	jsonfile = open('../archive_Interactome_DroID/Interactome_DroID.json', 'w')
	
	
	fieldnames = ("source","target")
	reader = csv.DictReader( csvfile, fieldnames)
	jsonfile.write('[')
	for row in reader:
		json.dump(row, jsonfile)
		jsonfile.write(',')
	
	jsonfile.write(']')
	
	csvfile.close()
	jsonfile.close()
	
	
	# CORRECTION ON THE JSON FILE
	####################
	
	fileToOutput = sys.argv[1]
	pathInput='../archive_Interactome_DroID/Interactome_DroID.json'
	pathOutput='../archive_Interactome_DroID/'+str(theOutput)
	fileInput = open(pathInput, 'r')
	fileOutput = open(pathOutput, 'w')

	data = (fileInput.readline()).replace(',]',']')
	data2 = data.strip(',')
	
	for genes in data2 :
		fileOutput.write(genes)
	
	fileInput.close()
	fileOutput.close()

