#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 15 Mar 14:00:00 2017

Just Use to join the data in the Gene and Process File 

You give 3 argument :
a genelist.txt in DRGP_archive directory
a processlist.txt in Fly_archive directory
and an output name ## The file will be in the Fly_archive directory

@author: adrien
"""
import sys

if __name__ == '__main__':
	pathInputGene = '../Fly_archive/' + str( sys.argv[1])
	pathInputProcess ='../Fly_archive/' + str( sys.argv[2])
	pathOutput = '../Fly_archive/' + str( sys.argv[3])
	
	fileInputProcess = open( pathInputProcess, 'r')
	fileOutput = open( pathOutput, 'w')
	
	with open( pathInputGene) as fileInputGene :
		for line in fileInputGene :
			gene = line.strip()
			process = (fileInputProcess.readline()).strip()
			
			
			fileOutput.write( gene  + ' : ' + process + '\n')

	fileInputGene.close()
	fileInputProcess.close()
	fileOutput.close()

