#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Last update
06-06-2017

This file will build the interactome json from a String tsv format file 
computed on the String website with our selected mito-associated gene ID 

NB : THIS FILE IS ONLY USED IN A SCRIPT IN THE BIN DIRECTORY OF MITOXPLORER
	USE THIS FILE IF YOU UNDERSTAND CLEARLY HOW IT WORKS at least
	contact : adrien.bonnard@etu.univ-amu.fr
			  adrienbonnard83@yahoo.fr 


@author: adrien
"""
import sys
import csv
import json

if __name__ == '__main__':
	
	fileToUse = sys.argv[1]
	fileToOutput = sys.argv[2]
	
	# TSV to CSV FORMAT
	###################
	pathInput='../Download_Tools/scripts_for_interactome/'+str(fileToUse)
	pathOutput='../Download_Tools/scripts_for_interactome/DataSet.csv'
	fileInput = open(pathInput, 'r')
	fileOutput = open(pathOutput, 'w')
	
	i=0
	lineSplitted=[]
	while i<= 1000000 :
		lineSplitted = (fileInput.readline()).split('\t')
		if lineSplitted[0]!="" :
			data1 = lineSplitted[0]
			data2 = lineSplitted[1]
			fileOutput.write(data1)
			fileOutput.write(',')
			fileOutput.write(data2)
			fileOutput.write('\n')
		i+=1
	
	fileInput.close()
	fileOutput.close()
	
	# CSV to JSON FORMAT
	###################
	csvfile = open('../Download_Tools/scripts_for_interactome/DataSet.csv', 'r')
	jsonfile = open('../Download_Tools/scripts_for_interactome/Interactome_STRING.json', 'w')
	
	
	fieldnames = ("source","target")
	reader = csv.DictReader( csvfile, fieldnames)
	jsonfile.write('[')
	for row in reader:
		json.dump(row, jsonfile)
		jsonfile.write(',')
	
	jsonfile.write(']')
	
	csvfile.close()
	jsonfile.close()

	
	# CORRECTION IN THE JSON FILE 
	###################
	pathInput='../Download_Tools/scripts_for_interactome/Interactome_STRING.json'
	pathOutput='../Download_Tools/scripts_for_interactome/output_for_String/'+str(fileToOutput)
	fileInput = open(pathInput, 'r')
	fileOutput = open(pathOutput, 'w')

	data = (fileInput.readline()).replace(',]',']')
	data2 = data.strip(',')
	
	for genes in data2 :
		fileOutput.write(genes)
	
	fileInput.close()
	fileOutput.close()
