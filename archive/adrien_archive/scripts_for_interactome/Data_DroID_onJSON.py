#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Last update
06-06-2017

This file will build the interactome json from a DroID sif format file 
computed on cytoscape against the DroID library with our selected mito-associated gene ID 

NB : THIS FILE IS ONLY USED IN A SCRIPT IN THE BIN DIRECTORY OF MITOXPLORER
	USE THIS FILE IF YOU UNDERSTAND CLEARLY HOW IT WORKS at least
	contact : adrien.bonnard@etu.univ-amu.fr
			  adrienbonnard83@yahoo.fr 


@author: adrien
"""
import sys
import csv
import json

if __name__ == '__main__':
	dataset = sys.argv[1]
	theOutput = sys.argv[2]
	
	# CORRECTION ON THE SIF FILE
	##############
	pathInput='../Download_Tools/scripts_for_interactome/'+str(dataset)
	pathOutput='../Download_Tools/scripts_for_interactome/Interactome_DroID_corrected.sif'
	fileInput = open(pathInput, 'r')
	fileOutput = open(pathOutput, 'w')

	i = 1
	## with open('filename.txt') as f:
	## 	vowel = sum(ch in VOWELS for line in f for ch in line.strip())
	while i <= 1000000:
		data = (fileInput.readline()).replace('\t\t',',')
		if data!="" : fileOutput.write(data)
		i+=1
		
	fileInput.close()
	fileOutput.close()


	# RENAME THE FB ID WITH THE CORRECT GENE NAME USED ON MITOXPLORER
	##############
	pathInputGene='../Download_Tools/scripts_for_interactome/Interactome_DroID_corrected.sif'
	pathInputDictionary='../Download_Tools/scripts_for_interactome/synonymous_dictionary.txt'
	pathOutput='../Download_Tools/scripts_for_interactome/Interactome_DroID_renamed.txt'
	
	fileInputGene = open(pathInputGene, 'r')
	fileInputDictionary = open(pathInputDictionary, 'r')
	fileOutput = open(pathOutput, 'w')
	
	dictionary = fileInputDictionary.readline()
	mydict = {}
	Key_ValueList = dictionary.split(' , ')
	size = len(Key_ValueList)
	
	for item in range(size) :
		key_value = (Key_ValueList[item]).split(' : ')
		#~ print(key_value)
		mydict[key_value[0]] = key_value[1]
	
	
	i=0
	while i<= size :
		
		
		
		dataGene=(fileInputGene.readline().strip().split(','))
		gene1=dataGene[0]
		gene2=dataGene[1]
		
		
		if gene1 in mydict and gene2 in mydict:
			fileOutput.write( mydict[gene2] + ',' + mydict[gene1] + '\n')
	
		i+=1
	

	
	
	fileInputGene.close()
	fileInputDictionary.close()
	fileOutput.close()
	
	
	# BUILD THE JSON FILE FROM THE RENAMED TXT 
	#################
	csvfile = open('../Download_Tools/scripts_for_interactome/Interactome_DroID_renamed.txt', 'r')
	jsonfile = open('../Download_Tools/scripts_for_interactome/Interactome_DroID.json', 'w')
	
	
	fieldnames = ("source","target")
	reader = csv.DictReader( csvfile, fieldnames)
	jsonfile.write('[')
	for row in reader:
		json.dump(row, jsonfile)
		jsonfile.write(',')
	
	jsonfile.write(']')
	
	csvfile.close()
	jsonfile.close()
	
	
	# CORRECTION ON THE JSON FILE
	####################
	
	fileToOutput = sys.argv[1]
	pathInput='../Download_Tools/scripts_for_interactome/Interactome_DroID.json'
	pathOutput='../Download_Tools/scripts_for_interactome/output_for_DroID/'+str(theOutput)
	fileInput = open(pathInput, 'r')
	fileOutput = open(pathOutput, 'w')

	data = (fileInput.readline()).replace(',]',']')
	data2 = data.strip(',')
	
	for genes in data2 :
		fileOutput.write(genes)
	
	fileInput.close()
	fileOutput.close()

