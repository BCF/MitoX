<?php
$id = $_GET['id'];

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>MitoXplorer</title>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- Vis CSS -->
        <link href='./css/App.css' rel='stylesheet' type='text/css'>
        <link href="./css/style.css" rel="stylesheet">
        <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        
        <link rel="icon" type="image/png" href="img/logos/favicon.png">
        
    </head>
   <body>
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">MitoXplorer</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#database">Database</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#analysis">Analysis</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#tutorial">Tutorial</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#download">Download</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#contact">Contact</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <br>
    </body>
    <!-- App Script  -->
   
    <script>
		function openInNewTab(url) {
			var win = window.open(url, '_blank');
			if (win) {
				//Browser has allowed it to be opened
				win.focus();
			} else {
				//Browser has blocked it
				alert('Please allow popups for this website');
			}
			}
		
		function moveToThisInteractomeFemale() {
			theInteractome = document.getElementById("mySelectFemale").value;
			console.log(theInteractome);
			openInNewTab('mitomodelFly.php?id=../data/DGRP/female_repository/'+theInteractome);
			document.getElementById('defaultFEMALE').selected = 'selected';
		}
		
		function moveToThisInteractomeMale() {
			theInteractome = document.getElementById("mySelectMale").value;
			console.log(theInteractome);
			openInNewTab('mitomodelFly.php?id=../data/DGRP/male_repository/'+theInteractome);
			document.getElementById('defaultMALE').selected = 'selected';
			
		}
		
	
	</script>
   
   
   
   <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
					<h2 class="section-heading">Interactome</h2>
                    <h3 class="section-subheading text-muted">You are on the DGRP Dataset</h3>
                    <p class="large">Use the select picker and pick the sample you want and a window will be open</p>
					<p class="large">NB: pop-up will need to be allow in this web page</p>
					<div><br></div>
					<div><br></div>
                    
					<div class="col-md-4 col-sm-6 database-item">
						<?php
							$path    = 'data/DGRP/female_repository';
							echo("<select class='select-type' id='mySelectFemale' onchange='moveToThisInteractomeFemale()'>");
							echo("<option id='defaultFEMALE' selected='selected'>Choose here</option>");
							foreach(glob($path.'/*') as $file) {
								echo($path.$file);
								$arrayFile = explode('/',$file);
								//~ echo("<br>");
								//~ print_r($arrayFile);
								//~ echo("<br>");
								$name = str_replace('.json','',$arrayFile['3']);
								//~ echo($name);
								//~ echo("<br>");
								echo("<option value=".$name.">$name</option>");
							}
							echo("</select>");
						?>	
						<div><br></div>
						<img src="img/database/DrosoFemale.jpg" class="img-responsive" alt="">
						
						
					
						<div class="database-caption">
							<h4>Female DGRP</h4>
							<p class="text-muted">Data on DGRP - Log2FoldChange beetween the summarized expression (Mean) against the general average </p>
						</div>
					</div>
					<div style="border:2px solid grey" class="col-md-4 col-sm-6 database-item">
						<div><br></div>
						<div><br></div>
						<p> This data about summarized expression are from the same article</p>
						<p> an artcile named : "Genetic basis of transcriptome diversity in Drosophila melanogaster"</p>
						
						<a href="https://www.ncbi.nlm.nih.gov/pubmed/26483487" target="_blank"> Huang & al 2015 PNAS </a>
						<br>
						<br>
						<a href="http://dgrp2.gnets.ncsu.edu" target="_blank"> Pick on the DGRP2 WebSite </a>
						<div><br></div>
						<div><br></div>
					</div>
					<div class="col-md-4 col-sm-6 database-item">
						
						
						<?php	
								$path    = 'data/DGRP/male_repository';
								echo("<select class='select-type' id='mySelectMale' onchange='moveToThisInteractomeMale()'>");
								echo("<option id='defaultMALE' selected='selected'>Choose here</option>");
								foreach(glob($path.'/*') as $file) {
									echo($path.$file);
									$arrayFile = explode('/',$file);
									//~ echo("<br>");
									//~ print_r($arrayFile);
									//~ echo("<br>");
									$name = str_replace('.json','',$arrayFile['3']);
									//~ echo($name);
									//~ echo("<br>");
									echo("<option value=".$name.">$name</option>");
								}
								echo("</select>");
								
						?>
						<div><br></div>
						<img src="img/database/DrosoMale.jpg" class="img-responsive" alt="">
							
							<div class="database-caption">
								<h4>Male DGRP</h4>
								<p class="text-muted">Data on DGRP - Log2FoldChange beetween the summarized expression (Mean) against the general average </p>
							</div>
					</div>
					
                </div>
			</div>
		</div>
</html>
