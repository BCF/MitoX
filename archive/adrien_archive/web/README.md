#* Last Update
** BONNARD Adrien 06-06-2017,**


# Download_Tools 

	This directory hold the different file that BONNARD Adrien used during his internship to product the Fly interactome and the different 
	json files use for the Fly part of MitoXplorer

|||-------------------------------------------|||

## archive_Interactome_DroID

	Hold the different_file that have been build to production the json file
	holding the data about the fly mito-associated gene interactome with the DroID sif file.


|||-------------------------------------------|||

## Fly_archive

	Hold the different files about Fly, that have been used to build the different json 
	for each dataset in the Fly part


|||-------------------------------------------|||

## Flybase_synonymous

	Hold the file wiht synonymous file list for each Fbgn Id in our interactome get in Flybase.org


|||-------------------------------------------|||

## python_tools

	hold the different python script to build or rebuild all the json files in  each  dataset of the Fly part of MitoXplorer


|||-------------------------------------------|||

## script_for_interactome

	Hold the python script and files, to build or rebuild the two different kind of interactome used in the MitoXplorer Fly part.


|||-------------------------------------------|||

## synonymous_dictionary

	Hold the text file with the dictionary with all the combination of synonymous - geneName
	this file is used in different python script to rename geneID or FlybaseID to a convergent name used in our interactome.


|||-------------------------------------------|||
