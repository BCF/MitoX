<?php 
if (isset($_COOKIE['mitox_session_id'])) { 
        session_id($_COOKIE['mitox_session_id']);
}

session_start();
$id = session_id();

$get = $_COOKIE['mitox_session_id'];
setcookie('mitox_session_id',$id,time() + (86400 * 7));

$compare = $_GET['compare'];
if($compare){
    $jsarray =implode(",", $compare);
}
echo $id
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MitoXplorer</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="css/font.css" rel="stylesheet" type="text/css">

    <!-- Theme CSS -->
    <link href="css/agency.css" rel="stylesheet">
    
    <!-- CSS for database -->
    <link href="css/database.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link rel="icon" type="image/png" href="img/logos/favicon.png">

</head>

<body id="page-top" class="index">
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">MitoXplorer</a>
                
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
                    <li>
                        <a href="index.php">Human & Mouse Database</a>
                    </li>
                    
                    <li>
                        <a href="indexFly.php">Fly Database</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Welcome to</div>
                <div class="intro-heading">The Tutorial Session</div>
                <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>

            </div>
        </div>
    </header>

	<!-- About Section -->
    <section id="about" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Tutorial MitoXplorer</h2>
                    <h3 class="section-subheading text-muted">Tutorial - A Visualization of the different Tools</h3>
                    <p class="large">This part of MitoXplorer offer to show you the different tools you are able to use</p>
                    <p class="large"> We will explain <strong>how to launch this different tools</strong> and <strong>how to use their options</strong></p><br>
                    <p class="large"> See at :</p><br>
                    <p class="large"><u><a href="tutorial.php#Page_1_Global" class="database-link" data-toggle="modal"> - Global Tutorial -</a></u></p> <br>
                    <p class="large"><u><a href="tutorial.php#Page_1_SHT" class="database-link" data-toggle="modal"> - Scatter Plot & Hitmap Tutorial -</a></u></p> <br>
                    <p class="large"><u><a href="tutorial.php#Page_1_PCAT" class="database-link" data-toggle="modal"> - PCA Tutorial -</a></u></p> <br>
                    <p class="large"><u><a href="tutorial.php#Page_1_HT" class="database-link" data-toggle="modal"> - Heatmap Tutorial -</a></u></p> <br>
                    
                </div>
            </div>
        </div>
    </section>
	
	
	<!-- Tutorial Grid Section -->
   
    <section id="database">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Tutorials</h2>
                    <h3 class="section-subheading text-muted">Browse our tutorials</h3>
                </div>
            </div>
            <div class="row">
				
				
				<!-- The Global Tutorial-->
                <div class="col-md-3 col-sm-9 database-item" type="text/x-handlebars-template">
					<a href="#Page_1_Global" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="tuto_Analysis/PNG/1.png" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>The Global Tutorial</h4>
                        <br>
                        <p class="text-muted"> You ll have an overview of all the tools and how to launch them and use their options</p>
                    </div>
                </div>
                
                <!-- The Scatter Plot & Hitmap Tutorial-->
                <div class="col-md-3 col-sm-9 database-item" type="text/x-handlebars-template">
					<a href="#Page_1_SHT" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="tuto_Analysis/PNG/5.png" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>The Scatter Plot & Hitmap Tutorial</h4>
                        <br>
                        <p class="text-muted"> You ll specifically watch at the Scatterplot & hitmap tool, how to use it and use its options</p>
                    </div>
                </div>
                
                <!-- The PCA Tutorial-->
                <div class="col-md-3 col-sm-9 database-item" type="text/x-handlebars-template">
					<a href="#Page_1_PCAT" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="tuto_Analysis/PNG/10.png" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>The PCA Tutorial</h4>
                        <br>
                        <p class="text-muted">You ll specifically watch at the PCA tool, how to use it and use its options</p>
                    </div>
                </div>
				
				<!-- The Heatmap Tutorial-->
                <div class="col-md-3 col-sm-9 database-item" type="text/x-handlebars-template">
					<a href="#Page_1_HT" class="database-link" data-toggle="modal">
                        <div class="database-hover">
                            <div class="database-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="tuto_Analysis/PNG/14.png" class="img-responsive" alt="">
                    </a>
                    <div class="database-caption">
                        <h4>The Heatmap Tutorial</h4>
                        <br>
                        <p class="text-muted">You ll specifically watch at the Heatmap tool, how to use it and use its options</p>
                    </div>
                </div>
                
                
			</div>
        </div>
    
    </section>
    
    
    <!-- database Modals -->
    <!-- Use the modals below to showcase details about your database projects! -->
	
    
    <!-- Global Tutorial-->
    <div class="database-modal modal fade" id="Page_1_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 1			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/1.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 1 </p>
								</li>
								<li>
									<button class="btn" data-number="1_Global_F" href="Page_2_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="1_Global_FF" href="Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
			</div>
		</div>
    </div>				
    <div class="database-modal modal fade" id="Page_2_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 2			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/2.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="2_Global_B" href="#Page_1_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 2 </p>
								</li>
								<li>
									<button class="btn" data-number="2_Global_F" href="#Page_3_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="2_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>			
	<div class="database-modal modal fade" id="Page_3_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 3			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/3.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="3_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="3_Global_B" href="#Page_2_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 3 </p>
								</li>
								<li>
									<button class="btn" data-number="3_Global_F" href="#Page_4_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="3_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_4_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 4			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/4.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="4_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="4_Global_B" href="#Page_3_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 4 </p>
								</li>
								<li>
									<button class="btn" data-number="4_Global_F" href="#Page_5_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="4_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_5_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 5			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/5.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="5_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="5_Global_B" href="#Page_4_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 5 </p>
								</li>
								<li>
									<button class="btn" data-number="5_Global_F" href="#Page_6_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="5_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_6_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 6			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/6.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="6_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="6_Global_B" href="#Page_5_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 6 </p>
								</li>
								<li>
									<button class="btn" data-number="6_Global_F" href="#Page_7_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="6_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_7_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 7			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/7.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="7_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="7_Global_B" href="#Page_6_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 7 </p>
								</li>
								<li>
									<button class="btn" data-number="7_Global_F" href="#Page_8_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="7_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    
    <div class="database-modal modal fade" id="Page_8_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 8			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/8.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="8_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="8_Global_B" href="#Page_7_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 8 </p>
								</li>
								<li>
									<button class="btn" data-number="8_Global_F" href="#Page_9_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="8_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_9_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 9			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/9.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="9_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="9_Global_B" href="#Page_8_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 9 </p>
								</li>
								<li>
									<button class="btn" data-number="9_Global_F" href="#Page_10_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="9_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_10_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 10			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/10.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="10_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="10_Global_B" href="#Page_9_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 10 </p>
								</li>
								<li>
									<button class="btn" data-number="10_Global_F" href="#Page_11_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="10_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_11_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 11			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/11.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="11_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="11_Global_B" href="#Page_10_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 11 </p>
								</li>
								<li>
									<button class="btn" data-number="11_Global_F" href="#Page_12_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="11_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_12_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 12			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/12.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="12_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="12_Global_B" href="#Page_11_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 12 </p>
								</li>
								<li>
									<button class="btn" data-number="12_Global_F" href="#Page_13_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="12_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_13_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 13			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/13.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="13_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="13_Global_B" href="#Page_12_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 13 </p>
								</li>
								<li>
									<button class="btn" data-number="13_Global_F" href="#Page_14_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="13_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_14_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 14			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/14.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="14_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="14_Global_B" href="#Page_13_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 14 </p>
								</li>
								<li>
									<button class="btn" data-number="14_Global_F" href="#Page_15_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="14_Global_FF" href="#Page_16_Global"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_15_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 15			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/15.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="15_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="15_Global_B" href="#Page_14_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 15 </p>
								</li>
								<li>
									<button class="btn" data-number="15_Global_F" href="#Page_16_Global"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_16_Global" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 16			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/16.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="16_Global_FB" href="#Page_1_Global"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="16_Global_B" href="#Page_15_Global"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 16 </p>
								</li>
								<li>
									<button class="btn"><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>			

<!--
     
     SCATTER PLOT & HITMAP TUTORIAL
     
-->
    <div class="database-modal modal fade" id="Page_1_SHT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 1			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/1.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 1 </p>
								</li>
								<li>
									<button class="btn" data-number="1_SHT_F" href="Page_2_SHT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="1_SHT_FF" href="Page_9_SHT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
			</div>
		</div>
    </div>				
    <div class="database-modal modal fade" id="Page_2_SHT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 2			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/2.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="2_SHT_B" href="#Page_1_SHT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 2 </p>
								</li>
								<li>
									<button class="btn" data-number="2_SHT_F" href="#Page_3_SHT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="2_SHT_FF" href="#Page_9_SHT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>			
	<div class="database-modal modal fade" id="Page_3_SHT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 3			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/3.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="3_SHT_FB" href="#Page_1_SHT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="3_SHT_B" href="#Page_2_SHT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 3 </p>
								</li>
								<li>
									<button class="btn" data-number="3_SHT_F" href="#Page_4_SHT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="3_SHT_FF" href="#Page_9_SHT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_4_SHT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 4			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/4.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="4_SHT_FB" href="#Page_1_SHT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="4_SHT_B" href="#Page_3_SHT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 4 </p>
								</li>
								<li>
									<button class="btn" data-number="4_SHT_F" href="#Page_5_SHT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="4_SHT_FF" href="#Page_9_SHT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_5_SHT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 5			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/5.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="5_SHT_FB" href="#Page_1_SHT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="5_SHT_B" href="#Page_4_SHTl"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 5 </p>
								</li>
								<li>
									<button class="btn" data-number="5_SHT_F" href="#Page_6_SHT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="5_SHT_FF" href="#Page_9_SHT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_6_SHT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 6			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/6.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="6_SHT_FB" href="#Page_1_SHT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="6_SHT_B" href="#Page_5_SHT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 6 </p>
								</li>
								<li>
									<button class="btn" data-number="6_SHT_F" href="#Page_7_SHT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="6_SHT_FF" href="#Page_9_SHT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_7_SHT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 7			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/7.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="7_SHT_FB" href="#Page_1_SHT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="7_SHT_B" href="#Page_6_SHT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 7 </p>
								</li>
								<li>
									<button class="btn" data-number="7_SHT_F" href="#Page_8_SHT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="7_SHT_FF" href="#Page_9_SHT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    
    <div class="database-modal modal fade" id="Page_8_SHT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 8			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/8.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="8_SHT_FB" href="#Page_1_SHT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="8_SHT_B" href="#Page_7_SHT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 8 </p>
								</li>
								<li>
									<button class="btn" data-number="8_SHT_F" href="#Page_9_SHT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_9_SHT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 9			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/9.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="9_SHT_FB" href="#Page_1_SHT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="9_SHT_B" href="#Page_8_SHT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 9 </p>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    
<!--
    
    
    PCA TUTORIAL
    
    
-->
   
   <!-- Global Tutorial-->
    <div class="database-modal modal fade" id="Page_1_PCAT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 1			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/1.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 1 </p>
								</li>
								<li>
									<button class="btn" data-number="1_PCAT_F" href="Page_2_PCAT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="1_PCAT_FF" href="Page_8_PCAT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
			</div>
		</div>
    </div>				
    <div class="database-modal modal fade" id="Page_2_PCAT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 2			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/2.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="2_PCAT_B" href="#Page_1_PCAT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 2 </p>
								</li>
								<li>
									<button class="btn" data-number="2_PCAT_F" href="#Page_3_PCAT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="2_PCAT_FF" href="#Page_8_PCAT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>			
	<div class="database-modal modal fade" id="Page_3_PCAT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 3			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/3.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="3_PCAT_FB" href="#Page_1_PCAT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="3_PCAT_B" href="#Page_2_PCAT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 3 </p>
								</li>
								<li>
									<button class="btn" data-number="3_PCAT_F" href="#Page_4_PCAT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="3_PCAT_FF" href="#Page_8_PCAT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_4_PCAT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 4			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/4.png"> 
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="4_PCAT_FB" href="#Page_1_PCAT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="4_PCAT_B" href="#Page_3_PCAT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 4 </p>
								</li>
								<li>
									<button class="btn" data-number="4_PCAT_F" href="#Page_5_PCAT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="4_PCAT_FF" href="#Page_8_PCAT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
   <div class="database-modal modal fade" id="Page_5_PCAT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 5		
					-->
					<div class="container">
						<div>
							<!-- CARE THIS IS THE CORRECT IMAGE BUT THE NUMBER IS NOT THE CURRENT PAGE-->
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/10.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="5_PCAT_FB" href="#Page_1_PCAT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="5_PCAT_B" href="#Page_4_PCAT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 5 </p>
								</li>
								<li>
									<button class="btn" data-number="5_PCAT_F" href="#Page_6_PCAT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="5_PCAT_FF" href="#Page_8_PCAT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_6_PCAT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 6			
					-->
					<div class="container">
						<div>
							<!-- CARE THIS IS THE CORRECT IMAGE BUT THE NUMBER IS NOT THE CURRENT PAGE-->
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/11.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="6_PCAT_FB" href="#Page_1_PCAT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="6_PCAT_B" href="#Page_5_PCAT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 6 </p>
								</li>
								<li>
									<button class="btn" data-number="6_PCAT_F" href="#Page_7_PCAT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="6_PCAT_FF" href="#Page_8_PCAT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_7_PCAT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 7			
					-->
					<div class="container">
						<div>
							<!-- CARE THIS IS THE CORRECT IMAGE BUT THE NUMBER IS NOT THE CURRENT PAGE-->
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/12.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="7_PCAT_FB" href="#Page_1_PCAT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="7_PCAT_B" href="#Page_6_PCAT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 7 </p>
								</li>
								<li>
									<button class="btn" data-number="7_PCAT_F" href="#Page_8_PCAT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_8_PCAT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 8		
					-->
					<div class="container">
						<div>
							<!-- CARE THIS IS THE CORRECT IMAGE BUT THE NUMBER IS NOT THE CURRENT PAGE-->
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/13.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="8_PCAT_FB" href="#Page_1_PCAT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="8_PCAT_B" href="#Page_7_PCAT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 8 </p>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
<!--
   
   HEATMAP TUTORIAL
   
   
-->
    <!-- Global Tutorial-->
    <div class="database-modal modal fade" id="Page_1_HT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 1			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/1.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 1 </p>
								</li>
								<li>
									<button class="btn" data-number="1_HT_F" href="Page_2_HT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="1_HT_FF" href="Page_7_HT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
			</div>
		</div>
    </div>				
    <div class="database-modal modal fade" id="Page_2_HT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 2			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/2.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="2_HT_B" href="#Page_1_HT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 2 </p>
								</li>
								<li>
									<button class="btn" data-number="2_HT_F" href="#Page_3_HT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="2_HT_FF" href="#Page_7_HT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>			
	<div class="database-modal modal fade" id="Page_3_HT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 3			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/3.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="3_HT_FB" href="#Page_1_HT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="3_HT_B" href="#Page_2_HT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 3 </p>
								</li>
								<li>
									<button class="btn" data-number="3_HT_F" href="#Page_4_HT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="3_HT_FF" href="#Page_7_HT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_4_HT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 4			
					-->
					<div class="container">
						<div>
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/4.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="4_HT_FB" href="#Page_1_HT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="4_HT_B" href="#Page_3_HT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 4 </p>
								</li>
								<li>
									<button class="btn" data-number="4_HT_F" href="#Page_5_HT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="4_HT_FF" href="#Page_7_HT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_5_HT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 5			
					-->
					<div class="container">
						<div>
							<!-- CARE THIS IS THE CORRECT IMAGE BUT THE NUMBER IS NOT THE CURRENT PAGE-->
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/14.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="5_HTl_FB" href="#Page_1_HT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="5_HT_B" href="#Page_4_HT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 5 </p>
								</li>
								<li>
									<button class="btn" data-number="5_HT_F" href="#Page_6_HT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="5_HT_FF" href="#Page_7_HT"><i class="fa fa-fast-forward" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_6_HT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 6			
					-->
					<div class="container">
						<div>
							<!-- CARE THIS IS THE CORRECT IMAGE BUT THE NUMBER IS NOT THE CURRENT PAGE-->
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/15.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="6_HT_FB" href="#Page_6_HT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="6_HT_B" href="#Page_6_HT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 6 </p>
								</li>
								<li>
									<button class="btn" data-number="6_HT_F" href="#Page_7_HT"><i class="fa fa-forward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>	
    <div class="database-modal modal fade" id="Page_7_HT" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="close-modal" data-dismiss="modal">
						<div class="lr">
							<div class="rl">
							</div>
						</div>
					</div>
					<!--
							PAGE 7			
					-->
					<div class="container">
						<div>
							<!-- CARE THIS IS THE CORRECT IMAGE BUT THE NUMBER IS NOT THE CURRENT PAGE-->
							<img width="100%" height="100%" border="0" align="center"  src="tuto_Analysis/PNG/16.png">
						</div>
						<div class="row">
							<ul class="list-inline analysis">
								<li>
									<button class="btn" data-number="7_HT_FB" href="#Page_1_HT"><i class="fa fa-fast-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" data-number="7_HT_B" href="#Page_6_HT"><i class="fa fa-backward" aria-hidden="true"></i></button>
								</li>
								<li>
									<p class="large"> PAGE 7 </p>
								</li>
								<li>
									<button class="btn"><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
								<li>
									<button class="btn" ><i class="fa fa-ban" aria-hidden="true"></i></button>
								</li>
							</ul>
						</div>	
					</div>	
				</div>
		</div>
    </div>		
    
    
    
    
    
    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/agency.min.js"></script>
    
    
 
	<script src="js/tutorial.js"></script>
	
	
</body>

</html>
