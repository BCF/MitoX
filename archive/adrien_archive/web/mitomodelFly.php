<?php
$id = $_GET['id'];

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>MitoXplorer</title>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- Vis CSS -->
        <link href='./css/App.css' rel='stylesheet' type='text/css'>
        <link href="./css/style.css" rel="stylesheet">
        <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        
        <link rel="icon" type="image/png" href="img/logos/favicon.png">
        
    </head>
    
    <body>
                
            <div class="col-md-12 title" style="font-size:10px;font-decoration:lowercase;margin-top:5px;text-align:left;">
                   
                    <a href="tutorial.php" onclick="window.open(this.href,'','scrollbars=no,resizable=yes, location=no,menubar=no,status=no,toolbar=no,left='+(screen.availWidth/2-350)+ ', top='+(screen.availHeight/2-350)+',width=1000,height=850');return false;">Launch tutorial <i class="fa fa-question-circle-o" aria-hidden="true">
                    </i>  
                    </a>
                    
            </div> 

</body>
               



    <!-- App Script  -->
    <script data-my_var_1="<?php echo $id; ?>" data-my_var_2="./data/zzfiles/dgrp-genes/dgrp-interactome.json" src="js/AppFly.js"></script>
    <script> 
        App.init({});
    </script>

</html>
<?php
$id = NULL;
?>
