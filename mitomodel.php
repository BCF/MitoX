<?php
$sampleID = $_GET['sampleID'];
$organism = $_GET['organism'];
$id = $_GET['sessionid'];

//Get info for mysql server
$str = file_get_contents('mysql/mysql_info.json');
$json = json_decode($str, true);
$host = $json['host'];
$port = $json['port'];
$user = $json['user'];
$passwd = $json['passwd'];
$unix_socket = $json['unix_socket'];
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>MitoXplorer</title>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- Vis CSS -->
        <link href='css/main.css' rel='stylesheet' type='text/css'> 
        <link href='css/mitomodel/App.css' rel='stylesheet' type='text/css'>       
        <link rel="icon" type="image/png" href="img/logos/favicon.png">   
    </head>
    
    <body>
                
            <!--<div class="col-md-12 title" style="font-size:10px;font-decoration:lowercase;margin-top:5px;text-align:left;">
                   
                    <a href="tutorial.php" onclick="window.open(this.href,'','scrollbars=no,resizable=yes, location=no,menubar=no,status=no,toolbar=no,left='+(screen.availWidth/2-350)+ ', top='+(screen.availHeight/2-350)+',width=1000,height=850');return false;">Launch tutorial <i class="fa fa-question-circle-o" aria-hidden="true">
                    </i>  
                    </a>
                    
            </div>-->

</body>
    <!-- App Script  -->    
    <script sampleID="<?php echo $sampleID; ?>" organism="<?php echo $organism; ?>" sessionid="<?php echo $id; ?>" host="<?php echo $host; ?>" port="<?php echo $port; ?>" user="<?php echo $user; ?>" passwd="<?php echo $passwd; ?>" unix_socket="<?php echo $unix_socket; ?>" src="js/mitomodel/App.js"></script>
    <script>
        App.init({});
    </script>
</html>
<?php
$id = NULL;
?>