<?php 
if (isset($_COOKIE['mitox_session_id'])) { 
        session_id($_COOKIE['mitox_session_id']);
}
if ($_GET['sessionid']) { 
        session_id($_GET['sessionid']);
}
session_start();
$id = session_id();

setcookie('mitox_session_id',$id,time() + (86400 * 7));

$PCA_path = "data/user_uploads/".$id."/PCA/";
if (!is_dir($PCA_path)){
    mkdir($PCA_path, 0777, true);
}

$heatmap_path = "data/user_uploads/".$id."/heatmap/";
if (!is_dir($heatmap_path)){
    mkdir($heatmap_path, 0777, true);
}

$compare = $_GET['compare'];
$organism = $_GET['organism'];

if($compare){
    $jsarray =implode(",", $compare);
}

if(!$organism){
    $organism = "Human";
}

//Get info for mysql server
$str = file_get_contents('mysql/mysql_info.json');
$json = json_decode($str, true);
$host = $json['host'];
$port = $json['port'];
$user = $json['user'];
$passwd = $json['passwd'];
$unix_socket = $json['unix_socket'];


$connect = mysql_connect($host,$user, $passwd, 'false', '128');
mysql_query('set global group_concat_max_len = 150000');
mysql_select_db($organism,$connect);

$query = "SELECT * from file_directory WHERE userID in ('mitox', '".$id."')";
$result = mysql_query($query);

$rows = array();
while($r = mysql_fetch_assoc($result)) {
    $rows[] = $r;
}
?>
<html lang="en">
    <head>
        <title>MitoXplorer - Analysis</title>
        <meta charset=utf-8>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSS -->
        <link href="./css/main.css" rel="stylesheet" >
        <link href="./css/compare/bootstrap-select.min.css" rel="stylesheet" >
        <link href="./css/compare/compare.css" rel="stylesheet" >
        
        <script src="./js/jquery.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <script src="./js/bootstrap-select.min.js"></script>
        <script src="./js/compare/js.cookie-2.1.4.min.js"></script>
        <link rel="icon" type="image/png" href="img/logos/favicon.png">
    </head>
    
    <body>
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php">MitoXplorer</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="index.php"></a>
                    </li>
                    <li>
                        <a href="about.html">About</a>
                    </li>
                    <li>
                        <a href="database.php">Database</a>
                    </li>
                    <li>
                        <a href="#" style="background-color: #12e2c0;border-radius: 3px;color:white">Analysis</a>
                    </li>
                    <li>
                        <a href="upload.php">Upload</a>
                    </li>
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
        <!-- Page Content -->


        <!-- Page Content -->     
    <div class="container-fluid main" id="content">
        <div class="col-md-2" style="padding:0px">
            <div id="sidebar">
                <div class="row">
                    <!-- Select organism -->
                    <div class="col-md-12 title" style="margin-top:10px">
                        <a data-toggle="collapse" href="#organism_collapse_div">
                            Organism
                        </a>
                        <!--<button type="button" class="btn btn-default btn-xs question" data-toggle="modal" data-target="#popup"><i class="fa fa-question-circle-o" aria-hidden="true"></i>
                            <span id="question-organism" class="question-tooltip" style="width: 125px;height: 40px;left: 75%;">Choose an<br>organism database</span>
                        </button>-->
                    </div>
                    <div class="col-md-12 panel-collapse collapse-in form-group" style="margin-top:10px;margin-bottom: 5px" id="organism_collapse_div">
                        <select class="selectpicker form-control" id="organisms" data-style="btn-default">
                            <option value="Fly">Fly</option>
                            <option value="Human">Human</option>
                            <option value="Mouse">Mouse</option>
                        </select>                 
                    </div>
                    <!--Select type of analysis -->
                    <div class="col-md-12 title" style="margin-top:15px">
                        <a data-toggle="collapse" href="#analysis_collapse_div">
                            Analysis
                        </a>
                        <!--<button type="button" class="btn btn-default btn-xs question" data-toggle="modal" data-target="#analysisPopup"><i class="fa fa-question-circle-o" aria-hidden="true"></i>
                            <span id="question-analysis" class="question-tooltip" style="width: 170px;height: 60px;left: 70%;">Select an analysis below<br>or click me to see<br>their descriptions</span>
                        </button>-->
                    </div>
                    <div id ="analysis_collapse_div" class="col-md-12 panel-collapse collapse-in" style="padding: 0 0 0 0px">
                        <div class="col-md-12 radio" style="font-size:12px;margin:10 0 0 0px">
                            <label><input type="radio" id="scatterplotanalysis" name="analysis" value="scatterplotanalysis" checked="checked">Descriptive Plots</label>
                        </div>
                        <div class="col-md-12 radio" style="font-size:12px;margin:0px">
                            <label><input type="radio" id="pcanalysis" name="analysis" value="pcanalysis">Principal Components</label>
                        </div>
                        <div class="col-md-12 radio" style="font-size:12px;margin:0px">
                            <label><input type="radio" id="heatmapanalysis" name="analysis" value="heatmapanalysis">Heat Map</label>
                        </div>
                    </div>
                    <!--Grouping for Mutation analysis -->
                    <div class="col-md-12 title group-div" style="margin-top:15px;display:">
                        <a data-toggle="collapse" href="#groups_collapse_div"> 
                            Groups
                        </a>
                        <font style="text-transform:lowercase;font-size:65%;color:#929292">(optional)</font>
                        <!--<button type="button" class="btn btn-default btn-xs question" data-toggle="modal" data-target="#popup"><i class="fa fa-question-circle-o" aria-hidden="true"></i>
                            <span id="question-group" class="question-tooltip" style="width: 175px;height: 100px;left: 95%;">Add up to 6 groups<br>Create and select the group<br>to add data below<br>Click x to remove<br>the selected group</span>
                        </button>-->
                    </div>
                    <div class="col-md-12 panel-collapse collapse-in" style="padding: 0 0 0 0px" id="groups_collapse_div">
                        <div class="col-md-12 group-div form-group" style="margin-top:10px;margin-bottom:5px;" id="groups_div">
                            <select class="selectpicker form-control" id="groups" data-style="btn-default" title="Create Groups">
                            </select>
                        </div>

                        <div class="col-md-12" style="text-align:right;margin-top:0px;">
                            <button id = "removegroup" type="button" class="btn btn-xs btn-default databutton">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>     Remove
                                <!-- <span class="databutton-tooltip" style="width: 165px;height: 40px;left: 62%;">Click to remove selected<br>data from the list above</span> -->
                            </button>
                        </div>


                    </div>
                    <!--Folders to select data -->
                    <div class="col-md-12 title" style="margin-top:15px;">
                        <a data-toggle="collapse" href="#select_collapse_div"> 
                        Select Data
                        </a>
                    </div>
                    <div class="col-md-12 panel-collapse collapse-in" style="padding: 0 0 0 0px" id="select_collapse_div">
                        <div class="col-md-12 form-group" style="margin-top:10px; margin-bottom:0px;">
                            <select class="selectpicker form-control" id="folders" data-style="btn-default" title="Pick dataset" >
                            </select>
                        </div>
                        <!--SubFolders to select data -->
                        <div class="col-md-12 form-group" style="margin-top:10px; margin-bottom:0px;display:none" id="subfolders-div">
                            <select class="selectpicker form-control" id="subfolders" data-style="btn-default" title="Pick subset">
                            </select>
                        </div>
                        <!--Select data -->
                        <div class="col-md-12 form-group" style="margin-top:10px; margin-bottom:0px;">
                            <select class="selectpicker form-control" MULTIPLE id="files" data-style="btn-default" title="Pick samples"  data-actions-box="true" data-selected-text-format="static">
                            </select>
                        </div>
                        <!--box to store selected data --> 
                        <div class="col-md-12 selectionbox" style="margin-top:10px" id="selected-sample_div">
                            <form id="form1">
                                <select name="file_list" SIZE="4" class="form-control" MULTIPLE   id="selected-sample" style="font-size: 14px">
                                </select>
                            </form>
                        </div>
                        <!--buttons to edit selected data -->
                        <div class="col-md-12" style="text-align:right">
                            <button id = "delete-selected" type="button" class="btn btn-xs btn-default databutton">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>     Remove
                                <!-- <span class="databutton-tooltip" style="width: 165px;height: 40px;left: 62%;">Click to remove selected<br>data from the list above</span> -->
                            </button>

                            <button id = "clear-all" type="button" class="btn btn-xs btn-danger databutton">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true">
                                </span> 
                                Clear
                                <!-- <span class="databutton-tooltip" style="width: 165px;height: 40px;left: 95%;">Click to remove all<br>data from the list above</span> -->
                            </button>
                        </div>
                    </div>
                    <!-- Data Ranges -->
                    <div class="col-md-12 title" style="margin-top:15px">
                        <a data-toggle="collapse" href="#range_collapse_div">
                            Data Range
                        </a>
                        <!--<button type="button" class="btn btn-default btn-xs question" data-toggle="modal" data-target="#popup"><i class="fa fa-question-circle-o" aria-hidden="true"></i>
                            <span id="question-organism" class="question-tooltip" style="width: 185px;height: 80px;left: 85%;">Only data points within<br>the following data range will<br>be included in the analysis.<br>Default is -20 to 20</span>
                        </button>-->
                    </div>
                    <div class="col-md-12 panel-collapse collapse-in" style="margin-top:10px;text-align:center" id="range_collapse_div">
                        <input id="lower_limit" type="text" size=4 value=-20> - <input id="upper_limit" type="text" size=4 value=20> 
                    </div>
                    <!--warning area -->
                    <div class="col-md-12" id="warning" style="margin-top:10px">
                    </div>
                    <div style="display:none" class="col-md-12" id="groupwarning" style="margin-top:10px">
                        <font color = "red">No more than 6 groups!</font>
                    </div>
                    <!--the GO button -->
                    <div class="col-md-12" style="margin-top:10px;text-align: center">
                        <button id = "compareButton" class="btn btn-success">Compare</button>
                    </div>
                    <!--Download area -->
                    <div class="col-md-12 title" style="margin-top:15px">
                        <a data-toggle="collapse" href="#download_collapse_div">
                            Download
                        </a>
                    </div>
                    <div class="col-md-12 panel-collapse collapse-in" style="margin-top:10px;text-align:center" id="download_collapse_div">
                        <button id="downloadsvg" type="button" class="btn btn-default btn-xs disabled">Image (.png)</button>
                        <button id="downloadText" type="button" class="btn btn-default btn-xs disabled">Data (.txt)</button>
                    </div>
                    <div class="col-md-12"><hr>
                    </div>
                </div>
                <div id="rowtip1" class="row tip" style="margin-top:0px;"></div>
                <div id="rowtip2" class="row tip" style="margin-top:0px;"></div>
            </div>
        </div>
        <div class="col-md-10">
            <div id = "svgs-all" class="col-md-12">
                <div style = "text-align: center; padding-top: 50px"><font size="+2em"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Start the analysis by making your selections!</font> <br>
                    <font size="+1.2em">To know more about how to perform analyses, please check out the <a href="about.html#analysis"><font color="#24d8ba"><u>About</u></font></a> page</font>
                </div>
            </div>   
        </div>

    </div>
    <canvas width=1000 height = 700 id="canvasDownload" style="display:none"></canvas>
    <canvas width=1600 height = 1600 id="canvasDownloadPCA" style="display:none"></canvas>
    <canvas width=1000 height = 700 id="test2" style="display:none"></canvas>

    <!--
    <div class="database-modal modal fade" id="popup" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="container">
                <div class="row">
                <div class="col-lg-12" style="text-align:right;padding:0px 50px;"><button type="button" class="btn btn-primary btn-lg dismiss" data-dismiss="modal"><i class="fa fa-times"></i> Close </button></div>
                <div class="col-md-12">
                    <div class="modal-body" style="text-align:left">
                        <h2>Instructions</h2>
                        <div class="col-md-12" style='font-size:18px'>
                                <div class="col-md-3 col-sm-6 database-item">
                                    <img src="img/popup/organism.jpg" class="img-responsive" alt="">
                                    <div class="database-caption">
                                        <h4>1. Choose a database</h4>
                                        <p class="text-muted">Choose the database with data you would like to analyse.<br><br>
                                        This will reload the page with data of that organism, both the ones in our database and those you have uploaded, which you could select and perform analysis with.</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 database-item">
                                    <img src="img/popup/analysis.jpg" class="img-responsive" alt="">
                                    <div class="database-caption">
                                        <h4>2. Pick an analysis</h4>
                                        <p class="text-muted">Pick the analysis you would like to perform with your data.<br><br>
                                        For details of each type of analysis, click the <i class="fa fa-question-circle-o" aria-hidden="true"></i> next to <font color="black">ANALYSIS</font>.</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 database-item">
                                    <img src="img/popup/groups.jpg" class="img-responsive" alt="">
                                    <div class="database-caption">
                                        <h4>3. Create groups (optional)</h4>
                                        <p class="text-muted">Create up to 6 groups for your analysis.<br><br>
                                        <b>Descriptive Plots</b> and <b>Clustering Heat Map</b>: The <b><u>mean values</u></b> of the groups instead of individual samples will be used to make the plots.<br>
                                        <b>Principle Component Analysis</b>: Samples will be colored differently according to the grouping.</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 database-item">
                                    <img src="img/popup/data.jpg" class="img-responsive" alt="">
                                    <div class="database-caption">
                                        <h4>4. Select data</h4>
                                        <p class="text-muted">Select samples and perform analysis.<br><br>
                                        <b>Add samples to groups</b>: Select a group that you have created and pick the samples.<br>
                                        <b>If no groups are created</b>, samples will be analysed individually. You could add up to 6 individual samples or groups for Descriptive Plots and 100 samples for Heat Map.</p>
                                    </div>
                                </div>
                        </div> 
                        <div class="col-md-12" style='font-size:18px'>
                            <label style="margin-left:10px;font-size:12px"><input type="checkbox" id="dismissCheckbox" style="margin-left:10px;font-size:14px">Don't show again
                            </label>
                        </div>                    
                    </div>   
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="database-modal modal fade" id="analysisPopup" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="container">
                <div class="row">
                <div class="col-lg-12" style="text-align:right;padding:0px 50px;"><button type="button" class="btn btn-primary btn-lg dismiss" data-dismiss="modal"><i class="fa fa-times"></i> Close </button></div>
                <div class="col-md-12">
                    <div class="modal-body" style="text-align:left">
                        <h2>Pick one of the analysis:</h2>
                        <div class="col-md-12" style='font-size:18px'>
                                <div class="col-md-4 col-sm-6 database-item">
                                    <a href="#" onclick="return changeAnalysis('scatterplotanalysis');" class="database-link" data-dismiss="modal">
                                    <div class="database-hover">
                                        <div class="database-hover-content">
                                        Choose this analysis
                                        </div>
                                    </div>
                                    <img src="img/analysis_popup/scatterplot.png" class="img-responsive" alt="">
                                </a>
                                    <div class="database-caption">
                                        <h4>Descriptive Plots</h4>
                                        <p class="text-muted"> Users could produce some descriptive plots on mutation and expression data for up to 6 individual samples, or 6 groups of multiple samples (group means) with this option.<br><a data-toggle="modal" href="#descriptiveAnalysisPopup">Click me for details</a></p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 database-item">
                                    <a href="#" onclick="return changeAnalysis('pcanalysis');" class="database-link" data-dismiss="modal">
                                    <div class="database-hover">
                                        <div class="database-hover-content">
                                        Choose this analysis
                                        </div>
                                    </div>
                                    <img src="img/analysis_popup/PCA.png" class="img-responsive" alt="">
                                </a>
                                    <div class="database-caption">
                                        <h4>Principle Component Analysis</h4>
                                        <p class="text-muted">Here are some descriptions...<br><a data-toggle="modal" href="#PCAnalysisPopup">Click me for details</a></p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 database-item">
                                    <a href="#" onclick="return changeAnalysis('heatmapanalysis');" class="database-link" data-dismiss="modal">
                                    <div class="database-hover">
                                        <div class="database-hover-content">
                                        Choose this analysis
                                        </div>
                                    </div>
                                    <img src="img/analysis_popup/heatmap.png" class="img-responsive" alt="">
                                </a>
                                    <div class="database-caption">
                                        <h4>Heatmap with Clustering</h4>
                                        <p class="text-muted">Here are some descriptions...<br><a data-toggle="modal" href="#HMAnalysisPopup">Click me for details</a></p>
                                    </div>
                                </div>
                        </div>                       
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="database-modal modal fade" id="descriptiveAnalysisPopup" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="container">
                <div class="row">
                <div class="col-lg-12" style="text-align:right;padding:0px 50px;"><button type="button" class="btn btn-primary btn-lg dismiss" data-dismiss="modal"><i class="fa fa-times"></i> Close </button></div>
                <div class="col-md-12">
                    <div class="modal-body" style="text-align:left">
                        <h2>Descriptive Plots</h2>
                        <div class="col-md-12" style='font-size:18px;text-align:center'>
                            <img src="img/analysis_popup/scatterplot.png" class="img-responsive" alt="">
                        </div>                       
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="database-modal modal fade" id="PCAnalysisPopup" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="container">
                <div class="row">
                <div class="col-lg-12" style="text-align:right;padding:0px 50px;"><button type="button" class="btn btn-primary btn-lg dismiss" data-dismiss="modal"><i class="fa fa-times"></i> Close </button></div>
                <div class="col-md-12">
                    <div class="modal-body" style="text-align:left">
                        <h2>Principle Component Analysis</h2>
                        <div class="col-md-12" style='font-size:18px;text-align:center'>
                            <img src="img/analysis_popup/PCA.png" class="img-responsive" alt="">
                        </div>                       
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="database-modal modal fade" id="HMAnalysisPopup" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="container">
                <div class="row">
                <div class="col-lg-12" style="text-align:right;padding:0px 50px;"><button type="button" class="btn btn-primary btn-lg dismiss" data-dismiss="modal"><i class="fa fa-times"></i> Close </button></div>
                <div class="col-md-12">
                    <div class="modal-body" style="text-align:left">
                        <h2>Heatmap with Clustering</h2>
                        <div class="col-md-12" style='font-size:18px;text-align:center'>
                            <img src="img/analysis_popup/heatmap.png" class="img-responsive" alt="">
                        </div>                       
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    -->
        <!--This loads the info at sidebar and control its behavior -->
        <script src="./js/compare/sidebar.js" session-id="<?php echo $id; ?>" organism="<?php echo $organism; ?>" files="<?php echo $jsarray; ?>" file_directory = '<?php echo json_encode($rows); ?>'></script>
        <!-- The complied scripts for the three viz -->
        <script src = "./js/compare/DesPlot.js"></script>
        <script src = "./js/compare/PCA.js"></script>
        <script src = "./js/compare/Heatmap.js"></script>
        <!-- Initiate the analysis and call the corresponding scripts to carry out viz -->
        <script files="<?php echo $jsarray; ?>" session-id="<?php echo $id; ?>" organism="<?php echo $organism; ?>" host="<?php echo $host; ?>" port="<?php echo $port; ?>" user="<?php echo $user; ?>" passwd="<?php echo $passwd; ?>" unix_socket="<?php echo $unix_socket; ?>" src="./js/compare/svgs.js" ></script>
        <!-- scripts for downloading the images as png -->
        <script src = "./js/compare/html2canvas.js"></script>
        <script src = "./js/compare/saveSvgAsPng.js"></script>
        <script src = "./js/compare/downloadSvg.js"></script>
    </body>
</html>
