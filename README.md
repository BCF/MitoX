# Expression and Mutation Analysis Pipeline for RNA-seq Data

The following pipeline follows [GATK Best Practice](https://dev.mysql.com/downloads/mysql/)

## Set up your local server

**1. Uncomment following lines in /etc/apache2/httpd.conf**
 
    LoadModule include_module libexec/apache2/mod_include.so  
    LoadModule cgi_module libexec/apache2/mod_cgi.so  
    LoadModule userdir_module libexec/apache2/mod_userdir.so  
    LoadModule rewrite_module libexec/apache2/mod_rewrite.so  
    LoadModule php5_module libexec/apache2/libphp5.so  
 
    Include /private/etc/apache2/extra/httpd-userdir.conf 


**2. Uncomment following line in /etc/apache2/extra/httpd-userdir.conf** 
 
    Include /private/etc/apache2/users/*.conf 
  

**3. Create personal config file /etc/apache2/users/(your user name).conf** 
 
    <Directory "/Users/(your user name)/Sites/"> 
        AddLanguage en .en
        Options Indexes MultiViews
        AllowOverride None
        Order allow,deny
        Allow from localhost
        Require all granted
    </Directory> 
  
   
**4. Create Sites folder (if you computer doesn't already have one), and then a html- and php-file under this folder for testing** 

To create Sites folder:
    
    mkdir ~/Sites
    
To create index.html and info.php:
    
    echo "<html><body><h1>My site works</h1></body></html>" > ~/Sites/index.html   
    echo "<?php echo phpinfo(); ?>" > ~/Sites/info.php
 
**5. (Re)Start apache webserver and test configuration** 
 
    sudo apachectl start | restart

**6. Now you could test if the local server is set up successfully**

To test the html and php page go to:

    http://localhost/~(your user name)
    http://localhost/~(your user name)/info.php 
 
**Also visit https://discussions.apple.com/docs/DOC-3083 for installation steps of your MacOS**



## Set up MySQL database

Simply download the MySQL **"DMG Archive"** from [https://dev.mysql.com/downloads/mysql/](https://dev.mysql.com/downloads/mysql/) and install it according to the instructions. Then export the path for MySQL so it could be accessed anywhere at the Terminal

    export PATH=${PATH}:/usr/local/mysql/bin/
	echo 'export PATH="/usr/local/mysql/bin:$PATH"' >> ~/.bash_profile

## Set up MitoX
 
**1. Clone the mitoX project from the repository in ~/Sites/ (You need to provide your user name and password)** 
	
    cd ~/Sites
    git clone https://framagit.org/BCF/MitoX.git


**2. Install all the python libraries needed**
	
    cd MitoX/python
    python setup.py
	
	
**3. Adjust Apache and php parameter allow Python cgi wrapper script execution and file uploads and restart apache**

Change Apache parameter (edit /etc/apache2/users/(your user name).conf):  
    
    <Directory "/Users/(your user name)/Sites/MitoX2/">
       Order allow,deny
       Allow from all
       Options ExecCGI
       Addhandler cgi-script .py
    </Directory>
    
Change php parameter:
First locate the php.ini file (Loaded Configuration File:)

    php --ini

OR

    locate php.ini

Change these lines to allow the max file size:

    upload_max_filesize = 10M
    post_max_size = 10M

To restart apache: 
    
    sudo apachectl -k restart
    sudo apachectl graceful
    

**4. Import MySQL database**

Change directory to the folder below and start mysql (you have to log in as root user and make sure it's already running - simply check the MySQL Panel at System Preference):

    cd ~/Sites/MitoX/mysql/
    mysql -u root -p

**Inside MySQL:**
    
    mysql> source Human.sql
    mysql> source Fly.sql
    
**Quit MySQL and add the config file (~/Sites/MitoX/mysql/mysql_info.json), using mysql_info_template.json as template:**

To check your port number and socket:
    
    mysqladmin -u root -p variable | grep port
    mysqladmin -u root -p variable | grep socket
    
If your socket is /tmp/mysql.sock but no /var/mysql/mysql.sock you should:

    cd /var 
    mkdir mysql
    cd mysql
    ln -s /tmp/mysql.sock mysql.sock

**5. Allow files to be uploaded to user_uploads folder**
    
    Change the rights of the folder:
    chmod o+w ~/Sites/MitoX/data/user_uploads/
    
    Or change the ownership to _www (the web server):
    1) Add yourself to the _www:
        sudo dseditgroup -o edit -a (your username) _www
    2) Change the ownership:
        sudo chown _www ~/Sites/MitoX/data/user_uploads/
 
 
**5. Test if everything is working fine**

To test Python cgi and MySQL database connection:
    
    http://localhost/~(your user name)/MitoX/python/test.py

If there's an error check the apache error log:
    
    sudo tail /var/log/apache2/error_log
    
To test the visualization tool go to: 

    http://localhost/~(your user name)/MitoX/compare.php
    
To test the upload function go to:

    http://localhost/~(your user name)/MitoX/upload.html

Sample files are located at:

	~/Sites/MitoX2/data/zzraw_files/aneuploidy/HCT116-5-4_newexp.csv (expression)
	~/Sites/MitoX2/data/zzraw_files/aneuploidy/HCT116-5-4_newmut.csv (mutation)
	


## Setup for Linux

1. Install apache2, php, libapache2-mod-php, python2.7, python-pandas, python-mpld3, python-sklearn, python-seaborn version 0.7.1, python-matplotlib, python-jinja2,  
2. Activate modules 

	a2enmod cgi
	
	a2enmod userdir
	
	a2enmod rewrite
	
	a2emmod php7
	
	systemctl apache2 restart

3. Activate cgi to execute python and R scripts
	In /etc/apache2/mods-available/mime/conf
	Uncomment line cgi-script and add the file extensions for python, perl or R
	
	AddHandler cgi-script .cgi .py 
	
	(.pl .R scripts not needed anymore)

4. Add mitox.com.conf file from MitoX repo to the /etc/apache2/sites-enabled directory.
5. Delete the old symbolic link of original default config if necessary, for example :
	
	sudo a2dissite 000-default
	sudo systemctl reload apache2	

6. Enable the new mitox website
	
	sudo a2ensite mitox.com
	sudo systemctl reload apache2

7. Give the rights to read, write and execute the user_uploads directory under MitoX/data to user www-data. 

# Authors and contributions:
Annie Yim (main author), Adrien Bonnard (implemented Drosophila mito-database, implemented upload function, helped in coding)
Prasanna Koti (started mitoX project, implemented first version), Edlira Nano (supervised code transfer, helped in supervision)
Jose Villaveces (implemented interactome view), Salma Gamal (implementation of Mutation Analysis Pipeline)
Fabio Marchiano (current main developer), Bianca Habermann (conceived and supervised mitoX project)

# Funding:
Annie Yim was funded by the DFG project 'CancerSysDB' awarded to Bianca Habermann
This work was partially funded by the IFB project [T5](https://www.france-bioinformatique.fr/fr/projets2015/t5-project)
Adrien Bonnard was financed by the Aix-Marseille University, INSERM, TAGC U1090 and Aix-Marseille University, CNRS, IBDM UMR 7288
Fabio Marchiano was financed by ERC grant FLIGHT MUSCLE awarded to Frank Schnorrer (Project ID: 310939)
This work is supported by the Max Planck Society and the CNRS
The continuation of this work is made possible by ANR grant MITO-DYNAMICS awarded to Bianca Habermann and the
MITO-DYNAMICS consortium (Frank Schnorrer, Alice Carrier, Benoit Giannesini)

