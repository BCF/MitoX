import pandas as pd
import pymysql
import datetime as DT
from datetime import datetime
import sys

organism = sys.argv[1]
today = datetime.today().date()
weekago = (today - DT.timedelta(days=7)).strftime('%Y%m%d')
#weekago = "20171231"

expression = 0
mutation = 0
target_exp = 0
target_mut = 0
file_directory = 0

##connecting to mysql database
conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db=organism)
cur = conn.cursor()
query= 'SELECT userID, sampleID from file_directory WHERE datetime < '+weekago
df = pd.read_sql(query, con=conn)
queryd = 'WHERE (userID,sampleID) in ('

for index, row in df.iterrows():
    if (index != 0):
        queryd = queryd + ','
    queryd = queryd + '("'+row['userID']+'","'+row['sampleID']+'")'
queryd = queryd + ')'

if (len(df.index) > 0):
    query = 'DELETE from expression '+queryd
    expression = cur.execute(query)
    conn.commit()
    query = 'DELETE from mutation '+queryd
    mutation = cur.execute(query)
    conn.commit()
    query = 'DELETE from target_exp '+queryd
    target_exp = cur.execute(query)
    conn.commit()
    query = 'DELETE from target_mut '+queryd
    target_mut = cur.execute(query)
    conn.commit()
    query = 'DELETE from file_directory '+queryd
    file_directory = cur.execute(query)
    conn.commit()

conn.close()

print("rows of expression table affected: "+ str(expression))
print("rows of mutation table affected: "+ str(mutation))
print("rows of target_exp table affected: "+ str(target_exp))
print("rows of target_mut table affected: "+ str(target_mut))
print("rows of file_directory table affected: "+ str(file_directory))
