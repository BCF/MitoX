import pandas as pd
import pymysql

organism = "Human"
target_path = "/Users/ayim/Sites/MitoX2/dist/main_files/human/gene_function.txt"


conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db=organism, local_infile=True)
cur = conn.cursor()

cur.execute("DROP TABLE IF EXISTS target")
query = "CREATE TABLE target (gene varchar(50), process varchar(100), chr varchar(5), gene_function varchar(100), ENSG varchar(25))"
cur.execute(query)
query = "LOAD DATA LOCAL INFILE '"+target_path+"' INTO TABLE target FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
cur.execute(query)
conn.commit()

query = 'SELECT gene from target'
genes = pd.read_sql(query,con=conn)
genes = genes['gene'].unique()
    
cur.execute("DROP TABLE IF EXISTS target_exp")
cur.execute("DROP TABLE IF EXISTS target_mut")

query = 'CREATE TABLE target_exp SELECT * FROM expression WHERE gene in ('+','.join(map("'{0}'".format, genes))+')'
cur.execute(query)
query = 'CREATE TABLE target_mut SELECT * FROM mutation WHERE gene in ('+','.join(map("'{0}'".format, genes))+')'
cur.execute(query)
conn.commit()

conn.close()


