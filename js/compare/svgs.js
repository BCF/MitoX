$(document).ready(function(){

function setAffixContainerSize(){
    $('#sidebar').width($('#sidebar').parent().innerWidth());
}

//already setting the min width when it's loaded
setAffixContainerSize();

$(window).resize(function(){
    setAffixContainerSize();
});
    
var this_js_script = $('script[src*=svgs]'),
    sessionid = this_js_script.attr('session-id'),
    organism = this_js_script.attr('organism'),
    host = this_js_script.attr('host'),
    port = this_js_script.attr('port'),
    user = this_js_script.attr('user'),
    passwd = this_js_script.attr('passwd'),
    unix_socket = this_js_script.attr('unix_socket');

var files = this_js_script.attr('files');
     
if (files) initAnalysis();
$("#compareButton").on("click" ,function(){
    initAnalysis();
})
    
function initAnalysis(){
    
    document.getElementById('warning').innerHTML="";
    
    //Get a list of selected samples here
    var jsonGroupCount = 0;
    var json = {};
    var element = document.getElementsByClassName('group_selection');
    for (i=0; i<element.length; i++){
        
        select = document.getElementById(element[i].id);
        var selectionLength = select.options.length;
        
        if (selectionLength > 0){
            
            arr = [];
            
            for (j = 0; j < selectionLength; j++) {
                arr[j] = select.options[j].value;
                jsonGroupCount += 1;
            }
            json[element[i].id] = arr;
        }  
        
    }
    
    //Get arrays of selected samples if group's not defined  
    if (element.length === 0){
        
        select = document.getElementById('selected-sample');

        json = [];
        for (i = 0; i < select.options.length; i++) {
           json[i] = select.options[i].value;
        }
    }
    
    var lower_limit = document.getElementById('lower_limit').value;
    var upper_limit = document.getElementById('upper_limit').value;
    
    var parameter = 'jsons=' + JSON.stringify(json) + '&organism='+ organism +'&sessionid='+ sessionid + '&host='+host + '&port='+port + '&user='+user + '&passwd='+passwd + '&unix_socket='+unix_socket + '&lower_limit='+lower_limit + '&upper_limit='+upper_limit;
    
    var svg = "svgs-all";
    
    //Add loading gif here
    var el = document.getElementById( 'svgs-all' );
    while (el.hasChildNodes()) {el.removeChild(el.firstChild);}
    var div = document.createElement('div');
    div.setAttribute("align", "center");
    div.innerHTML ='<img id="loading" src="./img/loading.gif">';
    el.appendChild(div);
    
    //Determine which analysis has to be performed and initiate it
    var analysis = document.querySelector('input[name = "analysis"]:checked').value;
    

    $('#rowtip1').empty();
    $('#rowtip2').empty();
    $('#rowtip2').css('display', 'none');
    $('#rowtip1').css('display', '');

    $('#downloadsvg').addClass('disabled');
    $("#downloadText").addClass('disabled')

    if (analysis == "scatterplotanalysis"){
        //$("#sidebar").css("position","fixed")
        DesPlot.init(json,parameter,svg,"./python/SP_mysql.py",onError);
        $('#downloadsvg').removeClass('disabled');
        $("#downloadText").removeClass('disabled')
        
    } 
    else if (analysis == "pcanalysis"){
        //$("#sidebar").css("position","fixed")
        PCA.init(json,jsonGroupCount,sessionid,parameter,svg,"./python/PCA_mysql.py",onError);      
        $('#downloadsvg').removeClass('disabled');
        
    } 
    else {
        //$("#sidebar").css("position","fixed")
        Heatmap.init(json,jsonGroupCount,sessionid,parameter,svg,["./python/heatmap_getprocess.py","./python/heatmap_mysql.py"],onError);
        $('#downloadsvg').removeClass('disabled');
    }
}
    
function onError(res) {
    d3.select('#loading').remove();
    document.getElementById('warning').innerHTML="<font color=\"red\">"+res;
    throw new Error("Something went badly wrong!");
}
    
})
