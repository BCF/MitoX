var this_js_script = $('script[src*=userFile]'),
    sessionid = this_js_script.attr('session-id'),
    host = this_js_script.attr('host'),
    port = this_js_script.attr('port'),
    user = this_js_script.attr('user'),
    passwd = this_js_script.attr('passwd'),
    unix_socket = this_js_script.attr('unix_socket'),
    organism;

function deleteSelected(project){
    var selects = $('#table').bootstrapTable('getSelections');
    sampleIDs = $.map(selects, function (row) {
        return row.sampleID;
    });
    $("#confirm-delete").modal()
    
    var parameter = 'project='+project + '&organism='+organism + '&sessionid='+sessionid + '&host='+host + '&port='+port + '&user='+user + '&passwd='+passwd + '&unix_socket='+unix_socket + '&sampleID='+ JSON.stringify(sampleIDs);
    
    console.log(parameter)
    
    document.getElementById('deleteSamples').onclick = function() {
        
        jQuery.ajax({
            url: "./python/delete_table.py",
            data: parameter,
            type: "POST",
            error: function (e) {
                console.log(e);
            },
            success: function (response) {
                console.log(response)
                $('#table').bootstrapTable('remove', {
                    field: 'sampleID',
                    values: sampleIDs
                });
            }
        });
        
    }
}

function createTable(project,title,organismInput){
    
    organism = organismInput
    
    document.getElementById("beforedb").style.display = "none";
    document.getElementById("interactome").style.display = "none";
    document.getElementById("visside").style.display = "none";
    document.getElementById("db").style.display = "";
    
    var parameter = 'project='+project + '&organism='+organism + '&sessionid='+sessionid + '&host='+host + '&port='+port + '&user='+user + '&passwd='+passwd + '&unix_socket='+unix_socket
    var checkedRows = [];

    
    document.getElementById("database-head").innerHTML = title;
    $( "#loading" ).css('display','');
    document.getElementById("userfiles").innerHTML=""
  
    $("#deleteSelected").remove();
    
    if (project == "User_upload"){
        var childNode = document.createElement("button");
        childNode.className = "btn btn-danger";
        childNode.id = "deleteSelected";
        childNode.innerHTML = "Delete"
        childNode.setAttribute("onClick", "deleteSelected('"+project+"')")
        document.getElementById("buttons").appendChild(childNode); 
    }
 

    
    var table = document.createElement("TABLE");
    table.setAttribute("id", "table");
    table.setAttribute("style", "font-size:14px");
    document.getElementById("userfiles").appendChild(table)
    
    jQuery.ajax({
        url: "./python/create_table.py", 
        data: parameter,
        type: "POST",
        //dataType: "json",    
        success: function (jsonData) {
            $( "#loading" ).css('display','none');
            if(typeof jsonData != 'object'){
                checkedRows = [];
                message = "Error connecting to the database, please try again later!";
                if (project == "User_upload") message = "It appears that you haven't uploaded anything yet!"
                document.getElementById("userfiles").innerHTML=""
                var div = document.createElement("div");
                div.setAttribute("class", "col-md-12");
                div.setAttribute("style", "font-size:14px");
                div.innerHTML="<br><br><b>"+message+"</b>"
                document.getElementById("userfiles").append(div)
            }else{
                checkedRows = [];
                var column = [{
                        field: 'state',
                        checkbox: true
                        },{
                        field: 'sampleID',
                        title: 'Sample ID',
                        sortable: true,
                        formatter: function sampleFormatter(value){
                            return '<a href="mitomodel.php?sampleID='+value+'&organism='+organism+'&sessionid='+sessionid+'" target="_blank"><span class="glyeye glyphicon glyphicon-eye-open" aria-hidden="true" style="font-size: 1.2em;color:black;padding-right:10px"></span></a>'+value
                        },
                        cellStyle: function cellStyle(){
                            return {
                            css: {"white-space": "nowrap"}
                          };
                        }
                        }];
                keys = Object.keys(jsonData[0])
                for (var i = 0; i < keys.length; i++){
                    var entry = new Object();
                    entry.field = keys[i];
                    entry.title = keys[i].charAt(0).toUpperCase() + keys[i].slice(1);
                    entry.sortable = true
                    if (keys[i] != "sampleID" && keys[i] != "cancer_type" && keys[i] != "flySex") column.push(entry);
                }
                $('#table').bootstrapTable({
                    search: true,
                    pagination: true,
                    columns: column,
                    data: jsonData,
                    clickToSelect: true
                });

                $('#table').on('check.bs.table', function (e, row) {
                    //console.log(row.sampleID)
                  checkedRows.push(row.sampleID);
                });

                $('#table').on('uncheck.bs.table', function (e, row) {
                  $.each(checkedRows, function(index, value) {
                    //console.log(row.sampleID)
                      if (value === row.sampleID) checkedRows.splice(index,1)
                  });
                });
                
                $('#table').on('check-all.bs.table', function (e,rows) {
                    checkedRows = []
                    $.each(rows, function(index,value){
                        //console.log(value.sampleID)
                        checkedRows.push(value.sampleID)
                    })
                });
                
                $('#table').on('uncheck-all.bs.table', function (e,rows) {checkedRows = []});
            }
            
        },error: function(e){
            console.log(e);
        }})

    $("#readygo").click(function() {
        //console.log(checkedRows)
        if (checkedRows.length == 0){
           onError(new Error('Please select samples!!'));
        }
        if (checkedRows.length > 6){
            onError(new Error('Please select 6 samples at most!!'));
        } 
        
        var phptext ='compare.php?organism='+organism
        $.each(checkedRows, function(index, value) {
          phptext = phptext + '&compare%5B%5D='+value+'&'
      });
        window.location.href = phptext;
    });

    
    $(".dismiss").click(function(e){
        $('input:checkbox').removeAttr('checked');
        $(".database").css('background-color', 'white');
    })
    
    $(".close-modal").click(function(e){
        $('input:checkbox').removeAttr('checked');
        $(".database").css('background-color', 'white');
    })
    
    function onError(res) {
    document.getElementById('warning').innerHTML="<font color=\"red\">"+res;
    $( "#warning" ).fadeIn( 300 ).delay( 400 ).fadeOut( 300 );
    throw new Error("Something went badly wrong!");
    }
    
    
}
