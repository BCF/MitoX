<?php 
if (isset($_COOKIE['mitox_session_id'])) { 
        session_id($_COOKIE['mitox_session_id']);
}
if ($_GET['sessionid']) { 
        session_id($_GET['sessionid']);
}
session_start();
$id = session_id();

setcookie('mitox_session_id',$id,time() + (86400 * 7));


//Get info for mysql server
$str = file_get_contents('mysql/mysql_info.json');
$json = json_decode($str, true);
$host = $json['host'];
$port = $json['port'];
$user = $json['user'];
$passwd = $json['passwd'];
$unix_socket = $json['unix_socket'];

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MitoXplorer - Analysis</title>
        <meta charset=utf-8>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <script src="./js/jquery.min.js"></script>
        <script src="//d3js.org/d3.v3.min.js"></script>
        
        <!-- CSS for database -->
        <link href="./css/main.css" rel="stylesheet" >
        <link href="./css/database/database.css" rel="stylesheet" >
        <link href="./css/database/bootstrap-table.min.css" rel="stylesheet" >
        
        <link rel="icon" type="image/png" href="img/logos/favicon.png">
    </head>
    
    <body>

<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-header page-scroll">
               <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".sidebar-nav">
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
               </button>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php">MitoXplorer</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="index.php"></a>
                    </li>
                    <li>
                        <a href="about.html">About</a>
                    </li>
                    <li>
                        <a href="#" style="background-color: #12e2c0;border-radius: 3px;color:white">Database</a>
                    </li>
                    <li>
                        <a href="compare.php">Analysis</a>
                    </li>
                    <li>
                        <a href="upload.php">Upload</a>
                    </li>
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                </ul>
            </div>

        </div>

    </nav>
<div id="content" class="container-fluid">
    
<div class="row row-offcanvas row-offcanvas-left">
    
    <div id="sidebar" class="sidenav col-xs-6 col-sm-6 col-md-3 col-lg-2 sidebar-offcanvas" style="padding:20px 10px 0px 10px; text-align: left;">
        
        <a data-toggle="collapse" href="#collapseInteract" aria-expanded="true" aria-controls="collapseExample" style="margin:10px 10px;font-size:22px" onclick='$("#collapseDatabase").collapse("hide")' class="collapsetitle">Interactome</a>
            
        <div class="rounded collapse" id="collapseInteract" style="margin:0px 10px; padding: 0px 0px;">

            <a href="#" class="interactomeorgs" onclick="return App.init('Human')"><img src="img/database2/human.png" height="42" width="42">Human</a>
            
            <a href="#" class="interactomeorgs" onclick="return App.init('Mouse')"><img src="img/database2/mouse.png" height="42" width="42">Mouse</a>
            
            <a href="#" class="interactomeorgs" onclick="return App.init('Fly')"><img src="img/database2/fly.png" height="42" width="42">Fly</a>
        </div>

        <a data-toggle="collapse" href="#collapseDatabase" aria-expanded="true" aria-controls="collapseExample" style="margin:10px 10px;font-size:22px" onclick='$("#collapseInteract").collapse("hide")' class="collapsetitle">Public Data / My Uploads</a>
            
        <div class="rounded collapse" id="collapseDatabase" style="margin:0px 10px; padding: 0px 0px;">

            <a data-toggle="collapse" href="#collapseHuman" aria-expanded="false" aria-controls="collapseExample"><img src="img/database2/human.png" height="42" width="42">Human</a>
            <div class="collapse sub" id="collapseHuman" style="padding:0px 10px">
                <a href="#" onclick="return createTable('Aneuploidy','Aneuploidy Cell Lines','Human');">Aneuploidy</a>
                <a href="#" onclick="return createTable('Aneuploidy_public','Public Human Data','Human');">Public Data (GEO)</a>
                <a href="#" onclick="return createTable('TCGA','The Cancer Genome Atlas','Human');">The Cancer Genome Atlas</a>
                <a href="#" onclick="return createTable('User_upload','My Data (Human)','Human');">My uploads</a>
            </div>
            
            <a data-toggle="collapse" href="#collapseMouse" aria-expanded="false" aria-controls="collapseExample"><img src="img/database2/mouse.png" height="42" width="42">Mouse</a>
            <div class="collapse sub" id="collapseMouse" style="padding-left:10px">
                <a href="#" onclick="return createTable('Aneuploidy_public_m','Public Mouse Data','Mouse');">Public data (GEO)</a>
                <a href="#" onclick="return createTable('User_upload','My Data (Mouse)','Mouse');">My uploads</a>
            </div>
            
            <a data-toggle="collapse" href="#collapseFly" aria-expanded="false" aria-controls="collapseExample"><img src="img/database2/fly.png" height="42" width="42">Fly</a>
            <div class="collapse sub" id="collapseFly" style="padding-left:10px">
                <a href="#" onclick="return createTable('DGRP','DGRP','Fly');">DGRP</a>
                <a href="#" onclick="return createTable('DEGENES','Time Course','Fly');">Time Course</a>
                <a href="#" onclick="return createTable('FALLEN','TF gene knockout','Fly');">Fallen</a>
                <a href="#" onclick="return createTable('User_upload','My Data (Fly)','Fly');">My uploads</a>
            </div>
        </div>
        
        <div class="row" id="visside" style="margin:20px 0px;display:none">

            <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Search gene by name..." style="font-size:11px">
                <ul class="list-group results"></ul>
            </div>


            <div class="col-md-12 miniTitle" style="margin-top:10px;">
                Legend
            </div>

            <div class="col-md-6 legendText">
                <strong>Color</strong> shows gene regulation
            </div>

            <div class="col-md-6">
                <svg width="90" height="30">
                    <rect x="0" y="10" width="28" height="7" fill="#2171b5"></rect>
                    <rect x="28" y="10" width="28" height="7" fill="#BECCAE"></rect>
                    <rect x="56" y="10" width="28" height="7" fill="#C72D0A"></rect>

                    <text x="5" y="26" class="legendText">&gt;1.5</text>
                    <text x="30" y="26" class="legendText"></text>
                    <text x="55" y="26" class="legendText">&lt;-1.5 </text>
                </svg>
            </div>

            <div class="col-md-12 legendText" style="padding-right:0px">
                <strong>Dark Borders</strong> show mutations<br> <strong>Size</strong> shows Log2 fold change
            </div>

            <div class="col-md-12">
                <hr>
            </div>
            <div class="row tip" style="margin-top:20px;font-size:11px">
            </div>

        </div>
    </div>
    
    <div id="main" class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
        <div id="beforedb">
            <h1>Database Overview</h1>
            <div style="padding:0px 0px 10px 0px"><span style="font-size:16px">Below is a summary of our interactome and the public data we hosted on MitoXplorer.<br>Explore the data by clicking on the side menu.</span></div>
            <div class="col-md-5"><h4>Our Interactome</h4></div>
            <div class="col-md-7"><h4>Our Expression and Mutation Data</h4></div>
            <div class="col-md-12" id="loadingdb" style="text-align:center"><img id="loading" src="./img/loading.gif"></div>
            <div class="col-md-5" id="barchart" style="display:none">
                <div class="col-md-12" style="padding-bottom: 10px; text-decoration: underline">By Processes</div>
                <div class="col-md-12" id="chart" style="padding-left: 0px; padding-right: 0px;"></div>
            </div>
            <div class="col-md-7" id="pies" style="border-left: 4px solid #e5e5e5; display:none">
                <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                    <div class="col-md-12" style="padding-bottom: 10px; text-decoration: underline">By Organisms</div>
                    <div class="col-md-12" id="pie1" style="padding-left: 0px; padding-right: 0px; padding-top:20px"></div>
                </div>
                <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                    <div class="col-md-12" style="padding-bottom: 10px; text-decoration: underline">By Projects</div>
                    <div class="col-md-12" id="pie2" style="padding-left: 0px; padding-right: 0px; padding-top:20px"></div>       
                </div>
                <div class="col-md-12" id="pcinfo"></div>
            </div>
        </div>
        <div id="db" style="display:none" class="col-lg-12">
            <h2 id="database-head">s</h2>
            <div class="showdb" style="padding:0px 0px 10px 0px;text-decoration:underline"><span style="font-size:14px;cursor:pointer" onclick="openNav()">Show database</span></div>
            <div class="col-md-12" style='font-size:14px'>
            <b>Quick Browse</b> <span class="glyphicon glyphicon-eye-open" aria-hidden="true" style="font-size: 1.2em"></span><br>Check out the expression and mutation profile of individual samples in a new window<br><br>
            <b>Comparative Analysis</b> <span class="glyphicon glyphicon-ok" aria-hidden="true" style="font-size: 1.2em"></span><br>Select up to 6 samples from the table and click compare to start the Data Analysis Portal
            <br><br>
            <div id="buttons"><button id="readygo" class="btn btn-success" style="margin-right:10px"> Compare </button></div>
            <div style = "display:none;margin-top:10px" id = "warning"></div>
            </div>
            <div id="loading" style="text-align:center">
            <img src="./img/loading.gif">
            </div>
            <div class="row body" id="userfiles">
            </div><br><br>
        </div>
        <div id="interactome" style="display:none" class="col-lg-12">
            <div id="loadingvis" style="text-align:center;display:none">
            <img src="./img/loading.gif">
            </div>
            <div id="vis" class="vis"></div>
        </div>
    </div>

</div>
</div>
    <script session-id="<?php echo $id; ?>" host="<?php echo $host; ?>" port="<?php echo $port; ?>" user="<?php echo $user; ?>" passwd="<?php echo $passwd; ?>" unix_socket="<?php echo $unix_socket; ?>" src="js/database/userFiles.js"></script>
    <script host="<?php echo $host; ?>" port="<?php echo $port; ?>" user="<?php echo $user; ?>" passwd="<?php echo $passwd; ?>" unix_socket="<?php echo $unix_socket; ?>" src="js/database/AppInteractome.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/database/bootstrap-table.min.js"></script>
    <script>
    /*function openNav() {    
        var p = document.getElementById("content");
        var style = window.getComputedStyle(p);
        var mleft = style.marginLeft.split("px")[0]

        var pnav = document.getElementById("mainNav");
        var stylenav = pnav.currentStyle || window.getComputedStyle(pnav);
        var mheight = stylenav.height.split("px")[0]

        document.getElementById("mySidenav").style.width = "300px";
        document.getElementById("mySidenav").style.marginTop = mheight+"px";
        document.getElementById("main").style.marginLeft = 300-mleft+"px";
        toggleContent();
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        toggleContent();

    }

    function toggleContent(){
        var all = document.getElementsByClassName('showdb');
        for (var i = 0; i < all.length; i++) {
            if (all[i].innerHTML.includes('Show'))
                all[i].innerHTML = '<span style="font-size:14px;cursor:pointer" onclick="closeNav()">Hide sidebar</span>';
            else all[i].innerHTML = '<span style="font-size:14px;cursor:pointer" onclick="openNav()">Show sidebar</span>';
        }
    }
    openNav();*/
    $("#collapseInteract").collapse("show")
    
    $('.interactomeorgs').on({'click': function(){
        $('.interactomeorgs').css('text-decoration','none');
        $('.interactomeorgs').css('color','#818181');
        $(this).css('text-decoration','underline');
    }})
        
    $('.collapsetitle').on({'click': function(){
        $('.interactomeorgs').css('text-decoration','none');
        $('.interactomeorgs').css('color','#818181');
    }})

    $('.interactomeorgs').mouseover(function(){
        $(this).css('color','lightskyblue');
    })

    $('.interactomeorgs').mouseout(function(){
        $(this).css('color','#818181');
    })

    </script>
    <script>
        $(document).ready(function() {
          $('[data-toggle=offcanvas]').click(function() {
            $('.row-offcanvas').toggleClass('active');
          });
        });  
    </script>
    <script host="<?php echo $host; ?>" port="<?php echo $port; ?>" user="<?php echo $user; ?>" passwd="<?php echo $passwd; ?>" unix_socket="<?php echo $unix_socket; ?>" src="js/database/summary.js"></script>
        
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
            </div>

            <div class="modal-body">
                <p>You are about to delete one track, this procedure is irreversible.</p>
                <p>Do you want to proceed?</p>
                <p class="debug-url"></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger btn-ok" id="deleteSamples"  data-dismiss="modal">Delete</button>
            </div>
        </div>
    </div>
</div>
        
</body>
</html>
