#!/usr/bin/env python

import cgi, os, re, sys
import cgitb;cgitb.enable()
import json
import pandas as pd
import pymysql

form = cgi.FieldStorage()
project = form.getvalue('project')
jsons = form.getvalue('sampleID')
sampleID = json.loads(jsons)
sessionid = form.getvalue('sessionid')
organism = form.getvalue('organism')
host = form.getvalue('host')
port = form.getvalue('port')
user = form.getvalue('user')
passwd = form.getvalue('passwd')
unix_socket = form.getvalue('unix_socket')

result = "Fail"

try:
    if (project == "User_upload"):
        conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=organism, unix_socket=unix_socket)
        cur = conn.cursor()
        query = 'DELETE from file_directory WHERE sampleID in ('+','.join(map("'{0}'".format, sampleID))+') AND userID ="'+sessionid+'"'
        cur.execute(query)
        conn.commit()
        query = 'DELETE from target_exp WHERE sampleID in ('+','.join(map("'{0}'".format, sampleID))+') AND userID ="'+sessionid+'"'
        cur.execute(query)
        conn.commit()
        query = 'DELETE from target_mut WHERE sampleID in ('+','.join(map("'{0}'".format, sampleID))+') AND userID ="'+sessionid+'"'
        cur.execute(query)
        conn.commit()
        query = 'DELETE from expression WHERE sampleID in ('+','.join(map("'{0}'".format, sampleID))+') AND userID ="'+sessionid+'"'
        cur.execute(query)
        conn.commit()
        query = 'DELETE from mutation WHERE sampleID in ('+','.join(map("'{0}'".format, sampleID))+') AND userID ="'+sessionid+'"'
        cur.execute(query)
        conn.commit()
        conn.close
        result = "Success"
    else:
        result = "Fail"
except:
    result = "Fail"
    
print 'Content-type: text/html\n'
print (result)
        