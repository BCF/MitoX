#!/usr/bin/env python

#basic libraries
import cgi, os, re, sys
import cgitb;cgitb.enable()
import json
import pandas as pd
import numpy as np

#for PCA
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale

#for heatmap
import collections
import matplotlib
matplotlib.use('Agg')

import mpld3
from mpld3 import utils
from mpld3 import plugins
import jinja2
import seaborn_hm

#for seaborn
import itertools
from matplotlib.collections import LineCollection
import matplotlib.pyplot as plt
from matplotlib import gridspec
from scipy.spatial import distance
from scipy.cluster import hierarchy

from seaborn.axisgrid import Grid
from seaborn.palettes import cubehelix_palette
from seaborn.utils import despine, axis_ticklabels_overlap, relative_luminance

import pymysql

mysql_file = "../mysql/mysql_info.json"
mysql = pd.read_json(mysql_file,typ='series')
host = mysql['host']
port = mysql['port']
user = mysql['user']
passwd = mysql['passwd']
unix_socket = mysql['unix_socket']
organism = "Human"

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=organism, unix_socket=unix_socket)
query = 'SELECT sampleID, folder FROM file_directory LIMIT 10'
file_directory = pd.read_sql(query, con=conn)

print "Content-type: text/html\n"
print "<html>"
print "Congrats! You have everything you need!"
print "<br>"
print "Some data from your database"
print "<br>"
print file_directory
print "</html>"

