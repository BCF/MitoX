#!/usr/bin/env python

import cgi, os, re, sys
import cgitb;cgitb.enable()
import json
import pandas as pd
import pymysql
import numpy as np
import os.path

form = cgi.FieldStorage()
project = form.getvalue('project')
organism = form.getvalue('organism')
sessionid = form.getvalue('sessionid')
host = form.getvalue('host')
port = form.getvalue('port')
user = form.getvalue('user')
passwd = form.getvalue('passwd')
unix_socket = form.getvalue('unix_socket')

#project = 'TCGA'
#organism = 'Human'
#sessionid = 'test2'
#host = "localhost"
#port = 3306
#user = "root"
#passwd = ""
#unix_socket = "/tmp/mysql.sock"

#fname = '../data/user_uploads/'+sessionid+'/file_directory_'+organism+'.json'
#if os.path.isfile(fname):
#    with open(fname) as json_data:
#        d = json.load(json_data)
#    file_directory = pd.read_json(d,orient='records')
#else:
##connecting to mysql database
conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=organism, unix_socket=unix_socket)
query = 'SELECT sampleID, folder,subfolder FROM file_directory WHERE userID in ("mitox","'+sessionid+'") AND folder = "'+ project + '"'
file_directory = pd.read_sql(query, con=conn)
conn.close()

#file_directory_json = file_directory.to_json(orient='records')
#
#with open(fname, 'w') as fp:
#    json.dump(file_directory_json,fp)

if (file_directory.empty):
    print "Content-type: text/html\n"
    print "None"
else:
    file_directory = file_directory.loc[file_directory['folder'] == project]
    file_directory.replace('', np.nan, regex=True,inplace=True)
    file_directory.rename(columns={'subfolder': 'subset'}, inplace=True)
    file_directory.dropna(axis=1,how='all',inplace=True)

    typefile = pd.read_csv("../main_files/filetype.txt",sep="\t")
    try:
        typeurl = typefile['file'][typefile['filetype'] == project].values[0]
        info = pd.read_csv(typeurl,sep="\t")
        
    except:
        info = pd.DataFrame()
    
    if not info.empty:
        summary = pd.merge(file_directory,info,on='sampleID',how='left')
    else:
        summary = file_directory
        
    summary.replace("[NA]","",inplace=True)
    summary.replace(np.nan, '', regex=True,inplace=True)
    summary.drop('folder',axis=1,inplace=True)
        
    data = summary.to_json(orient='records')


    
    print 'Content-Type: application/json\n\n'
    print (data)

# print "Content-type: text/html\n"
# print "<html>"
# print file_directory
# print "</html>"