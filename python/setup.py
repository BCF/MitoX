import sys
import os
	
try:
   import pip
except ImportError:
   print "installing pip"
   cmd = "sudo python package/get-pip.py"
   print "Required package is missing\nPlease enter root password to install required package when needed"
   os.system(cmd)

cmd = "sudo pip2.7 install -r package/python-libraries.txt"
print "Required package is missing\nPlease enter root password to install required package when needed"
os.system(cmd)

try: 
   import seaborn
except ImportError:
   print "the library seaborn doesn't exist!" 
   cmd = "sudo pip2.7 install package/seaborn-0.7.1.tar.gz" 
   print "Required package is missing\nPlease enter root password to install required package when needed"
   os.system(cmd)
   
print ("Installation of python libraries is completed")


	
