#!/usr/bin/env python

import cgi
import cgitb;cgitb.enable()
import json
import pandas as pd
import pymysql
#import simplejson

form = cgi.FieldStorage()
host = form.getvalue('host')
port = form.getvalue('port')
user = form.getvalue('user')
passwd = form.getvalue('passwd')
unix_socket = form.getvalue('unix_socket')

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="Human", unix_socket=unix_socket)
query = "SELECT process as name, count(process) as total from target group by process order by total DESC"
process = pd.read_sql(query, con=conn)

query = "select folder as name, count(folder) as total from file_directory group by folder"
dfHuman = pd.read_sql(query, con=conn)
conn.close()
conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="Mouse", unix_socket=unix_socket)
dfMouse = pd.read_sql(query, con=conn)
conn.close()
conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="Fly", unix_socket=unix_socket)
dfFly = pd.read_sql(query, con=conn)

conn.close()

dfHuman = dfHuman[dfHuman.name != "User_upload"]
dfMouse = dfMouse[dfMouse.name != "User_upload"]
dfFly = dfFly[dfFly.name != "User_upload"]
proj = dfHuman.append([dfMouse,dfFly])

orgs = pd.DataFrame(columns=('name', 'total'))
orgs.loc[0]=['Human',dfHuman.total.sum()]
orgs.loc[1]=['Mouse',dfMouse.total.sum()]
orgs.loc[2]=['Fly',dfFly.total.sum()]

#Combine together
process = process.to_json(orient='records')
orgs = orgs.to_json(orient='records')
proj = proj.to_json(orient='records')

json_all = [{'process' : process, 'orgs' : orgs, 'proj' : proj}]
json_all = json.dumps(json_all)

print 'Content-Type: application/json\n\n'
print (json_all)





