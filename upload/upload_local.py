#!/usr/bin/python

import cgi, os, re, sys
#import cgitb;cgitb.enable()
import pandas as pd
import numpy as np
import pymysql
import json
from os import listdir
from os.path import isfile, join
from datetime import datetime

#userID = "testannie"
#organism = "Human"
#exppath = "../data/user_uploads/testannie/expression/"
#mutpath = "../data/user_uploads/testannie/mutation/"
#tmp = [1,2,3,4,5,6,1,2,3,4,5,6,7,8]

error_log = []

mysql = json.load(open("../mysql/mysql_info.json"))

userID = "mitox"
folder = sys.argv[2]
subfolder = ""
organism = sys.argv[1]
exp_date = 20870519
exppath = "exp/"
mutpath = "mut/"
host = mysql['host']
port = mysql['port']
user = mysql['user']
passwd = mysql['passwd']
unix_socket = mysql['unix_socket']


##connecting to mysql database
try:
    conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=organism, unix_socket=unix_socket, local_infile=True)
    cur = conn.cursor()
    query = 'SELECT gene from target'
    genes = pd.read_sql(query,con=conn)
    genes = genes['gene'].unique()
    query = 'SELECT sampleID from file_directory WHERE userID = "'+userID+'"'
    file_directory = pd.read_sql(query, con=conn)
    mysample = file_directory['sampleID'].unique()
except:
    error_log.append("Error occur while connecting to the database")

#Deal with expression file
exp_all = pd.DataFrame()
exp_exist=False

if os.path.isfile(exppath+"all_exp.csv"):
    os.unlink(exppath+"all_exp.csv")
if os.path.isfile(exppath+"file_directory.csv"):
    os.unlink(exppath+"file_directory.csv")
    
for f in listdir(exppath):
    if (isfile(join(exppath,f)) and not f.startswith('.')):
        exp_exist = True
        exp = pd.DataFrame()
        try:
            thisfile = join(exppath,f)
            print(thisfile)
            colnames = ["sampleID","gene","normal","abnormal","log2","pvalue"]
                
            exp = pd.read_csv(thisfile,sep=",")
            exp.columns = colnames
            
            exp['normal'].fillna(0, inplace=True)
            exp['abnormal'].fillna(0, inplace=True)
            exp['pvalue'].fillna(1, inplace=True)
            
            exp['userID'] = userID
            exp = exp[["userID","sampleID","gene","normal","abnormal","log2","pvalue"]]
            exp.dropna(axis=0,how='any',inplace=True)
            
            
            #column checking
            if not (np.issubdtype(exp['log2'].dtype, np.number)):
                error_log.append("The file " + f + " has non-numerical Log2 Fold change value")
                break
            if not (np.issubdtype(exp['normal'].dtype, np.number)):
                error_log.append("The file " + f + " has non-numerical expression value for normal sample")
                break
            if not (np.issubdtype(exp['abnormal'].dtype, np.number)):
                error_log.append("The file " + f + " has non-numerical expression value for mutant sample")
                break
            if not (exp['pvalue'].between(0, 1).all()):
                error_log.append("The file " + f + " has p-value that is not between 0 and 1")
                break

            if exp_all.empty:
                exp_all = exp
            else:
                exp_all = pd.concat([exp_all,exp])
                
        except pd.errors.EmptyDataError:
            pass
        except ValueError:
            error_log.append("The column numbers are entered incorrectly")
            break
        except:
            error_log.append("Error occurred while reading expression files")
            break

if not exp_exist:
     error_log.append("Expression files are not uploaded")
    
try:    
    exp_all.drop_duplicates(subset=['sampleID','gene'],inplace=True)
    exp_all.dropna(axis=0,how='any',inplace=True)
    exp_all.to_csv(exppath+"all_exp.csv",index=False)
    exp_sample = exp_all['sampleID'].unique()
    if (np.any(np.in1d(exp_sample, mysample))):
        error_log.append("There are duplicated samples in your database!")
except KeyError:
    if (error_log == []):
        error_log.append("Error occurred while reading expression files")
except:
    error_log.append("Something went wrong!")
        
####################################################################

#Deal with mutation file
mut_all = pd.DataFrame()
mut_exist = False
if os.path.isfile(mutpath+"all_mut.csv"):
    os.unlink(mutpath+"all_mut.csv")

for f in listdir(mutpath):
    if (isfile(join(mutpath,f)) and not f.startswith('.')):
        mut = pd.DataFrame()
        mut_exist = True
        try:
            thisfile = join(mutpath,f)
            mut = pd.read_csv(thisfile,sep=",",usecols=cols,header=None)
            mut.columns = ['sampleID','gene','chr','position','ref','alt','effect','consequence']
                
            mut['mutation'] = mut['chr'].astype(str)+ ' ' + mut['position'].astype(str) + ' ' + mut['ref'] + '|' + mut['alt'] + ': ' + mut['effect'] + ': ' + mut['consequence']
            mut = mut[['sampleID','gene','mutation']]
            mut = mut.groupby(['sampleID','gene']).apply(lambda x: '; '.join(x.mutation)).reset_index()
            mut.columns=['sampleID','gene','mutation']
            
            mut['userID'] = userID
            mut = mut[['userID','sampleID','gene','mutation']]
            mut.dropna(axis=0,how='any',inplace=True)

            if mut_all.empty:
                mut_all = mut
            else:
                mut_all = pd.concat([mut_all,mut])
                
        except pd.errors.EmptyDataError:
            pass
        except ValueError:
            error_log.append("The column numbers are entered incorrectly")
            break
        except:
            error_log.append("Error occurred while reading mutation files")
            break

if mut_exist:
    try:
        exp_sample = pd.DataFrame(exp_all['sampleID'].unique(),columns=['sampleID'])
        mut_all = pd.merge(exp_sample,mut_all,on="sampleID",how='inner')
        mut_all.drop_duplicates(subset=['sampleID','gene'],inplace=True)
        mut_all.dropna(axis=0,how='any',inplace=True)
        mut_all = mut_all[['userID','sampleID','gene','mutation']]
        mut_all.to_csv(mutpath+"all_mut.csv",index=False)

    except KeyError:
        if (error_log == []):
            error_log.append("Error occurred while reading mutation files")
    except:
        error_log.append("Something went wrong!")

####################################################################
#Upload files
if (error_log == []):
    try:  
        cur.execute("DROP TABLE IF EXISTS tmp_exp")
        query = "CREATE TABLE tmp_exp (userID varchar(50), sampleID varchar(50), gene varchar(50), normal float, abnormal float, log2 float, pvalue float, PRIMARY KEY (userID, sampleID, gene))"
        cur.execute(query)
        query = "LOAD DATA LOCAL INFILE '"+exppath+"all_exp.csv"+"' INTO TABLE expression FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
        cur.execute(query)
        query = "LOAD DATA LOCAL INFILE '"+exppath+"all_exp.csv"+"' INTO TABLE tmp_exp FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
        cur.execute(query)
        conn.commit()
        query = 'INSERT INTO target_exp SELECT * FROM tmp_exp WHERE gene in ('+','.join(map("'{0}'".format, genes))+')'
        cur.execute(query)
        conn.commit()
        cur.execute("DROP TABLE IF EXISTS tmp_exp")

        if mut_exist:
            cur.execute("DROP TABLE IF EXISTS tmp_mut")
            query = "CREATE TABLE tmp_mut (userID varchar(50),sampleID varchar(50), gene varchar(50), mutation varchar(200),PRIMARY KEY (userID, sampleID, gene))"
            cur.execute(query)
            query = "LOAD DATA LOCAL INFILE '"+mutpath+"all_mut.csv"+"' INTO TABLE mutation FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
            cur.execute(query)
            query = "LOAD DATA LOCAL INFILE '"+mutpath+"all_mut.csv"+"' INTO TABLE tmp_mut FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
            cur.execute(query)
            conn.commit()
            query = 'INSERT INTO target_mut SELECT * FROM tmp_mut WHERE gene in ('+','.join(map("'{0}'".format, genes))+')'
            cur.execute(query)
            conn.commit()
            cur.execute("DROP TABLE IF EXISTS tmp_mut")
            
    except:
        error_log.append("Error occured while uploading to database!")
        
####################################################################
#Create file directory file

if (error_log == []):
    try:
        my_file_directory = pd.DataFrame(exp_all['sampleID'].unique(),columns=['sampleID'])
        my_file_directory['userID'] = userID
        my_file_directory['folder'] = folder
        my_file_directory['subfolder'] = subfolder
        my_file_directory['organism'] = organism
        my_file_directory['datetime'] = exp_date
        my_file_directory = my_file_directory[['userID','sampleID','folder','subfolder','organism','datetime']]
        my_file_directory.to_csv(exppath+"file_directory.csv",index=False)
        query = "LOAD DATA LOCAL INFILE '"+exppath+"file_directory.csv"+"' INTO TABLE file_directory FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
        cur.execute(query)
        conn.commit()
    except:
        error_log.append("Something went wrong!")
    
conn.close() 
result = json.dumps(error_log)
print (result)
        
        
        
        
        

            

            
    
