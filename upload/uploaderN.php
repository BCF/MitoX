<?php
$id = $_GET['sessionid'];
//$id = "test";
   
//$organism = $_POST['organism'];
$target_path_expression = "../data/user_uploads/".$id."/expression/";
$target_path_mutation = "../data/user_uploads/".$id."/mutation/";

$geneName = $_POST['geneName'];
$log2 = $_POST['log2'];
$pvalue = $_POST['pvalue'];
$WT = $_POST['WT'];
$Abnormal = $_POST['Abnormal'];

$geneNameMut = $_POST['geneNameMut'];
$chromosome = $_POST['chr'];
$position = $_POST['pos'];
$reference = $_POST['ref'];
$alternative = $_POST['alt'];
$effect = $_POST['effect'];
$consequence = $_POST['consequence'];
$organism = $_POST['organism'];

//Get info for mysql server
$str = file_get_contents('../mysql/mysql_info.json');
$json = json_decode($str, true);
$host = $json['host'];
$port = $json['port'];
$user = $json['user'];
$passwd = $json['passwd'];
$unix_socket = $json['unix_socket'];

$error_upload = array();

//delete all previous files
$files = glob($target_path_expression . "*"); // get all file names
foreach($files as $file){ // iterate files
    if(is_file($file)) unlink($file); // delete file
}

$files = glob($target_path_mutation . "*"); // get all file names
foreach($files as $file){ // iterate files
    if(is_file($file)) unlink($file); // delete file
}

if (!is_dir($target_path_expression)){
    mkdir($target_path_expression, 0777, true);
}

if (!is_dir($target_path_mutation)){
    mkdir($target_path_mutation, 0777, true);
}

$total_expression = count($_FILES['expression']['name']);
$total_mutation = count($_FILES['mutation']['name']);

    
// Loop through each expresion file and upload them
$counter = count($_POST["samplename"]);

$error_upload = array();

for($i=0; $i<$counter; $i++) {
    $samplename = $_POST['samplename'][$i];

    $target_pathnew = $target_path_expression . basename( $_FILES['expfile']['name'][$i]); 

    $express = $_FILES['expfile']['name'][$i];
     if(move_uploaded_file($_FILES['expfile']['tmp_name'][$i], $target_pathnew)) {
         //echo "The file ".  basename( $_FILES['expfile1']['name']). " has been uploaded";
    } else{
        array_push($error_upload,"There was an error uploading the expression file of sample '$samplename', please try again!");
    }

    $expressfile = $target_path_expression.$samplename.".csv";
    rename ($target_pathnew, $expressfile);

    if ($_FILES['mutfile']['name'][$i]){
        $target_pathnew = $target_path_mutation . basename( $_FILES['mutfile']['name'][$i]); 

        $variant = $_FILES['mutfile']['name'][$i];
        if(move_uploaded_file($_FILES['mutfile']['tmp_name'][$i], $target_pathnew)) {
            //echo "The file ".  basename( $_FILES['mutfile1']['name']). " has been uploaded";
        } else{
            array_push($error_upload,"There was an error uploading the mutation file of sample '$samplename', please try again!");
        }

        $mutationfile = $target_path_mutation.$samplename.".csv";
        rename ($target_pathnew, $mutationfile);
    } 
}

if (empty($error_upload)){

    $cmd = "python uploadN.py ".$id." ".$organism." ".$geneName." ".$WT." ".$Abnormal." ".$log2." ".$pvalue." ".$geneNameMut." ".$chromosome." ".$position." ".$reference." ".$alternative." ".$effect." ".$consequence." ".$target_path_expression." ".$target_path_mutation." ".$host." ".$port." ".$user." ".$unix_socket." ".$passwd;

    $output = shell_exec($cmd);

    $jsoutput = json_decode($output, true);
    $error_upload = array_merge($error_upload,$jsoutput);

} 

//delete all previous files
$files = glob($target_path_expression . "*"); // get all file names
foreach($files as $file){ // iterate files
    if(is_file($file)) unlink($file); // delete file
}

$files = glob($target_path_mutation . "*"); // get all file names
foreach($files as $file){ // iterate files
    if(is_file($file)) unlink($file); // delete file
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>MitoXplorer - Upload</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="../css/compare/style.css" rel="stylesheet" >
    
    <script src="../js/jquery-1.12.4.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    
    
</head>
    
<body>
  
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="../index.php">MitoXplorer</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="index.php"></a>
                    </li>
                    <li>
                        <a href="../index.php#about">About</a>
                    </li>
                    <li>
                        <a href="../index.php#database">Database</a>
                    </li>
                    <li>
                        <a href="../index.php#analysis">Analysis</a>
                    </li>
                    <li>
                        <a href="../index.php#tutorial">Tutorial</a>
                    </li>
                    <li>
                        <a href="../index.php#download">Download</a>
                    </li>
                    <li>
                        <a href="../index.php#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    
<div class="container">
    
    <div class="jumbotron" id="message-div">
    </div>
        
    </div>     
</body>

<script>
    var error_upload = <?php echo json_encode($error_upload); ?>;
    var sessionid = "<?php echo $id; ?>";

    var messageDiv = document.getElementById('message-div');

    if (error_upload[0] != "No Error"){
        newdiv = document.createElement('div');
        newdiv.innerHTML = "<h1>Oops...</h1><br>There are some problems with the uploading process.";
        messageDiv.appendChild(newdiv);
        
        for (i=0; i<error_upload.length;i++){
            newdiv = document.createElement('div');
            newdiv.innerHTML = "<font color='red'>"+ error_upload[i] +"</font><br>";
            messageDiv.appendChild(newdiv);
        }
        
        newdiv = document.createElement('div');
        newdiv.innerHTML = "<br><br><p>Do you wish to go back and do the upload again?<p>";
        messageDiv.appendChild(newdiv);
        
    }else{
        
        newdiv = document.createElement('div');
        newdiv.innerHTML = "<h1>You are done!</h1><br><p>Check your database or go back to upload more samples!<br><br>Save this link to retrieve your data when you want to visit later:<br><a href='../index.php?sessionid="+sessionid+"'>http://mitoxplorer.biochem.mpg.de/index.php?sessionid="+sessionid+"</a></p>";
        messageDiv.appendChild(newdiv);
        
    }
    
    newdiv = document.createElement('div');
    newdiv.innerHTML = "<br><a href='../upload.php'><button type='button' class='btn btn-primary'>Go Back</button></a><a href='../index.php#database'><button type='button' class='btn btn-primary' style='float: right;background-color: #12e2c0;border-color: #12e2c0;'>Check Your Database</button></a>";
    messageDiv.appendChild(newdiv);    
    

</script>
    
</html>

