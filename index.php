<?php 
// restore existing user upload session when browser was closed
if (isset($_COOKIE['mitox_session_id'])) { 
        session_id($_COOKIE['mitox_session_id']);
}
if ($_GET['sessionid']) { 
        session_id($_GET['sessionid']);
}
session_start();
$id = session_id();
setcookie('mitox_session_id',$id,time() + (86400 * 7));


$PCA_path = "data/user_uploads/".$id."/PCA/";
if (!is_dir($PCA_path)){
    mkdir($PCA_path, 0777, true);
}

$heatmap_path = "data/user_uploads/".$id."/heatmap/";
if (!is_dir($heatmap_path)){
    mkdir($heatmap_path, 0777, true);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MitoXplorer</title>

    <!-- CSS -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/index/index.css" rel="stylesheet">
    
    <!-- javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <link rel="icon" type="image/png" href="img/logos/favicon.png">

</head>

<body id="page-top" class="index">
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">MitoXplorer</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a href="about.html">About</a>
                    </li>
                    <li>
                        <a href="database.php">Database</a>
                    </li>
                    <li>
                        <a href="compare.php">Analysis</a>
                    </li>
                    <li>
                        <a href="upload.php">Upload</a>
                    </li>
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-heading col-md-12">Welcome to <span style='font-family: "Kaushan Script", "Helvetica Neue", Helvetica, Arial, cursive;'>MitoXplorer</span></div>
                <div class="intro-lead-in col-md-12">A Visualization Tool for Mitochondrial Gene Interactome</div>
                <div class="col-md-12"><p style="text-decoration: underline"><a data-toggle="collapse" href="#moreinfo">Click here for more</a></p>
                    <div id="moreinfo" class="collapse col-md-12">
                    <p style="font-size: 1.2em;">MitoXplorer maps mutation and expression data to our mitochondrial model that consists of known functions of mitochondria, and allows user to explore the data in an interactive way. You could start with browsing our databases, doing comparative analysis on them or even upload your own data.</p>
                    </div>
                </div>
                <div class="col-md-7" style="padding-top: 30px; padding-bottom: 20px">
                    <div class="col-md-12"><p class="large">Get started with one of the followings:</p></div>
                    <div class="col-md-12 selection">
                        <a href="about.html" class="icons"><i class="fa fa-question-circle-o"></i></a>
                        <p class="large2" style="display: inline-block;">&emsp;<a href="about.html">Get to know MitoXplorer and how to use it</a></p>
                    </div>
                    <div class="col-md-12 selection">
                        <a href="database.php" class="icons"><i class="fa fa-database"></i></a>
                        <p class="large2" style="display: inline-block;">&emsp;<a href="database.php">Browse our database</a></p>
                    </div>
                    <div class="col-md-12 selection">
                        <a href="compare.php" class="icons"><i class="fa fa-laptop"></i></a>
                        <p class="large2" style="display: inline-block;">&emsp;<a href="compare.php">Analyse and visualise data</a></p>
                    </div>
                    <div class="col-md-12 selection">
                        <a href="upload.php" class="icons"><i class="fa fa-upload"></i></a>
                        <p class="large2" style="display: inline-block;">&emsp;<a href="upload.php">Upload your own data</a></p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">

                      <!-- Wrapper for slides -->
                      <div class="carousel-inner">
                        <div class="item active">
                          <img src="./img/carousel/scatterplot.png">
                            <div class="carousel-caption d-none d-md-block">
                                <h4>Scatterplots</h4>
                            </div>
                        </div>

                        <div class="item">
                          <img src="./img/carousel/PCA.png">
                            <div class="carousel-caption d-none d-md-block">
                                <h4>Principle Component Analysis</h4>
                            </div>
                        </div>

                        <div class="item">
                          <img src="./img/carousel/heatmap.png">
                            <div class="carousel-caption d-none d-md-block">
                                <h4>Hierarchical Clustering</h4>
                            </div>
                        </div>
                      </div>

                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                </div>
            </div>
        </div>

    </header>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="copyright">Copyright &copy; Max-Planck-Gesellschaft, München 2017</span>
                </div>
            </div>
        </div>
    </footer>

</body>

</html>
